import { Pipe, PipeTransform } from '@angular/core';
   import { DatePipe } from '@angular/common';
   
   @Pipe({
     name: 'customDate'
   })
   export class CustomDatePipe extends DatePipe implements PipeTransform {
     transform(value: any, args?: any): any {
      //  return new Date(value);

      const db_date = new Date(value);
      const today =  new Date();
      //  return Date.now();

      var difference = today.getTime() - db_date.getTime();

      var yearsDifference = Math.floor(difference/1000/60/60/24/30/12);
      difference -= yearsDifference*1000*60*60*24*30*12;
      if(yearsDifference) {
        return yearsDifference + ' ' + 'year';
      }

      var monthsDifference = Math.floor(difference/1000/60/60/24/30);
      difference -= monthsDifference*1000*60*60*24*30;
      if(monthsDifference) {
        return monthsDifference  + ' ' + 'month';
      }

    var daysDifference = Math.floor(difference/1000/60/60/24);
    difference -= daysDifference*1000*60*60*24;

    if(daysDifference) {
      return daysDifference  + ' ' + 'days';
    }

    var hoursDifference = Math.floor(difference/1000/60/60);
    difference -= hoursDifference*1000*60*60;

    if(hoursDifference) {
      return hoursDifference  + ' ' + 'hours';
    }

    var minutesDifference = Math.floor(difference/1000/60);
    difference -= minutesDifference*1000*60;

    if(minutesDifference) {
      return minutesDifference  + ' ' + 'minutes';
    }

    var secondsDifference = Math.floor(difference/1000);
    if(secondsDifference) {
      return secondsDifference  + ' ' + 'seconds';
    }
    return

    console.log('difference = ' + 
    yearsDifference + ' years/s ' + 
    monthsDifference + ' months/s ' + 
      daysDifference + ' day/s ' + 
      hoursDifference + ' hour/s ' + 
      minutesDifference + ' minute/s ' + 
      secondsDifference + ' second/s ');
      // return db_date.getTime() + '  ' + today.getTime();

      const Time = db_date.getTime() - today.getTime(); 
    //  return Math.abs(Time) / (1000 * 60 * 60) % 24;
      let ter =  (today.getTime() - db_date.getTime());
      let fr = (1000 / 60 / 60) % 24
      console.log('diff',ter);
      console.log('formula',fr);
      
      console.log(ter/fr);
      
      return ter/(1000 / 60 / 60) % 24 ;
      let montths = Math.floor((today.getTime() - db_date.getTime()) / 1000 / 60 / 60 / 24 / 7 / 4);

      let days = Math.floor((today.getTime() - db_date.getTime()) / 1000 / 60 / 60 / 24 );
      let hours = Math.abs((today.getTime() - db_date.getTime()) / (1000 / 60 / 60) % 24 );
      return  hours;
      const Days = Time / (1000 * 3600 * 24); //Diference in Days
      return Math.floor(Days);
       return super.transform(value, "EEEE d MMMM y h:mm a");
     }
   }