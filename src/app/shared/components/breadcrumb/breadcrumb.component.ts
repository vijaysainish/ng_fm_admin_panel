import { CommonService } from 'src/app/services/common.service';
import { NavigationEnd, Router } from '@angular/router';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  @Input() title: string;
  @Input() items: any[];
  @Input() isUsers: boolean = false;
  @Input() isFiles: boolean = false;
  @Input() active_item: string;
  // @ViewChild('fileImportInput') fileImportInput: any;
  @Output('callImportCSV') callImportCSV: EventEmitter<any> = new EventEmitter();

  @ViewChild('inputcsvFile')
import_csv_input: ElementRef;

  activeLink: string;
  isAdmin: boolean = false;
  fileUpload(e){
    console.log(e.target.files);
    // this.router.navigate(['action-selection'], { state: { example: 'bar' } });
    this.cs.navigate('/file-management/upload/edit', { state: { data: e.target.files } })
  }
  public   csvfileUpload(files: FileList){
    console.log(files);
    if(files && files.length > 0) {
       let file : File = files.item(0); 
         console.log(file.name);
         console.log(file.size);
         console.log(file.type);
         let reader: FileReader = new FileReader();
         reader.readAsText(file);
         reader.onload = (e) => {
            let csv: string = reader.result as string;
            console.log(csv);
            alert('check your console')
         }
      }
  }
  

  constructor(private router: Router, private cs: CommonService) {
    this.isAdmin = this.cs.isAdmin();
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        let urlParts = e.urlAfterRedirects.split('/');
        if (urlParts.length > 2) {
          this.activeLink = urlParts[2];
        }
      }
    });
  }
  ngOnInit() {}

  importCSV($event) {
    console.log(444);
    // this.import_csv_input
    setTimeout(() => {
      this.import_csv_input.nativeElement.value = "";
    }, 3000);
   
    this.callImportCSV.emit($event);
  }
}
