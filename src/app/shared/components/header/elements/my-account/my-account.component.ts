import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {
  CookieService
} from 'ngx-cookie';
import { Subscription } from 'rxjs';
import {
  CommonService
} from 'src/app/services/common.service';
import { baseUrl,imgBaseUrl } from '../../../../../Constants/constants';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss'],
})
export class MyAccountComponent implements OnInit {
  passForm: FormGroup;
  requested: boolean = false;
  showNewPass: boolean = false;
  showCurrentPass: boolean = false;
  showPass: boolean = false;
  showPass2: boolean = false;
  isAdmin: boolean = false;
  user: any;
	subscriptions: Subscription[] = [];
  imgBaseUrl = imgBaseUrl;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private cs: CommonService,
    private cookie: CookieService
  ) {
    this.isAdmin = this.cs.isAdmin();
    this.passForm = this.fb.group({
      oldPassword: ["", Validators.required],

      newPassword: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
        ]),
      ],
    });
    this.fetchUserDetails();

   let get_updated_photo = this.cs.getUpdatedProfilePic.subscribe((data:any) => {
      console.log(data);
      if(data && data.is_profile_pic && this.user) {
        this.user.profilePic = data.img;
      } 
     });
    //  get_updated_photot.unsubscribe();
  }
  fetchUserDetails() {
    this.cs.httpRequest('get', `user`) ?.subscribe(
      (res: any) => {
        this.user = res;

      },
      (err) => this.cs.handleError(err)
    );
  }

  ngOnInit(): void {}
  changePass() {
		this.requested = true;
		let passSub = this.cs
			.httpRequest("post", "auth/change-password", this.passForm.value)
			.subscribe(
				(res: any) => {
					this.cs.showSuccessMsg(res.message, "Success");
          this.modalService.dismissAll();
					this.requested = false;
					this.passForm.reset();
				},
				(err: any) => {
					this.requested = false;
					if (err.status == 401) return this.cs.unAuthorized();
					if (err.status == 404)
						return this.cs.showErrorMsg(
							err.error.msg,
							"Error"
						);
					if (err.status == 500)
						return this.cs.showErrorMsg(
							err.error.msg,
							"Error"
						);
					this.cs.showErrorMsg(err.error.msg, "Error");
				}
			);
		this.subscriptions.push(passSub);
	}

 
  logout() {
    this.cs.logout();
  }
  openChangePassword(content) {
    this.modalService.open(content);

  }
}
