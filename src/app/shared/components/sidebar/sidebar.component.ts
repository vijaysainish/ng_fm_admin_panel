import { Component, HostListener, ViewEncapsulation ,ChangeDetectorRef} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { LayoutService } from '../../services/layout.service';
import { Menu, NavService } from '../../services/nav.service';

import { imgBaseUrl } from 'src/app/Constants/constants';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SidebarComponent {
  public iconSidebar;
  public menuItems: Menu[];
  public url: any;
  public fileurl: any;

  imgBaseUrl = imgBaseUrl;
  logo:any;
  title:any;

  // For Horizontal Menu
  public margin: any = 0;
  public width: any = window.innerWidth;
  public leftArrowNone: boolean = true;
  public rightArrowNone: boolean = false;
  public urread_msg_count:any;

  constructor(
    private router: Router,
    public navServices: NavService,
    public layout: LayoutService,
    private cs: CommonService,
  ) {
   let approveMessg =  this.cs.currentApprovalStageMessage.subscribe(msg => {
     console.log(msg);
     this.urread_msg_count = msg? msg:0;
    });
    let get_updated_photo = this.cs.getUpdatedProfilePic.subscribe((data:any) => {
      console.log(data);
      if(data && data.is_logo && this.logo) {
        this.logo = data.img;
        this.title = data.title
      } 
     });
    // approveMessg.unsubscribe();
    this.navServices.items.subscribe((menuItems) => {
      // console.log('menuitemssssssss',menuItems);
      
      this.router.events.subscribe((event) => {
        this.menuItems = menuItems;
        if (event instanceof NavigationEnd) {
          menuItems.filter((items) => {
            if (items.path === event.url) {
              this.setNavActive(items);
            }
          });
        }
      });
    });
    this.fetchLogo();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth - 500;
  }

  sidebarToggle() {
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
  }

  // Active Nave state
  setNavActive(item) {
    console.log(item);
    
    this.menuItems.filter((menuItem) => {
      if (menuItem !== item) {
        menuItem.active = false;
      }
    });
  }

  // Click Toggle menu
  toggletNavActive(item) {
    if (!item.active) {
      this.menuItems.forEach((a) => {
        if (this.menuItems.includes(item)) {
          a.active = false;
        }
      });
    }
    item.active = !item.active;
  }

  // For Horizontal Menu
  scrollToLeft() {
    if (this.margin >= -this.width) {
      this.margin = 0;
      this.leftArrowNone = true;
      this.rightArrowNone = false;
    } else {
      this.margin += this.width;
      this.rightArrowNone = false;
    }
  }

  scrollToRight() {
    if (this.margin <= -3051) {
      this.margin = -3464;
      this.leftArrowNone = false;
      this.rightArrowNone = true;
    } else {
      this.margin += -this.width;
      this.leftArrowNone = false;
    }
  }


  fetchLogo() {
    this.cs.httpRequest('get', `admin/logo`)?.subscribe(
      (res: any) => {
       this.logo = res.image;
       this.title = res.name;
       
      },
      (err) => this.cs.handleError(err)
    );
  }
}
