import { RolesGuard } from './../../guards/role.guard';
import { Routes } from '@angular/router';

export const content: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import('../../modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
  },
  {
    path: 'user',
    loadChildren: () =>
      import('../../modules/users/users.module').then((m) => m.UsersModule),
    canActivate:[RolesGuard]
  },
  {
    path: 'notifications',
    loadChildren: () =>
      import('../../modules/notification/notification.module').then((m) => m.NotificationModule),
  },
  {
    path: 'file-management',
    loadChildren: () =>
      import('../../modules/file-management/file-management.module').then((m) => m.FileManagementModule),
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('../../modules/settings/settings.module').then((m) => m.SettingsModule),
  },

];
