import { Routes } from '@angular/router';

export const other_user: Routes = [
 

  {
    path: 'other-user',
    loadChildren: () =>
      import('../../modules/other-user/other-user.module').then(
        (m) => m.OtherUserModule
      ),
  }];
