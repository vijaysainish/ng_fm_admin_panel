import { Routes } from '@angular/router';

export const full: Routes = [
  {
    path: 'auth/login',
    loadChildren: () =>
      import('../../modules/auth/login/login.module').then(
        (m) => m.LoginModule
      ),
  },
  {
    path: 'admin/login',
    loadChildren: () =>
      import('../../modules/auth/admin-login/admin-login.module').then(
        (m) => m.AdminLoginModule
      ),
  },

  {
    path: 'auth/forgot-password',
    loadChildren: () =>
      import('../../modules/auth/forgot-password/forgot-password.module').then(
        (m) => m.ForgotPasswordModule
      ),
  },
  {
    path: 'auth/reset-password/:token',
    loadChildren: () =>
      import('../../modules/auth/change-password/change-password.module').then(
        (m) => m.ChangePasswordModule
      ),
  },
  {
    path: 'auth/register',
    loadChildren: () =>
      import('../../modules/auth/profile/profile.module').then(
        (m) => m.ProfileModule
      ),
  },
];
