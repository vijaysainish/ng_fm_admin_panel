import { CommonService } from 'src/app/services/common.service';
import { Injectable, OnDestroy } from '@angular/core';
import { ChildActivationEnd, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, fromEvent, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

// Menu
export interface Menu {
  path?: string;
  title?: string;
  icon?: string;
  type?: string;
  active?: boolean;
  is_bedge?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class NavService implements OnDestroy {
  private unsubscriber: Subject<any> = new Subject();
  public screenWidth: BehaviorSubject<number> = new BehaviorSubject(
    window.innerWidth
  );

  // For Horizontal Layout Mobile
  public horizontal: boolean = window.innerWidth < 991 ? false : true;

  // Collapse Sidebar
  public collapseSidebar: boolean = window.innerWidth < 991 ? true : false;

  // Full screen
  public fullScreen: boolean = false;
  MENUITEMS: Menu[] = [
    {
      path: '/dashboard',
      title: 'Dashboard',
      icon: 'home',
      type: 'link',
    },
    {
      path: '/user',
      title: 'Users',
      icon: 'users',
      type: 'link',
    },
    {
      path: '/file-management',
      title: 'File Management',
      icon: 'folder',
      type: 'link',
    },
    {
      path: '/notifications',
      title: 'Notifications',
      is_bedge: true,
      icon: 'bell',
      type: 'link',
    },
    {
      path: '/settings',
      title: 'Settings',
      icon: 'tool',
      type: 'link',
    },
  ];

  constructor(private router: Router, private cs: CommonService) {
    this.router.events.subscribe((event) => {
      if (event instanceof ChildActivationEnd ) {

        if (!this.cs.isAdmin()) {
          this.MENUITEMS = this.MENUITEMS.filter((x) => x.path != '/user');
        }else {
          this.MENUITEMS= [
            {
              path: '/dashboard',
              title: 'Dashboard',
              icon: 'home',
              type: 'link',
            },
            {
              path: '/user',
              title: 'Users',
              icon: 'users',
              type: 'link',
            },
            {
              path: '/file-management',
              title: 'File Management',
              icon: 'folder',
              type: 'link',
            },
            {
              path: '/notifications',
              title: 'Notifications',
              is_bedge: true,
              icon: 'bell',
              type: 'link',
            },
            {
              path: '/settings',
              title: 'Settings',
              icon: 'tool',
              type: 'link',
            },
          ];
        }
        
        this.items.next(this.MENUITEMS);
      }
    });
    this.setScreenWidth(window.innerWidth);
    fromEvent(window, 'resize')
      .pipe(debounceTime(1000), takeUntil(this.unsubscriber))
      .subscribe((evt: any) => {
        this.setScreenWidth(evt.target.innerWidth);
        if (evt.target.innerWidth < 991) {
          this.collapseSidebar = true;
        }
      });
    if (window.innerWidth < 991) {
      // Detect Route change sidebar close
      this.router.events.subscribe((event) => {
        this.collapseSidebar = true;
      });
    }
  }

  ngOnDestroy() {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  private setScreenWidth(width: number): void {
    this.screenWidth.next(width);
  }

  
  // Array
  items = new BehaviorSubject<Menu[]>(this.MENUITEMS);
}
