import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import WebViewer, { WebViewerInstance } from '@pdftron/webviewer';
import { baseUrl } from '../../Constants/constants';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-other-user',
  templateUrl: './other-user.component.html',
  styleUrls: ['./other-user.component.css']
})
export class OtherUserComponent implements OnInit {

  isSignature: boolean = true;
  file_id: any;
  saveBtn: HTMLButtonElement;
  user_details: any;
  type: any;
  shared_file: any;
  is_annots_loaded:boolean;
  name:any;
  email:any;
  sign_submitted:boolean = false;
  constructor(private cs: CommonService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.file_id = params['id'];
      this.name = params['name'];
      this.email = params['email'];
      // this.type =  params['type'];

    });
  }

  ngOnInit(): void {
    this.cs.httpRequest('get', `file-management/other-user-file?id=${this.file_id}&name=${this.name}&email=${this.email}`).subscribe(res => {
      console.log(res);
      this.shared_file = res;
      const file_name = this.shared_file.file.fileName;
      const documentBlobObjectUrl = baseUrl + 'file-management/file/' + file_name
      // this.fetchUserDetails();
      this.uploadWebViewver(documentBlobObjectUrl);
    })
  }
  // fetchUserDetails() {
  //   this.cs.httpRequest('get', `user`)?.subscribe(
  //     (res: any) => {
  //       console.log(res);
  //       this.user_details = res;
  //     },
  //     (err) => this.cs.handleError(err)
  //   );
  // }

  uploadWebViewver(documentBlobObjectUrl) {
    WebViewer({
      path: './../../../../assets/lib',
      initialDoc: documentBlobObjectUrl
    }, document.getElementById('viewer'))
      .then(instance => {


        const { UI, Core } = instance;
        const { documentViewer, annotationManager, Tools, Annotations } = Core;
        // call methods from UI, Core, documentViewer and annotationManager as needed
        instance.UI.disableElements(this.disableElementsForAdmin());
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Z)
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Y)

        instance.UI.setZoomLevel(0.4);
        // create a form field
        const { WidgetFlags } = Annotations;
        // set author name for comment
        annotationManager.setAnnotationDisplayAuthorMap((userId) => {
          return this.name;

        });

        // const displayAuthor = annotationManager.getDisplayAuthor(annotation.Author);
        // add_annot_btn is your own custom button
        // document.getElementById('rm_annot_btn').addEventListener('click', () => {


        // });

        // add_annot_btn is your own custom button
        // document.getElementById('add_annot_btn').addEventListener('click', () => {

        // });

        documentViewer.addEventListener('documentLoaded', () => {
          console.log('document loaded');

        });
        documentViewer.addEventListener('annotationsLoaded', () => {
          console.log('annots loadded');
          
          this.is_annots_loaded = true;
        });
        
        instance.UI.setHeaderItems(header => {
          const parent = documentViewer.getScrollViewElement().parentElement;

          const menu = document.createElement('div');
          menu.classList.add('Overlay');
          menu.classList.add('FlyoutMenu');
          menu.style.padding = '1em';

          const downloadBtn = document.createElement('button');
          downloadBtn.textContent = 'Download';
          downloadBtn.onclick = () => {
            // Download
          };

          menu.appendChild(downloadBtn);

          let isMenuOpen = false;

          const renderCustomMenu = () => {
            this.saveBtn = document.createElement('button');
            this.saveBtn.textContent = 'Save';
            this.saveBtn.id = 'savebtn';
            this.saveBtn.disabled = false;
            this.saveBtn.hidden = true;

            this.saveBtn.onclick = () => {
              console.log(55);
              annotationManager.exportAnnotations().then(xfdfString => {
                console.log(44555);

                const doc = documentViewer.getDocument();
                doc.getFileData({ xfdfString }).then(data => {
                
                  const arr = new Uint8Array(data);
                  const blob = new Blob([arr], { type: 'application/pdf' });
                  console.log(blob);
                  console.log(URL.createObjectURL(blob));
                  let api_data = {
                    file_id: parseInt(this.file_id)
                  }
                  // this.redirectLink();
                  // return;
                  // console.log(this.selectedUser);
                  this.blobImage(blob,api_data);

                })
              });

              if (isMenuOpen) {
                parent.removeChild(menu);
              } else {
                menu.style.left = `${document.body.clientWidth - (this.saveBtn.clientWidth + 40)}px`;
                menu.style.right = 'auto';
                menu.style.top = '40px';
                parent.appendChild(menu);
              }

              isMenuOpen = !isMenuOpen;
            };

            return this.saveBtn;
          };

          const newCustomElement = {
            type: 'customElement',
            render: renderCustomMenu,
          };

          header.push(newCustomElement);
        });
        annotationManager.addEventListener('annotationChanged', (annotations, action) => {
          // use when new annots added
         
          if (action === 'add' &&  !this.is_annots_loaded) {
            const annots = annotationManager.getAnnotationsList();
            console.log('this is a change that added annotations');
            const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
            const sign_here_annots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
            // const signAnnots2 = annots.filter(a => a instanceof Annotations.type);
          console.log('widgetAnnots',widgetAnnots);
          console.log('signAnnots',signAnnots);
          console.log('signAnnots',annots);

          console.log('sign_here_annots',sign_here_annots);
        
            annots.forEach((element, i) => {
              console.log('element id',element.Id);
              // console.log('this.user_details.id',this.user_details.id);
              if(element.getCustomData('user_id')) {
              element.Hidden = (this.email+ ' '+ this.name) !=(element.getCustomData('user_id')).trim()? true:false;
              }else {
                element.Hidden = (this.email+ ' '+ this.name) != (element.Id).trim()? true:false;
              }
              element.ReadOnly = true;
              console.log(element);
              console.log('user_id',element.getCustomData('user_id'));

            });
          }
        
          // use when annots read from docs
          if (action === 'add' &&  this.is_annots_loaded) {
            console.log('annotations', annotations);
            annotations[0].setCustomData('user_id',(this.email+ ' '+ this.name));
            annotations[0].NoMove = true;
            const annots = annotationManager.getAnnotationsList();
            console.log('annots',annots);
            
            console.log('this is a change that added annotations');
            const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
            const sign_here_annots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
            console.log('widgetAnnots',widgetAnnots);
            console.log('signAnnots',signAnnots);
            console.log('sign_here_annots',sign_here_annots);
            widgetAnnots.forEach((element, i) => {
              element.Locked = true;
              element.LockedContents = true;
            
              // console.log('length',(this.email+ ' '+ this.name).length);
              // console.log((this.email+ ' '+ this.name));

              // console.log('element id',element.Id);

              // console.log('elemnt user id length',(element.Id).length);

              // console.log( (this.email+ ' '+ this.name) != element.Id);
              

              element.Hidden = (this.email+ ' '+ this.name) != (element.Id).trim() ? true:false;
              element.ReadOnly = true;
            });
            // sign_here_annots.forEach((element, i) => {
            //   element.Locked = true;
            //   element.LockedContents = true;
            
            //   console.log('length',(this.email+ ' '+ this.name).length);
            //   console.log((this.email+ ' '+ this.name));

            //   console.log('element id',element.getCustomData('user_id'));

            //   console.log('elemnt user id length',(element.getCustomData('user_id')).length);

            //   console.log( (this.email+ ' '+ this.name) != element.getCustomData('user_id'));
              

            //   element.Hidden = (this.email+ ' '+ this.name) != (element.getCustomData('user_id')).trim() ? true:false;
            //   element.ReadOnly = true;
            // });
            signAnnots.forEach((element, i) => {
              element.Hidden = (this.email+ ' '+ this.name) !=(element.getCustomData('user_id')).trim()? true:false;
              element.ReadOnly = (this.email+ ' '+ this.name) !=(element.getCustomData('user_id')).trim()? true:false;
              console.log(element);
             
              console.log('user_id',element.getCustomData('user_id'));
            });

          } else if (action === 'modify') {

            console.log('this change modified annotations');

          } else if (action === 'delete') {
            console.log('deleted annotation', annotations);

          }
          
        });


        instance.UI.loadDocument(documentBlobObjectUrl);
        // later save the document with updated annotations data
        documentViewer.addEventListener('annotationsLoaded', () => {

        })
      })
  }

  blobImage(blob: any,api_data) {

    
    const formData = new FormData();
    formData.append('file', blob, this.shared_file.fileName);
    console.log('blobbbb',blob);
    
    this.cs.pdfUpload(formData).subscribe(
      (events: any) => {
       
        if (events.type === HttpEventType.Response) {
          console.log(events.body);
          api_data.filename = events.body.filename;
          api_data.name = this.name;
          api_data.email = this.email;
          this.updateFile(api_data);
          this.cs.showInfoMsg('File Shared Successfully', 'Uploaded');

        }
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }
  updateFile(api_data) {
    this.cs.httpRequest('post','file-management/update-other-user-file',api_data).subscribe(
      (res:any) => {
       console.log(res);
       this.sign_submitted= true;
       location.reload();
      //  this.router.navigateByUrl('/file-management/listing');
      //  this.redirectLink();
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      });
  }
  redirectLink() {
    this.router.navigateByUrl('/file-management/shared');
  }
  saveFile() {
      this.saveBtn.click();
   let  btn =  document.getElementById('savebtn')
   console.log(btn); 
  }
  handleDenial() {

  }
  handleDismiss(e) {
    
  }

  submitSign() {
    if(this.shared_file.sign_status == 'Pending' && this.shared_file.is_locked) {
      this.cs.showInfoMsg('Locked File','file is locked, its not your turn.')
    }else if(!this.shared_file.is_locked) {
      this.saveFile();
    }
    
  }

  disableElementsForAdmin() {
    return [
      'viewControlsButton',
      'downloadButton',
      'printButton',
      'fileAttachmentToolGroupButton',
      'toolbarGroup-Edit',
      'crossStampToolButton',
      'checkStampToolButton',
      'dotStampToolButton',
      'rubberStampToolGroupButton',
      'dateFreeTextToolButton',
      // 'toolbarGroup-Shapes',
      'toolbarGroup-Forms',
      'shapeToolGroupButton',
      'highlightToolGroupButton',
      'freeHandHighlightToolGroupButton',
      'freeHandToolGroupButton',
      'selectToolButton',
      'strikeoutToolGroupButton',
      'squigglyToolGroupButton',
      'calloutToolGroupButton',
      'dropdown-item-toolbarGroup-Annotate',
      'linkButton',
      //  'dropdown-item-toolbarGroup-FillAndSign',
      'dropdown-item-toolbarGroup-Insert',
      'freeTextToolButton',
      'freeHandToolButton',
      'eraserToolButton',
      'redoButton',
      'undoButton',
      'dropdown-item-toolbarGroup-View',
      'dropdown-item-toolbarGroup-Shapes',
      'toggleNotesButton',
      'dropdown-item-toolbarGroup-Shapes',
      'toolbarGroup-Insert',
      'toolbarGroup-Shapes',
      'toolbarGroup-Annotate',
      'freeTextToolGroupButton',
      'toolbarGroup-View',
      'toolbarGroup-FillAndSign',
      'signatureToolGroupButton',
      'toolsOverlay'
    ]
  }
}
