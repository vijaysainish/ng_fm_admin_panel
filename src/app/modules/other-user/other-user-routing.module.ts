import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OtherUserComponent } from './other-user.component';

const routes: Routes = [{
  path:'',
  component:OtherUserComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherUserRoutingModule { }
