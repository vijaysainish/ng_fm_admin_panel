import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtherUserRoutingModule } from './other-user-routing.module';

import { OtherUserComponent } from './other-user.component';
@NgModule({
  declarations: [
    OtherUserComponent
  ],
  imports: [
    CommonModule,
    OtherUserRoutingModule,
  ]
})
export class OtherUserModule { }
