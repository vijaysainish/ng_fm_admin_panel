import { HttpEventType } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie';
import { imgBaseUrl } from 'src/app/Constants/constants';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  isAdmin: boolean = false;
  profileForm: FormGroup;
  isActive: boolean = false;
  userId: any;
  user: any;
  item: any = [];
  image!: File;
  logo!: File;
  imgBaseUrl = imgBaseUrl;
  emailNotification: boolean = false;
  uploading: boolean = false;
  imagefileName: any;

  title:any = '';

  constructor(
    private modalService: NgbModal,
    private cs: CommonService,
    private cookie: CookieService,
    private fb: FormBuilder,
    private route: ActivatedRoute,public cdr: ChangeDetectorRef
  ) {
    this.profileForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: [''],
      email: ['',Validators.required],
      phoneNumber: [''],
      profilePic: [''],
    });
    this.isAdmin = this.cs.isAdmin();
    this.route.params.subscribe((res) => {
      this.userId = res.id;
      this.fetchUserDetails();
      this.fetchLogo();
    });
  }

  ngOnInit(): void {}
  update(e) {
    // debugger;
    this.cs
      .httpRequest(
        'patch',
        this.emailNotification
          ? `user/enable-notifications`
          : 'user/disable-notifications'
      )
      ?.subscribe(
        (res: any) => {
          this.cs.showSuccessMsg('Updated', res.message);
        },
        (err) => this.cs.handleError(err)
      );
  }

  updateUser() {
    console.log(this.profileForm);
    console.log(this.profileForm.status);
    console.log(this.user );
    if (this.profileForm.status === 'INVALID') return;
    let userDetails = {
      firstName: this.profileForm.controls.firstName.value,
      lastName: (this.profileForm.controls.lastName.value).trim(),
      phoneNumber: this.profileForm.controls.phoneNumber.value + '',
      profilePic: this.image,
      logo: this.logo,
      isAdmin: this.user.role == 'User' ? false : true,
      isActive: this.user.isActive,
    };
    console.log(userDetails);

    this.cs.httpRequest('patch', 'user', userDetails)?.subscribe(
      (res: any) => {
        this.cs.showSuccessMsg('Updated', res.message);
        this.fetchUserDetails();
      },
      (err) => this.cs.handleError(err)
    );
  }

  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
        console.log(res);
        
        this.profileForm.setValue({
          email: res.email,
          firstName: res.firstName,
          lastName: res.lastName,
          phoneNumber: res.phoneNumber || null,
          profilePic: res.profilePic,
        });
        this.isActive = res.isActive;
        this.user = res;
        this.item = res;
        this.image = res.profilePic;
 
        
        this.emailNotification = res.isNotify;
      },
      (err) => this.cs.handleError(err)
    );
  }
  fetchLogo() {
    this.cs.httpRequest('get', `admin/logo`)?.subscribe(
      (res: any) => {
       this.title = res.name;
       this.logo = res.image;
       
      },
      (err) => this.cs.handleError(err)
    );
  }
  blogImage(e: any) {
    this.uploading = true;
   
    const uploaded_file = <File>e.target.files[0]
   
      this.image = uploaded_file;
  
    const formData = new FormData();
    formData.append('file', uploaded_file);
    this.cs.imgUpload(formData).subscribe(
      (events: any) => {
        console.log(events);
        if (events.type === HttpEventType.Response) {
          this.uploading = false;
         
            this.image = events.body.filename;
            let data = {
              img:this.image,
              is_profile_pic:true,
              is_logo:false
            }
            this.cs.updateProfilePic(data);
          this.cdr.detectChanges();
          this.updateImage(events.body.filename);
        }
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }


  
  blogLogoImage(e: any) {
    this.uploading = true;
   
    const uploaded_file = <File>e.target.files[0]
    this.logo = uploaded_file;
    const formData = new FormData();
    formData.append('file', uploaded_file);
    this.cs.imgUpload(formData).subscribe(
      (events: any) => {
        console.log(events);
        if (events.type === HttpEventType.Response) {
         
          this.uploading = false;
          this.logo = events.body.filename;
        
          this.cdr.detectChanges();
          // this.updateImage(events.body.filename,is_logo);
        }
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }

  updateImage(filename) {
    let data = {
      filename
    }
    this.cs.httpRequest('patch','user/update-image',data).subscribe(
      (res: any) => {
        console.log(res);
        this.cs.showSuccessMsg('Image Uploaded Successfully', 'Uploaded');
       
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }

  saveLogoInfo() {
    let data = {
      image:  this.logo,
      name: this.title
    }
    this.cs.httpRequest('patch','admin/update-logo-info',data).subscribe(
      (res: any) => {
        console.log(res);
        let data = {
          img: this.logo,
          is_profile_pic:false,
          is_logo:true,
          title:this.title
        }
        this.cs.updateProfilePic(data);
        this.cs.showSuccessMsg('Logo Updated Successfully', 'Updated');
       
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }
}
