import { CommonService } from './../../services/common.service';
import { Component, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Contact } from '../../models/contact.model';
import { ExcelService } from '../../services/excel/excel.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  totalCount: any;
  page: number = 1;
  activeCustomers = 'Loading...';
  deactivatedCustomers = 'Loading...';
  inactiveCustomers = 'Loading...';
  totalCustomers = 'Loading...';
  totalPages: number;
  search = '';
  searchUpdate = new Subject<string>();
  subscriptions: Subscription[] = [];
  users: Contact[] = [];
  exportContacts: Contact[] = [];

  selected_user = [];
  all_users_email_ids = [];

  constructor(private cs: CommonService, private excelSrv: ExcelService) {
    let subs = this.searchUpdate
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe((value) => {
        this.fetchUsers();
      });
    this.subscriptions.push(subs);
  }
  ngOnInit(): void {
    this.fetchCounts();
    this.fetchUsers(this.page);
    this.fetchAllUsers();
    this.getNotifications();
    // for (let index = 0; index < 10; index++) {
    //   const user = new Contact();
    //   this.users = faker.user.list;

    //   this.exportContacts.push(user);
    // }
  }

  fetchAllUsers() {
    this.cs
      .httpRequest(
        'get',
        `user/all-user-emails`
      )
      ?.subscribe(
        (res: any) => {
          console.log(res);
          this.all_users_email_ids = res.list;

          //  for (let item in data) {
          //     // data[item].bindName =
          //     //   data[item].firstName + " " + data[item].lastName;
          //     this.all_users_email_ids.push(data[item].email);
          // }
         
        },
        (err) => this.cs.handleError(err)
      );
  }
  

  fetchUsers(page = 1) {
    this.cs
      .httpRequest(
        'get',
        `user/list?search=${this.search}&page=${page}&limit=10`
      )
      ?.subscribe(
        (res: any) => {
          this.users = res.list;
          this.totalCount = res.total;
          // this.data = this.users;
          this.totalPages = Math.ceil(this.totalCount / 10);
        },
        (err) => this.cs.handleError(err)
      );
  }
  fetchPage(page) {
    this.page = page == 1 ? this.page + 1 : this.page - 1;
    this.fetchUsers(this.page);
  }

  updateUser(user) {
    this.cs
      .httpRequest('patch', `user/edit/${user.id}`, {
        isActive: user.isActive ? true : false,
      })
      ?.subscribe(
        (res: any) => {
          this.cs.showSuccessMsg('Updated', res.message);
          this.fetchCounts();
        },
        (err) => this.cs.handleError(err)
      );
  }
  fetchCounts() {
    this.cs.httpRequest('get', 'dashboard')?.subscribe(
      (res: any) => {
        this.activeCustomers = res.activeCustomers;
        this.deactivatedCustomers = res.deactivatedCustomers;
        this.inactiveCustomers = res.inactiveCustomers;
        this.totalCustomers = res.totalCustomers;
      },
      (err) => {
        this.activeCustomers = 'N/A';
        this.deactivatedCustomers = 'N/A';
        this.inactiveCustomers = 'N/A';
        this.totalCustomers = 'N/A';
        this.cs.handleError(err);
      }
    );
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      console.log(e);
      
      const bstr: string = e.target.result;
      console.log(bstr);
      const data = <any[]>this.excelSrv.importFromFile(bstr);
      console.log(data);
      const header: string[] = Object.getOwnPropertyNames(new Contact());
      // const header: string[] = Object.getOwnPropertyNames(data[0]);
      // const importedData = data.slice(1, -1);
      const importedData = data.slice(1);

      console.log('header',header);
      console.log('data',data[0]);
      let header_arr = ['firstName', 'lastName', 'email', 'password', 'phoneNumber', 'isActive'];
      let is_column_exist = true;
      data[0].forEach(header => {
        if(is_column_exist) {
        is_column_exist = header_arr.includes(header);
        }
      });
      console.log('is_column_exist',is_column_exist);
      
      if(!is_column_exist) {
        this.cs.showErrorMsg('Invalid File', 'Please import valid File');
        return
      } 
      let csv_users = data.map((arr,k) => {
        const obj = {};
        for (let i = 0; i < header.length; i++) {
          const k = header[i];
        
          if(k == 'isActive') {
            console.log('header',k);
            console.log('arr',arr[i]);
            arr[i] =  arr[i] =='Active' ? 1 : 0;
          }
          
          obj[k] = arr[i];
         
        }
       
        
        return <Contact>obj;
      });
     
        console.log('this.all_users_email_ids',this.all_users_email_ids);
        csv_users =   csv_users.slice(1);
        let exist_users = [];
        let email_ids = [];
        let is_duplicate_email = false;
        this.all_users_email_ids.forEach(user => {
          csv_users.forEach(e => {
            if(e.email == user.email) {
              exist_users.push(e.email)
            }
          });
        });
        console.log('exist_users',exist_users);
        exist_users.forEach(email => {
          const indexOfObject = csv_users.findIndex(object => {
            return object.email === email;
          });
          csv_users.splice(indexOfObject, 1);
        });
        console.log('csv_users',csv_users);
        if(!csv_users.length) {
          this.cs.showErrorMsg('Duplicate Users', 'All users already exist');
          return
        }else {
          var valueArr = csv_users.map((item) =>{ return item.email });
          is_duplicate_email = valueArr.some((item, idx) =>{ 
              return valueArr.indexOf(item) != idx 
          });
        }
        if(is_duplicate_email) {
          this.cs.showErrorMsg('Duplicate Emails', 'Remove duplicate email id');
          return
        }
        
      
      // return;
      console.log('csv_users',csv_users);
      console.log('is_duplicate_email',is_duplicate_email);
      
      this.addByCSV(csv_users);
      console.log('importedData',importedData);
      
    };
    reader.readAsBinaryString(target.files[0]);
  }

  exportData(tableId: string) {

   
   console.log(this.pushToArray(this.users));
   
  //  return
    let data:any = this.selected_user.length ? this.pushToArray(this.selected_user) : this.pushToArray(this.users)
    // this.excelSrv.exportToFile('users_data', tableId);
    this.excelSrv.exportAsExcelFile(data,'users_list');
  }
  pushToArray = (arr = []) => {
    const result = arr.reduce((acc, obj,i) => {
      // console.log(i);
      // console.log(acc);
      // console.log('obj',obj);
      const is_exist =  this.selected_user.includes(obj.id);
      
     delete obj['resetPasswordToken'];
     delete obj['salt'];
     delete obj['updatedAt'];
     delete obj['profilePic'];
     delete obj['password'];
     delete obj['isNotify'];
     delete obj['deletedAt'];
    //  delete obj['createdAt'];
    //  delete obj['id'];
     obj.id = i+1;
    
     obj.firstName =  obj.firstName;
     obj.isActive = obj.isActive ? 'Active':'Inactive';
     obj.createdAt = new Date(obj.createdAt);
     acc.push(obj);
    
    // acc.push(obj.firstName);
    return acc;
 }, []);
    return result;
 };

 selectUser(e,user) {
   console.log(e.target.checked );
   console.log(user);

   if(e.target.checked) {
     this.selected_user.push(user);
   }else {
    this.selected_user = this.selected_user.filter((ele) =>{ 
      return ele.id != user.id; 
    });
   }

   console.log( this.selected_user);
   
   
 }

  getNotifications() {
    this.cs.httpRequest('get', 'notifications/user-notifications').subscribe((res:any) => {
      console.log(res);
      this.cs.updateApprovalMessage(res.unread_msg);
    })
  }


  addByCSV(csv_users) {
    this.cs.httpRequest('post', 'user/add-by-csv', csv_users)?.subscribe(
      (res) => {
        this.fetchCounts();
        this.fetchUsers(1);
        this.fetchAllUsers();
        this.cs.showSuccessMsg('Added', 'Users added successfully');
      },
      (err) => this.cs.handleError(err)
    );
  }
}
