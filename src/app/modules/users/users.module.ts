import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Angular2CsvModule } from 'angular2-csv';

@NgModule({
  declarations: [
    UsersComponent,
    AddComponent,
    EditComponent
  ],
  imports: [
    SharedModule,
    Angular2CsvModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
