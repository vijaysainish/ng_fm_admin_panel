import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from './../../../services/common.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import SignaturePad from 'signature_pad';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  signaturePad: SignaturePad;
  @ViewChild('canvas') canvasEl: ElementRef;
  signatureImg: string;


  showPass: boolean = false;
  userId: any;
  userForm: FormGroup;
  isActive: boolean = false;
  isDeleted: any;
  isFormSubmitted = false;
  constructor(
    private fb: FormBuilder,
    private cs: CommonService,
    private route: ActivatedRoute,
    private modalService: NgbModal
  ) {
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: [''],
      email: [''],
      password: [''],
      phoneNumber: [''],
      role: ['', Validators.required],
    });
    this.route.params.subscribe((res) => {
      this.userId = res.id;
      this.fetchUserDetails();
    });
  }
ngOnInit(): void {
    
}
// ngAfterViewInit() {
//   this.signaturePad = new SignaturePad(this.canvasEl.nativeElement);
// }

  startDrawing(event: Event) {
    console.log(event);
    // works in device not in browser

  }

  moved(event: Event) {
    // works in device not in browser
  }

  clearPad() {
    this.signaturePad.clear();
  }

  savePad() {
    const base64Data = this.signaturePad.toDataURL();
    this.signatureImg = base64Data;
  }

  updateUser() {
    this.isFormSubmitted = true;
    this.userForm.patchValue({ firstName: (this.userForm.controls.firstName.value).trim() });
    if (this.userForm.status === 'INVALID' || this.userForm.controls.firstName.value == '') return;
    let userDetails = {
      firstName: this.userForm.controls.firstName.value,
      lastName: (this.userForm.controls.lastName.value).trim(),
      phoneNumber: this.userForm.controls.phoneNumber.value + '',
      profilePic: null,
      isAdmin: this.userForm.controls.role.value === 'User' ? false : true,
      isActive: this.isActive ,
      isDeleted: this.isDeleted ? 'true' : 'false',
    };
    this.cs.httpRequest('patch', `user/edit/${this.userId}`, userDetails)?.subscribe(
      (res: any) => {
        this.cs.showSuccessMsg('Updated', res.message);
      },
      (err) => this.cs.handleError(err)
    );
  }

  fetchUserDetails() {
    this.cs.httpRequest('get', `user/d/${this.userId}`)?.subscribe(
      (res: any) => {
        this.userForm.setValue({
          email: res.email,
          firstName: res.firstName,
          lastName: res.lastName,
          password: null,
          phoneNumber: res.phoneNumber || null,
          role: res.role || 'User',
        });
        this.isActive = res.isActive || false;
        this.isDeleted = res.deletedAt ? true : false;
      },
      (err) => this.cs.handleError(err)
    );
  }

  showSignature(content) {
    this.modalService.open(content);
  }
}
