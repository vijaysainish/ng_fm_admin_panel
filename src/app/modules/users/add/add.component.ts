import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})
export class AddComponent implements OnInit {
  showPass: boolean = false;
  userForm: FormGroup;
  isFormSubmitted = false;
  subscriptions:Subscription[];

  constructor(private cs: CommonService, private fb: FormBuilder) {
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: [''],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required, Validators.minLength(8),
      Validators.maxLength(20)],
      phoneNumber: [''],
      role: ['1'],
      status: ['1'],
    });
  }
  submitUser() {
    this.isFormSubmitted = true;
    console.log(this.userForm.status);
    // return;
    this.userForm.patchValue({ firstName: (this.userForm.controls.firstName.value).trim() });
    if (this.userForm.status === 'INVALID' || this.userForm.controls.firstName.value == '') return;
    let userDetails = {
      firstName: this.userForm.controls.firstName.value,
      lastName: (this.userForm.controls.lastName.value).trim(),
      phoneNumber: this.userForm.controls.phoneNumber.value + '',
      email: this.userForm.controls.email.value,
      password: this.userForm.controls.password.value,
      isAdmin: this.userForm.controls.role.value === '1' ? false : true,
      isActive: this.userForm.controls.status.value == '1' ? true : false,
    };
let getSub=    this.cs.httpRequest('post', 'user/add', userDetails)?.subscribe(
      (res) => {
        this.cs.showSuccessMsg('Added', 'User added successfully');
        this.userForm.reset();
        this.userForm.setValue({
          status:'1',
          firstName:'',
          lastName:'',
          email:'',
          password:'',
          phoneNumber:"",
          role:'1',
        })
        this.isFormSubmitted = false
        this.subscriptions.unshift(getSub);

      },
      (err) => this.cs.handleError(err)
    );
  }
  ngOnInit(): void {}
}
