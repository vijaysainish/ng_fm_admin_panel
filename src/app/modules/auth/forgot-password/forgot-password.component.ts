import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { imgBaseUrl } from 'src/app/Constants/constants';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  resetForm: FormGroup;
  mailSent: boolean =false;
  requested: boolean=false;
   email:string="";
   isAdmin: boolean = false;
   imgBaseUrl = imgBaseUrl;
   logo:any;

  constructor(		private fb: FormBuilder,
		public modalService: NgbModal,
		private cs: CommonService
) { 
	this.isAdmin = this.cs.isAdmin();
	// let token = this.cs.getCookie('token');

    this.resetForm = this.fb.group({
			email: [
				"",
				Validators.compose([Validators.required, Validators.email]),
			],
		});
		console.log(this.resetForm)
  }

  ngOnInit(): void {
	  this.fetchLogo();
  }
  
	resetPass() {
		this.mailSent = false;
		this.requested = true;
		this.email=""
	 
     this.cs.httpRequest("post", "auth/forgot-password",this.resetForm.value)?.subscribe(
				(res:any) => {
					this.cs.showSuccessMsg('Success', res.message);
					
					// if(this.isAdmin){
					// 	this.cs.navigate('/admin/login');
					// }
					// else 		
					// if(!this.isAdmin){
					// 	this.cs.navigate('/auth/login');
					// }
						
					this.mailSent = true;
		   this.requested = false;
				
			},
				(err) => {
					this.requested = false;
					this.cs.showErrorMsg(
						err.error.message,
						"Error Occured"
					);
				}
			);
	}


	fetchLogo() {
		this.cs.httpRequest('get', `admin/logo`)?.subscribe(
		  (res: any) => {
		   this.logo = res.image;
		   
		  },
		  (err) => this.cs.handleError(err)
		);
	  }

}
