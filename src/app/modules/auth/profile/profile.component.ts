import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { imgBaseUrl } from 'src/app/Constants/constants';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  registerForm: FormGroup;
  requested: boolean = false;
  isAdmin: boolean= false;
  showPass2: boolean = false;
  logo:any;
  imgBaseUrl = imgBaseUrl;


  constructor(private fb: FormBuilder, private cs: CommonService) {
    this.cs.setTitle();
    this.isAdmin = this.cs.isAdmin();

    this.registerForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
        ]),
      ],
      firstName: [
        '',
        Validators.compose([
          Validators.required,
          this.cs.noWhitespaceValidator,
        ]),
      ],
      lastName: [''],
      isAdmin: false,
    });
  }

  ngOnInit(): void {
    this.fetchLogo();
  }
  register() {
    this.requested = true;
    let creds = this.registerForm.value;
    this.cs.httpRequest('post', `auth/admin/register`, creds)?.subscribe(
      (res: any) => {
        this.requested = false;
        this.cs.showSuccessMsg('Signed up', 'Signed Up Successfully');
        localStorage.setItem('debug', 'false');
        this.cs.navigate('/auth/login');
      },
      (err) => {
        this.requested = false;
        this.cs.showErrorMsg('Ooops!', err.error.message);
      }
    );
  }

  fetchLogo() {
    this.cs.httpRequest('get', `admin/logo`)?.subscribe(
      (res: any) => {
       this.logo = res.image;
       
      },
      (err) => this.cs.handleError(err)
    );
  }
}
