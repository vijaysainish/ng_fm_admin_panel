import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { imgBaseUrl } from 'src/app/Constants/constants';
@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  loginForm: FormGroup;
  requested: boolean = false;

  imgBaseUrl = imgBaseUrl;
  logo:any;

  constructor(private fb: FormBuilder, private cs: CommonService) {
    this.cs.setTitle();
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(8)]),
      ],
    });
    this.fetchLogo();
  }

  ngOnInit(): void {}
  login() {
    this.requested = true;
    let creds = this.loginForm.value;
    this.cs.httpRequest('post', `auth`, creds)?.subscribe(
      (res: any) => {
        this.requested = false;
        if(res.user.role == 'Admin') {
        this.cs.setCookie('token', res.accessToken);
        this.cs.setStrCookie('user', res.user);
        this.cs.showSuccessMsg('Logged In', 'Admin Logged in Successfully');
        localStorage.setItem('debug', 'false');
        this.cs.navigate('/dashboard');
        console.log(res.accessToken);
      }
      else {
        this.cs.showErrorMsg('Ooops!', 'No user found');
      }
      },
      (err) => {
        this.requested = false;
        this.cs.showErrorMsg('Ooops!', err.error.message);
      }
    );
  }

  fetchLogo() {
    this.cs.httpRequest('get', `admin/logo`)?.subscribe(
      (res: any) => {
       this.logo = res.image;
       
      },
      (err) => this.cs.handleError(err)
    );
  }
}
