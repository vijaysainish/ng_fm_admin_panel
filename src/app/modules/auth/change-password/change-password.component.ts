import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { CommonService } from 'src/app/services/common.service';
import { imgBaseUrl } from 'src/app/Constants/constants';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
})
export class ChangePasswordComponent implements OnInit {
  requested: boolean = false;
  showNewPass: boolean = false;
  showCurrentPass: boolean = false;
  showPass: boolean = false;
  showPass2: boolean = false;
  confirmPass: FormGroup
  token: any = "";

  imgBaseUrl = imgBaseUrl;
  logo:any;
  
  constructor(private fb: FormBuilder, private cs: CommonService, private route: ActivatedRoute,   private cookie: CookieService,
    )
     {
    this.route.paramMap.subscribe((res)=>{
    console.log('res',res);
   
    
      this.token= res.get('token');
      console.log('res',this.token);
    })
    this.confirmPass = this.fb.group({
      "password": ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      "password_confirmation": ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    }, {
      validators: this.cs.matchPass('password', 'password_confirmation')
    })

  }

  ngOnInit(): void {
    this.fetchLogo();
  }
  submit() {
    this.requested = true;
    const {
      password,
      password_confirmation
    } = this.confirmPass.value
    let send = {
      password,
      password_confirmation,
      resetToken: this.token
    }
    console.log(send);
    this.cs.httpRequest('patch', 'auth/reset-password', send).subscribe((res:any) => {
      this.cs.navigate('/auth/login')
      this.cs.showSuccessMsg(res.message, 'Success');
       
      this.requested = false
    }, (err) => {
      this.cs.handleError(err)
      this.requested = false
      // if (err.status == 401) return this.cs.unAuthorized();
      // if (err.status == 404)
      //   return this.cs.showErrorMsg(err.error.message, 'Error');
      // if (err.status == 500)
      //   return this.cs.showErrorMsg(err.error.message, 'Error');
      // this.cs.showErrorMsg(err.error.message, 'Error');
   
    })
  }

  fetchLogo() {
		this.cs.httpRequest('get', `admin/logo`)?.subscribe(
		  (res: any) => {
		   this.logo = res.image;
		   
		  },
		  (err) => this.cs.handleError(err)
		);
	  }
}
