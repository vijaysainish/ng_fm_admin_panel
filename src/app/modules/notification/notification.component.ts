import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { baseUrl,imgBaseUrl } from '../../Constants/constants';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  message:string="";
  is_msg_error:boolean;
  page: number = 1;
  total:any;
  constructor(  private cs: CommonService) {
   
   }
notifications:any = [];
user_details: any;
imgBaseUrl = imgBaseUrl;
  ngOnInit(): void {
    
    this.fetchUserDetails();
    this.getNotifications();
  }

  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
        console.log(res);
        this.user_details = res;
       
      },
      (err) => this.cs.handleError(err)
    );
  }
  fetchPage(page) {
    this.page = page == 1 ? this.page + 1 : this.page - 1;
    this.getNotifications(this.page);
  }
  getNotifications(page = 1) {
   
    this.cs.httpRequest('get', `notifications/user-notifications?page=${this.page}&limit=10`).subscribe((res:any) => {
      console.log(res);
      this.notifications = res.list;
      this.total = res.total;
      this.total = Math.ceil(this.total / 10);
      this.readAllNotifications();
     
    })
  }
  readAllNotifications() {
    this.cs.httpRequest('patch', 'notifications/read-all').subscribe((res:any) => {
      console.log(res);
      this.cs.updateApprovalMessage(0);
     
    })
  }

  updateFileStatus(data,status) {
    console.log(data);
   
    if(!this.message && status == 2) {
      this.is_msg_error = true;
      return
    }else {
      data.request_sent = 0;
      this.is_msg_error = false;
    let req = {
      notification_id:data.id,
      file_id:data.file_id,
      status,
      initiator_id:data.send_by_user.id,
      message:this.message
    }
    console.log(req);

    this.cs.httpRequest('post', 'file-management/update-file-request-status',req).subscribe((res:any) => {
      console.log(res);
      this.cs.showSuccessMsg('Request has been sent to initiator', 'Requested')
     
    })
  }
    
    
  }

}
