import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  usersChart = {
    series: [
      {
        name: 'Active Users',
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66],
      },
      {
        name: 'Inactive Users',
        data: [76, 85, 101, 98, 87, 105, 91, 114, 94],
      },
    ],
    colors: ['#590202','#2f88fb'],
    chart: {
      type: 'bar',
      height: 350,
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '45%',
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: [
        "Feb`22",
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        
      ],
    },
    yaxis: {
      title: {
        text: 'No. of Users',
      },
    },
    fill: {
      opacity: 1,
    },
    tooltip: {
      y: {
        formatter: function (val) {
        
          
          return val;
        },
      },
    },
  };
  filesChart = {
    series: [
      {
        name: 'Signed Files',
        data: [23, 56, 77, 96, 11, 28, 33, 10, 40],
      },
      {
        name: 'Total Files Uploaded',
        data: [78, 55, 101, 38, 81, 125, 61, 124, 42],
      },
    ],
    colors: ['#590202','#23518c'],
    chart: {
      type: 'bar',
      height: 350,
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '45%',
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: [
        'Febs',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
      ],
    },
    yaxis: {
      title: {
        text: 'No. of Files',
      },
    },
    fill: {
      opacity: 1,
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return val;
        },
      },
    },
  };
  totalCustomers: string = 'Loading...';
  activeCustomers: string = 'Loading...';
  deactivatedCustomers: string = 'Loading...';
  inactiveCustomers: string = 'Loading...';
  isAdmin: boolean= false;
  inactiveFiles:any = 'Loading...';
  activeFiles: any = 'Loading...';
  totalFiles: any = 'Loading...';
  dashboard_data:any;
  constructor(private cs: CommonService) {}

   ngOnInit() {
    this.isAdmin = this.cs.isAdmin();
    this.fetchDashData();
    this.getNotifications();
  }

   fetchDashData() {
    this.cs.httpRequest('get', 'dashboard')?.subscribe(
      async (res: any) => {
        this.dashboard_data =res;
        this.totalCustomers = res.totalCustomers;
        this.activeCustomers = res.activeCustomers;
        this.inactiveCustomers = res.inactiveCustomers;
        this.deactivatedCustomers = res.deactivatedCustomers;
        this.totalFiles = res.total_files? res.total_files.total_files_count : 0;
        this.activeFiles = res.active_files? res.active_files.active_files_count : 0;
        this.inactiveFiles = (this.totalFiles - this.activeFiles);
        // console.log(this.dashboard_data.all_users,'all_users');
        let active_user_count_arr = [];
        let inactive_user_count_arr = [];
        let month_year_arr = [];
        await this.dashboard_data.all_users.forEach(element => {
          console.log(element);
         const montharr =  element.month.split(',');
         const countarr =  element.count.split(',');
         const statusarr =  element.status.split(',');
          console.log(montharr);
          month_year_arr.push((montharr[0]).slice(0, 3) + '`' + (element.year).slice(-2));
          if(montharr.length == 1) {
            countarr.push(0);
            if(statusarr[0] == 1) {
              statusarr.push(0);
            }else {
              statusarr.push(1);
            }
          }
          active_user_count_arr.push(countarr[0]);
          inactive_user_count_arr.push(countarr[1]);
          console.log(countarr);
          console.log(statusarr);
          
          
          
        });


        let completed_files_count_arr = [];
        let pending_files_count_arr = [];
        let file_month_year_arr = [];
        await this.dashboard_data.all_files.forEach(element => {
          console.log(element);
         const montharr =  element.month.split(',');
         const countarr =  element.count.split(',');
         const statusarr =  element.status.split(',');
          console.log(montharr);
          file_month_year_arr.push((montharr[0]).slice(0, 3) + '`' + (element.year).slice(-2));
          if(montharr.length == 1) {
            countarr.push(0);
            if(statusarr[0] == 'Completed') {
              statusarr.push('Pending');
            }else {
              statusarr.push('Completed');
            }
          }
          completed_files_count_arr.push(countarr[1]);
          pending_files_count_arr.push(countarr[0]);
          console.log(countarr);
          console.log(statusarr);
          
          
          
        });
        console.log(active_user_count_arr);
        console.log(inactive_user_count_arr);
        console.log(month_year_arr);

        console.log(completed_files_count_arr);
        console.log(pending_files_count_arr);
        console.log(file_month_year_arr);

        this.usersChart.series = [
           
            {
              name: 'Inactive Users',
              data: inactive_user_count_arr,
            },
            {
              name: 'Active Users',
              data: active_user_count_arr,
            }
        ];
        this.usersChart.xaxis= {
          categories:month_year_arr
        }
        this.filesChart.series = [
           
          {
            name: 'Uploaded Files',
            data: pending_files_count_arr,
          },
          {
            name: 'Signed Files',
            data: completed_files_count_arr,
          }
      ];
      this.filesChart.xaxis= {
        categories:file_month_year_arr
      }
      },
      (err) => {
        console.log(err.statusCode);
        
        this.totalCustomers = 'N/A';
        this.activeCustomers = 'N/A';
        this.inactiveCustomers = 'N/A';
        this.deactivatedCustomers = 'N/A';
        this.cs.handleError(err);
      }
    );
  }

  getNotifications() {
    this.cs.httpRequest('get', 'notifications/user-notifications').subscribe((res:any) => {
      console.log(res);
      this.cs.updateApprovalMessage(res.unread_msg);
     
    })
  }
}
