import { Component, ViewChild, OnInit, Output, EventEmitter, ElementRef, AfterViewInit, ChangeDetectorRef, ViewChildren, QueryList, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import WebViewer, { WebViewerInstance } from '@pdftron/webviewer';
import { trigger, animate, transition, style, query } from '@angular/animations';

import PSPDFKit from "pspdfkit";


import { Subject } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';
import { HttpEventType } from '@angular/common/http';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgOption, NgSelectComponent } from '@ng-select/ng-select';
import { async } from 'rxjs/internal/scheduler/async';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  @ViewChild('viewer') viewer: ElementRef;
  @ViewChild('savebtn') savebtn: ElementRef<HTMLElement>;
  @ViewChildren(NgSelectComponent) selects: NgSelectComponent;
  @ViewChildren('selects') selects2: QueryList<any>;
  wvInstance: WebViewerInstance;
  @Output() coreControlsEvent: EventEmitter<string> = new EventEmitter();
  blob: any;
  selectedUser: any = [];
  other_signer_name: any = '';
  other_signer_email: any = '';

  selectedUser2: any;
  selectedUser3: any = [];
  users_list: any = [];
  saveBtn: HTMLButtonElement;
  start_date: Date;
  end_date: any;
  comment: any;

  selected_user_name: any = '';
  selected_user_email: any = '';
  selected_user_id: any;
  selected_rm_user_id: any;
  selected_rm_index: any;
  user_details: any;
  productForm: FormGroup;
  is_submit: boolean;
  is_form_submit: boolean;
  is_deleted_by_btn: boolean;
  filename: any;
  private documentLoaded$: Subject<void>;
  otherUserFormArr: FormGroup;
  angForm = new FormGroup({
    names: new FormArray([
      new FormControl('', Validators.required),
      new FormControl('', Validators.required),
    ])
  });
  pdf_current_page: number = 1;
  min_date: any;
  max_date: any;
  today_date: Date = new Date();

  is_add_other_user: boolean;
  is_save:boolean;
  is_annots_loaded:boolean;
  // @HostListener('document:click', ['$event'])

  selectedCar: number;

 
 
  albums = [
      {
        "userId": 1,
        "id": 1,
        "title": "quidem molestiae enim"
      },
      {
        "userId": 1,
        "id": 2,
        "title": "sunt qui excepturi placeat culpa"
      },
      {
        "userId": 1,
        "id": 3,
        "title": "omnis laborum odio"
      },
      {
        "userId": 1,
        "id": 4,
        "title": "non esse culpa molestiae omnis sed optio"
      },
      {
        "userId": 1,
        "id": 5,
        "title": "eaque aut omnis a"
      },
      {
        "userId": 1,
        "id": 6,
        "title": "natus impedit quibusdam illo est"
      },
      {
        "userId": 1,
        "id": 7,
        "title": "quibusdam autem aliquid et et quia"
      },
      {
        "userId": 1,
        "id": 8,
        "title": "qui fuga est a eum"
      },
      {
        "userId": 1,
        "id": 9,
        "title": "saepe unde necessitatibus rem"
      },
      {
        "userId": 1,
        "id": 10,
        "title": "distinctio laborum qui"
      },
      {
        "userId": 2,
        "id": 11,
        "title": "quam nostrum impedit mollitia quod et dolor"
      },
      {
        "userId": 2,
        "id": 12,
        "title": "consequatur autem doloribus natus consectetur"
      },
      {
        "userId": 2,
        "id": 13,
        "title": "ab rerum non rerum consequatur ut ea unde"
      },
      {
        "userId": 2,
        "id": 14,
        "title": "ducimus molestias eos animi atque nihil"
      },
      {
        "userId": 2,
        "id": 15,
        "title": "ut pariatur rerum ipsum natus repellendus praesentium"
      },
      {
        "userId": 2,
        "id": 16,
        "title": "voluptatem aut maxime inventore autem magnam atque repellat"
      },
      {
        "userId": 2,
        "id": 17,
        "title": "aut minima voluptatem ut velit"
      },
      {
        "userId": 2,
        "id": 18,
        "title": "nesciunt quia et doloremque"
      },
      {
        "userId": 2,
        "id": 19,
        "title": "velit pariatur quaerat similique libero omnis quia"
      },
      {
        "userId": 2,
        "id": 20,
        "title": "voluptas rerum iure ut enim"
      },
      {
        "userId": 3,
        "id": 21,
        "title": "repudiandae voluptatem optio est consequatur rem in temporibus et"
      },
      {
        "userId": 3,
        "id": 22,
        "title": "et rem non provident vel ut"
      },
      {
        "userId": 3,
        "id": 23,
        "title": "incidunt quisquam hic adipisci sequi"
      },
      {
        "userId": 3,
        "id": 24,
        "title": "dolores ut et facere placeat"
      },
      {
        "userId": 3,
        "id": 25,
        "title": "vero maxime id possimus sunt neque et consequatur"
      },
      {
        "userId": 3,
        "id": 26,
        "title": "quibusdam saepe ipsa vel harum"
      },
      {
        "userId": 3,
        "id": 27,
        "title": "id non nostrum expedita"
      },
      {
        "userId": 3,
        "id": 28,
        "title": "omnis neque exercitationem sed dolor atque maxime aut cum"
      },
      {
        "userId": 3,
        "id": 29,
        "title": "inventore ut quasi magnam itaque est fugit"
      },
      {
        "userId": 3,
        "id": 30,
        "title": "tempora assumenda et similique odit distinctio error"
      },
      {
        "userId": 4,
        "id": 31,
        "title": "adipisci laborum fuga laboriosam"
      },
      {
        "userId": 4,
        "id": 32,
        "title": "reiciendis dolores a ut qui debitis non quo labore"
      },
      {
        "userId": 4,
        "id": 33,
        "title": "iste eos nostrum"
      },
      {
        "userId": 4,
        "id": 34,
        "title": "cumque voluptatibus rerum architecto blanditiis"
      },
      {
        "userId": 4,
        "id": 35,
        "title": "et impedit nisi quae magni necessitatibus sed aut pariatur"
      },
      {
        "userId": 4,
        "id": 36,
        "title": "nihil cupiditate voluptate neque"
      },
      {
        "userId": 4,
        "id": 37,
        "title": "est placeat dicta ut nisi rerum iste"
      },
      {
        "userId": 4,
        "id": 38,
        "title": "unde a sequi id"
      },
      {
        "userId": 4,
        "id": 39,
        "title": "ratione porro illum labore eum aperiam sed"
      },
      {
        "userId": 4,
        "id": 40,
        "title": "voluptas neque et sint aut quo odit"
      },
      {
        "userId": 5,
        "id": 41,
        "title": "ea voluptates maiores eos accusantium officiis tempore mollitia consequatur"
      },
      {
        "userId": 5,
        "id": 42,
        "title": "tenetur explicabo ea"
      },
      {
        "userId": 5,
        "id": 43,
        "title": "aperiam doloremque nihil"
      },
      {
        "userId": 5,
        "id": 44,
        "title": "sapiente cum numquam officia consequatur vel natus quos suscipit"
      },
      {
        "userId": 5,
        "id": 45,
        "title": "tenetur quos ea unde est enim corrupti qui"
      },
      {
        "userId": 5,
        "id": 46,
        "title": "molestiae voluptate non"
      },
      {
        "userId": 5,
        "id": 47,
        "title": "temporibus molestiae aut"
      },
      {
        "userId": 5,
        "id": 48,
        "title": "modi consequatur culpa aut quam soluta alias perspiciatis laudantium"
      },
      {
        "userId": 5,
        "id": 49,
        "title": "ut aut vero repudiandae voluptas ullam voluptas at consequatur"
      },
      {
        "userId": 5,
        "id": 50,
        "title": "sed qui sed quas sit ducimus dolor"
      },
      {
        "userId": 6,
        "id": 51,
        "title": "odit laboriosam sint quia cupiditate animi quis"
      },
      {
        "userId": 6,
        "id": 52,
        "title": "necessitatibus quas et sunt at voluptatem"
      },
      {
        "userId": 6,
        "id": 53,
        "title": "est vel sequi voluptatem nemo quam molestiae modi enim"
      },
      {
        "userId": 6,
        "id": 54,
        "title": "aut non illo amet perferendis"
      },
      {
        "userId": 6,
        "id": 55,
        "title": "qui culpa itaque omnis in nesciunt architecto error"
      },
      {
        "userId": 6,
        "id": 56,
        "title": "omnis qui maiores tempora officiis omnis rerum sed repellat"
      },
      {
        "userId": 6,
        "id": 57,
        "title": "libero excepturi voluptatem est architecto quae voluptatum officia tempora"
      },
      {
        "userId": 6,
        "id": 58,
        "title": "nulla illo consequatur aspernatur veritatis aut error delectus et"
      },
      {
        "userId": 6,
        "id": 59,
        "title": "eligendi similique provident nihil"
      },
      {
        "userId": 6,
        "id": 60,
        "title": "omnis mollitia sunt aliquid eum consequatur fugit minus laudantium"
      },
      {
        "userId": 7,
        "id": 61,
        "title": "delectus iusto et"
      },
      {
        "userId": 7,
        "id": 62,
        "title": "eos ea non recusandae iste ut quasi"
      },
      {
        "userId": 7,
        "id": 63,
        "title": "velit est quam"
      },
      {
        "userId": 7,
        "id": 64,
        "title": "autem voluptatem amet iure quae"
      },
      {
        "userId": 7,
        "id": 65,
        "title": "voluptates delectus iure iste qui"
      },
      {
        "userId": 7,
        "id": 66,
        "title": "velit sed quia dolor dolores delectus"
      },
      {
        "userId": 7,
        "id": 67,
        "title": "ad voluptas nostrum et nihil"
      },
      {
        "userId": 7,
        "id": 68,
        "title": "qui quasi nihil aut voluptatum sit dolore minima"
      },
      {
        "userId": 7,
        "id": 69,
        "title": "qui aut est"
      },
      {
        "userId": 7,
        "id": 70,
        "title": "et deleniti unde"
      },
      {
        "userId": 8,
        "id": 71,
        "title": "et vel corporis"
      },
      {
        "userId": 8,
        "id": 72,
        "title": "unde exercitationem ut"
      },
      {
        "userId": 8,
        "id": 73,
        "title": "quos omnis officia"
      },
      {
        "userId": 8,
        "id": 74,
        "title": "quia est eius vitae dolor"
      },
      {
        "userId": 8,
        "id": 75,
        "title": "aut quia expedita non"
      },
      {
        "userId": 8,
        "id": 76,
        "title": "dolorem magnam facere itaque ut reprehenderit tenetur corrupti"
      },
      {
        "userId": 8,
        "id": 77,
        "title": "cupiditate sapiente maiores iusto ducimus cum excepturi veritatis quia"
      },
      {
        "userId": 8,
        "id": 78,
        "title": "est minima eius possimus ea ratione velit et"
      },
      {
        "userId": 8,
        "id": 79,
        "title": "ipsa quae voluptas natus ut suscipit soluta quia quidem"
      },
      {
        "userId": 8,
        "id": 80,
        "title": "id nihil reprehenderit"
      },
      {
        "userId": 9,
        "id": 81,
        "title": "quibusdam sapiente et"
      },
      {
        "userId": 9,
        "id": 82,
        "title": "recusandae consequatur vel amet unde"
      },
      {
        "userId": 9,
        "id": 83,
        "title": "aperiam odio fugiat"
      },
      {
        "userId": 9,
        "id": 84,
        "title": "est et at eos expedita"
      },
      {
        "userId": 9,
        "id": 85,
        "title": "qui voluptatem consequatur aut ab quis temporibus praesentium"
      },
      {
        "userId": 9,
        "id": 86,
        "title": "eligendi mollitia alias aspernatur vel ut iusto"
      },
      {
        "userId": 9,
        "id": 87,
        "title": "aut aut architecto"
      },
      {
        "userId": 9,
        "id": 88,
        "title": "quas perspiciatis optio"
      },
      {
        "userId": 9,
        "id": 89,
        "title": "sit optio id voluptatem est eum et"
      },
      {
        "userId": 9,
        "id": 90,
        "title": "est vel dignissimos"
      },
      {
        "userId": 10,
        "id": 91,
        "title": "repellendus praesentium debitis officiis"
      },
      {
        "userId": 10,
        "id": 92,
        "title": "incidunt et et eligendi assumenda soluta quia recusandae"
      },
      {
        "userId": 10,
        "id": 93,
        "title": "nisi qui dolores perspiciatis"
      },
      {
        "userId": 10,
        "id": 94,
        "title": "quisquam a dolores et earum vitae"
      },
      {
        "userId": 10,
        "id": 95,
        "title": "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
      },
      {
        "userId": 10,
        "id": 96,
        "title": "unde et ut molestiae est molestias voluptatem sint"
      },
      {
        "userId": 10,
        "id": 97,
        "title": "est quod aut"
      },
      {
        "userId": 10,
        "id": 98,
        "title": "omnis quia possimus nesciunt deleniti assumenda sed autem"
      },
      {
        "userId": 10,
        "id": 99,
        "title": "consectetur ut id impedit dolores sit ad ex aut"
      },
      {
        "userId": 10,
        "id": 100,
        "title": "enim repellat iste"
      }
    ];


    heroForm: FormGroup;
    // dropdown = false;

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private cs: CommonService,
    private fb: FormBuilder,
    public cdr: ChangeDetectorRef
  ) {
    this.fetchUsers();
    this.documentLoaded$ = new Subject<void>();
    let sblob = localStorage.getItem('blob');
    console.log('sblob', sblob);

    this.min_date = this.today_date.toISOString().split('T')[0];
    this.max_date = this.today_date.toISOString().split('T')[0];

    if (this.router.getCurrentNavigation().extras.state) {
      console.log(this.router.getCurrentNavigation().extras.state.data);
      let file: any = this.router.getCurrentNavigation().extras.state.data[0];
      this.filename = file.name;
      // localStorage.setItem('file',JSON.stringify(this.router.getCurrentNavigation().extras.state.data))
      file.arrayBuffer().then((arrayBuffer) => {
        const blob = new Blob([new Uint8Array(arrayBuffer)], { type: file.type });
        console.log(blob);
        const documentBlobObjectUrl = URL.createObjectURL(blob);
        console.log(documentBlobObjectUrl);
        console.log('arrayBuffer', arrayBuffer);
        this.uploadWebViewver(documentBlobObjectUrl);
        // this.uploadPSPPSFFile(documentBlobObjectUrl);

      });
    } else {
      this.uploadWebViewver(sblob);
      // this.uploadPSPPSFFile(sblob);
    }

    // this.addOtherUser();
    this.fetchUserDetails();

    this.productForm = this.fb.group({
      name: '',
      signer_emails: this.fb.array([]),
    });
    
    this.heroForm = this.fb.group({
      album: '',
  });
   
  }
  ngOnInit(): void {
    this.otherUserFormArr = this.fb.group({
      otherUserForm: new FormArray([]),
    });
    this.addOtherUser()
   
    this.addSigner()
  }
  get names(): FormArray {
    return this.angForm.get('names') as FormArray;
  }
  onFormSubmit(): void {
    for (let i = 0; i < this.names.length; i++) {
      console.log(this.names.at(i).value);
    }
  }
  addNameField() {
    this.names.push(new FormControl('', Validators.required));
  }

  deleteNameField(index: number) {
    if (this.names.length !== 1) {
      this.names.removeAt(index);
    }
    console.log(this.names.length);
  }
  get otherFields() {
    // console.log(this.otherUserFormArr.value)
    return this.otherUserFormArr.get("otherUserForm") as FormArray
  }
  otherField(): FormArray {
    return this.otherUserFormArr.get("otherUserForm") as FormArray
  }
  getControls() {
    return (this.otherUserFormArr.get('otherUserForm') as FormArray).controls;
  }

  newOtherField(): FormGroup {
    return this.fb.group({
      name: ["", [Validators.required]],
      email: ["", [Validators.required]],
    })
  }

  addOtherUser() {


    console.log(this.otherField());
    console.log(this.newOtherField());
    console.log(this.newOtherField().value);
    // console.log(this.getControls());

    this.otherField().push(this.newOtherField());
    this.otherUserFormArr.value
  }

  removeOtherUser(i: number) {
    this.otherField().removeAt(i);
  }
  openComment(content) {
    this.modalService.open(content, { centered: true });
  }
  view() {
    this.router.navigateByUrl('/file-management/upload/comment');
    this.modalService.dismissAll();
  }

  ngAfterViewInit() {
    // WebViewer({
    //   path: './../../../../assets/lib',
    //   initialDoc: './../../../../assets/files/webviewer-demo-annotated.pdf'
    // }, document.getElementById('viewer'))
    //   .then(instance => {
    //     const { UI, Core } = instance;
    //     const { documentViewer, annotationManager, Tools, Annotations } = Core;
    //     // call methods from UI, Core, documentViewer and annotationManager as needed

    //     documentViewer.addEventListener('documentLoaded', () => {
    //       // call methods relating to the loaded document
    //     });

    //     instance.UI.loadDocument('http://fileserver/documents/test.pdf');
    //   })


  }

  uploadWebViewver(documentBlobObjectUrl) {
    WebViewer({
      path: './../../../../assets/lib',
      initialDoc: documentBlobObjectUrl
    }, document.getElementById('viewer'))
      .then(instance => {


        const { UI, Core } = instance;
        const { documentViewer, annotationManager, Tools, Annotations } = Core;
        // call methods from UI, Core, documentViewer and annotationManager as needed
        instance.UI.disableElements(this.disableElementsForAdmin());

        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Z)
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Y)

        // const signatureTool = documentViewer.getTool('AnnotationCreateSignature');

        // signatureTool.addEventListener('locationSelected', (x) => {
        //   signatureTool.setSignature(base64Image);
        //   signatureTool.addSignature();
        //   instance.UI.closeElements(['signatureModal']);
        // });
        // instance.UI.setColorPalette({
        //   toolNames: [Tools.ToolNames.FREETEXT],
        //   colors: [
        //     '#DDDDDD',
        //     '#9de8e8',
        //     '#A6A1E6',
        //     '#E2A1E6',
        //     '#EF1234',
        //     '#FF8D00',
        //     '#FFCD45',
        //   ],
        // });
        instance.UI.setZoomLevel(0.4);
        // create a form field
        const { WidgetFlags } = Annotations;

        annotationManager.setAnnotationDisplayAuthorMap((userId) => {
          return this.user_details ? this.user_details.firstName + ' ' + this.user_details.lastName : '';
          if (userId === '1') {
            return 'Will Ricker';
          } else if (userId === '2') {
            return 'Jean-Luc Picard'
          } else {
            return 'Guest';
          }
        });
        // const displayAuthor = annotationManager.getDisplayAuthor(annotation.Author);
        // add_annot_btn is your own custom button
        document.getElementById('rm_annot_btn').addEventListener('click', () => {
          const annots = annotationManager.getAnnotationsList();
          console.log(annots);

          this.is_deleted_by_btn = true;
          const text_widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
          const sign_widgetAnnots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
          console.log(text_widgetAnnots);
          text_widgetAnnots.forEach((element, i) => {
            // remove annotations
            console.log((this.selected_rm_user_id).length);
            console.log((element));

            // if (this.selected_rm_user_id.toString().trim() == element.Id) {
            //   console.log('remove id', element.Id);
            //   let rm_annot = []
            //   rm_annot.push(text_widgetAnnots[i])
            //   annotationManager.deleteAnnotations(rm_annot);
            // }
            if (this.selected_rm_index == i) {
              console.log('remove id', element.Id);
              let rm_annot = []
              rm_annot.push(text_widgetAnnots[i])
              annotationManager.deleteAnnotations(rm_annot);
            }
          });
          sign_widgetAnnots.forEach((element, i) => {
            // remove annotations
            // if (this.selected_rm_user_id.toString().trim() == element.Id) {
            //   console.log('remove id', element.Id);
            //   let rm_annot = []
            //   rm_annot.push(sign_widgetAnnots[i])
            //   annotationManager.deleteAnnotations(rm_annot);
            // }
            if (this.selected_rm_index == i) {
              console.log('remove id', element.Id);
              let rm_annot = []
              rm_annot.push(sign_widgetAnnots[i])
              annotationManager.deleteAnnotations(rm_annot);
            }
          });

        });

        // add_annot_btn is your own custom button
        document.getElementById('add_annot_btn').addEventListener('click', () => {

          //  const sign_here = new Annotations.SignatureWidgetAnnotation()
          // const flags = new WidgetFlags();
          // flags.set('Required', true);
          const sign_here_field = new Annotations.Forms.Field("some signature field name", {
            type: 'Sig',
            // flags,
          });
          // create a widget annotation
          const widgetAnnot = new Annotations.SignatureWidgetAnnotation(sign_here_field, {
            appearance: '_DEFAULT',
            appearances: {
              _DEFAULT: {
                Normal: {
                  data:
                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuMWMqnEsAAAANSURBVBhXY/j//z8DAAj8Av6IXwbgAAAAAElFTkSuQmCC',
                  offset: {
                    x: 100,
                    y: 100,
                  },
                },
              },
            },
          });
          // set position and size
          widgetAnnot.PageNumber = this.pdf_current_page;
          widgetAnnot.X = 400;
          widgetAnnot.Y = 590;
          widgetAnnot.Width = 100;
          widgetAnnot.Height = 50;
          widgetAnnot.ReadOnly = true;
          // widgetAnnot.Author = this.user_details.firstName + ' ' + this.user_details.lastName
          

          // widgetAnnot.ReadOnly = false;
          // widgetAnnot.NoMove = false
          // widgetAnnot.setcu = false
         let time_id = Date.now();
          widgetAnnot.Id = this.selectedUser[this.selectedUser.length - 1].id +'_'+time_id;
          if(typeof this.selectedUser[this.selectedUser.length - 1].id != 'number'){
            widgetAnnot.setCustomData('user_id', (this.selectedUser[this.selectedUser.length - 1].id).trim());
            widgetAnnot.setCustomData('time_id', time_id.toString());
          }else {
            widgetAnnot.setCustomData('user_id', this.selectedUser[this.selectedUser.length - 1].id);
            widgetAnnot.setCustomData('time_id', time_id.toString());
          }

          //add the form field and widget annotation
          annotationManager.getFieldManager().addField(sign_here_field);
          annotationManager.addAnnotation(widgetAnnot);
          annotationManager.drawAnnotationsFromList([widgetAnnot]);

          const freeText = new Annotations.FreeTextAnnotation();
          freeText.PageNumber = this.pdf_current_page;
          freeText.X = 300;
          freeText.Y = 600;
          freeText.Width = 300;
          freeText.Height = 150;
          freeText.StrokeThickness = 0;
          freeText.ReadOnly = false;

          // freeText.Color = new Annotations.Color(0, 255, 255);
          freeText.Id = this.selectedUser[this.selectedUser.length - 1].id+'_'+time_id;
          if(typeof this.selectedUser[this.selectedUser.length - 1].id != 'number'){
            freeText.setCustomData('user_id', (this.selectedUser[this.selectedUser.length - 1].id).trim());
            freeText.setCustomData('time_id', time_id.toString());
          }else {
            freeText.setCustomData('user_id', this.selectedUser[this.selectedUser.length - 1].id);
            freeText.setCustomData('time_id', time_id.toString());
          }
          freeText.TextColor = new Annotations.Color(0, 0, 0);

          // freeText.setPadding(new Annotations.Rect(0, 0, 0, 0));
          freeText.setContents('Signature: ' + '_____________' + `\n\n` + 'Name: ' + this.selected_user_name +
            `\n\n` + 'Email: ' + this.selected_user_email);
          // freeText.setContents(this.user_email);
          // freeText.FillColor = new Annotations.Color(0, 255, 255);
          freeText.FontSize = '18pt';
          // freeText.Author = this.user_details.firstName + ' ' + this.user_details.lastName;

          annotationManager.addAnnotation(freeText, { autoFocus: true, isUndoRedo: false });
          // annotationManager.re;
          annotationManager.redrawAnnotation(freeText);

         




          // const freeText2 = new Annotations.FreeTextAnnotation();
          // freeText2.PageNumber = 1;
          // freeText2.X = 300;
          // freeText2.Y = 750;
          // freeText2.Width = 150;
          // freeText2.Height = 50;
          // freeText2.StrokeThickness = 0;
          // freeText2.Color = new Annotations.Color(0, 255, 255);

          // freeText2.setContents(this.user_email);
          // freeText2.FontSize = '20pt';
          // annotationManager.addAnnotation(freeText2, { autoFocus: true });
          // // annotationManager.re;
          // annotationManager.redrawAnnotation(freeText2);
        });
        //  // add_annot_btn is your own custom button
        //  document.getElementById('add_annot_btn').addEventListener('click', () => {

        //    // set flags for multiline and required
        //    const flags = new WidgetFlags(1);
        //    flags.set('Multiline', true);
        //    flags.set('Required', true);

        //    // create a form field
        //    const field = new Annotations.Forms.Field("some text field name", {
        //      type: 'Tx',
        //      flags,
        //    });

        //    // create a widget annotation
        //    const widgetAnnot = new Annotations.TextWidgetAnnotation(field,1);

        //    // set position and size
        //    widgetAnnot.PageNumber = 1;
        //    widgetAnnot.X = 100;
        //    widgetAnnot.Y = 100;
        //    widgetAnnot.Width = 50;
        //    widgetAnnot.Height = 20;

        //    //add the form field and widget annotation
        //    annotationManager.getFieldManager().addField(field);
        //    annotationManager.addAnnotation(widgetAnnot);
        //    annotationManager.drawAnnotationsFromList([widgetAnnot]);
        //  });

        documentViewer.addEventListener('documentLoaded', () => {
          // call methods relating to the loaded document
        });
        documentViewer.addEventListener('pageNumberUpdated', (pageNumber) => {
          console.log(pageNumber, 'pageNumber');
          this.pdf_current_page = pageNumber;
          // here it's guaranteed that page {pageNumber} is fully rendered
          // you can get or set pixels on the canvas, etc
        })
        instance.UI.setHeaderItems(header => {
          const parent = documentViewer.getScrollViewElement().parentElement;

          const menu = document.createElement('div');
          menu.classList.add('Overlay');
          menu.classList.add('FlyoutMenu');
          menu.style.padding = '1em';

          const downloadBtn = document.createElement('button');
          downloadBtn.textContent = 'Download';
          downloadBtn.onclick = () => {
            // Download
          };

          menu.appendChild(downloadBtn);

          let isMenuOpen = false;

          const renderCustomMenu = () => {
            this.saveBtn = document.createElement('button');
            this.saveBtn.textContent = 'Save';
            this.saveBtn.id = 'savebtn';
            this.saveBtn.disabled = false;
            this.saveBtn.hidden = true;

            this.saveBtn.onclick = () => {
              console.log(55);
              console.log(annotationManager.getAnnotationsList());

              // return
              annotationManager.exportAnnotations().then(xfdfString => {
                console.log(44555);

                const doc = documentViewer.getDocument();
                doc.getFileData({ xfdfString }).then(async(data) => {
                  // console.log('aaaaaaaaaaaaaaa',data);
                  // return;
                  const arr = new Uint8Array(data);
                  const blob = new Blob([arr], { type: 'application/pdf' });
                  console.log(blob);
                  console.log(URL.createObjectURL(blob));
                  console.log(this.selectedUser);

                  let user_ids = this.selectedUser.map(u => u.id);
                  // distinct user id
                  let user_ids_mix = user_ids.filter((v, i, a) => a.indexOf(v) === i);
                  console.log(user_ids);
                  let db_users_ids = [];
                  let other_users_ids = [];
                  //seperate db and oher user ids
                
                  await user_ids.forEach(id => {
                    if (typeof id == 'number') {
                      let is_exist =  db_users_ids.includes(id);
                      if(!is_exist) {
                        db_users_ids.push(id);
                      }
                      
                    } else {
                      let is_exist =  other_users_ids.includes(id);
                      if(!is_exist) {
                        other_users_ids.push(id);
                      }
                    }
                  });
                  user_ids = db_users_ids.concat(other_users_ids);
                  console.log('user_ids',user_ids);
                  
                  // return;
                  let api_data = {
                    FileData: {
                      startDate: this.start_date,
                      endDate: this.end_date,
                      comment: this.comment
                    },
                    SharedUser: {
                      // user_ids: user_ids.join(),
                      user_ids: user_ids
                    }
                  }
                  //  this.addFile(file_user_data)

                  this.blobImage(blob, api_data);
                  // const formData = new FormData();
                  // formData.append('blob', blob);
                  // fetch(`/server/annotationHandler.js?filename=${filename}`, {
                  //   method: 'POST',
                  //   body: formData // written into a PDF file in the server
                  // });
                })
              });

              if (isMenuOpen) {
                parent.removeChild(menu);
              } else {
                menu.style.left = `${document.body.clientWidth - (this.saveBtn.clientWidth + 40)}px`;
                menu.style.right = 'auto';
                menu.style.top = '40px';
                parent.appendChild(menu);
              }

              isMenuOpen = !isMenuOpen;
            };

            return this.saveBtn;
          };

          const newCustomElement = {
            type: 'customElement',
            render: renderCustomMenu,
          };

          header.push(newCustomElement);
        });
        // var docViewer = documentViewer.docViewer;
        annotationManager.addEventListener('annotationChanged', (annotations, action, info) => {
          // return;
          //   let options:any = {
          //    xfdfString: documentViewer.getAnnotationManager().exportAnnotations()
          //   };
          //   console.log(annotationManager.exportAnnotations());
          //   let  bugg:any = annotationManager.exportAnnotations()
          //   const blob = new Blob([bugg], { type: "application/pdf" });
          //   console.log(blob);
          //  console.log(URL.createObjectURL(blob));

          //   let doc =documentViewer.getDocument();
          //   // let jfdslfk = JSON.stringify(doc);
          //   // let docww = JSON.parse(jfdslfk);
          //   console.log('doc',doc.getFileData(options));
          //   console.log('options',options);
          //   console.log('annotations',annotations);

          console.log('action', action);


          if (action === 'add') {
            console.log('this is a change that added annotations');
            const annots = annotationManager.getAnnotationsList();
            console.log(annots);
            
          } else if (action === 'modify') {
            // alert(33);

            console.log('this change modified annotations');
            if (annotations.length) {
              console.log('annotations x', annotations[0].X);
              console.log('annotations y', annotations[0].Y);
              console.log('id', annotations[0].Id);
              console.log('info', info);

              const annots = annotationManager.getAnnotationsList();
              console.log('annots', annots);

              const FreeTextAnnotation = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
              const widget_annots = annots.filter(a => a instanceof Annotations.WidgetAnnotation);
              const sticky_annots = annots.filter(a => a instanceof Annotations.StickyAnnotation);
              const signature_widget_annotation = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
              // const _orms = annots.filter(a => a instanceof Annotations.Forms);
              console.log('FreeTextAnnotation', FreeTextAnnotation);
              console.log('widgetAnnots', widget_annots);
              console.log('sticky_annots', sticky_annots);
              console.log('SignatureWidgetAnnotation', signature_widget_annotation);
              // console.log('Forms',_orms);
              sticky_annots.forEach(element => {
                element.Author = this.user_details.firstName + ' ' + this.user_details.lastName;
              });
              signature_widget_annotation.forEach(element => {
                console.log('sign here button id', element.Id);
                console.log('sign here button', element);
                console.log(' annotations[0].Id',  annotations[0].Id);
               
                if (element.Id == annotations[0].Id) {
                  console.log('sign here button X', element.X);
                  console.log('sign here button Y', element.Y);
             
                  console.log('pageNumber3', annotations[0].PageNumber);
                  element.setPageNumber(annotations[0].PageNumber);
                  // element.PageNumber = this.pdf_current_page;
                  
                  element.setX(annotations[0].X + 100);
                  element.setY(annotations[0].Y - 10);
                  // annotationManager.updateAnnotation(element);
                  annotationManager.drawAnnotationsFromList([element]);
                  this.cdr.detectChanges();
                }

              });

              console.log('SignatureWidgetAnnotation', signature_widget_annotation);
            }





          } else if (action === 'delete') {
            console.log('deleted annotation', annotations);
            console.log('before', this.selectedUser);
            annotations.forEach((element, i) => {
              console.log(element);

              console.log(element.Id);
              let index = this.selectedUser.findIndex(ele => ele.id === element.Id);
              console.log(index);
              if (!this.is_deleted_by_btn && index >= 0) {
                this.removeSigner(index)
              }

              this.selectedUser = this.selectedUser.filter(t => t.id !== element.Id);
              this.cdr.detectChanges();
              this.is_deleted_by_btn = false;
            });


            console.log('After', this.selectedUser);
            console.log('there were annotations deleted');

          }
        });


        instance.UI.loadDocument(documentBlobObjectUrl);
        instance.UI.enableElements(['contentEditButton']);
        // const signatureTool = documentViewer.getTool('AnnotationCreateSignature');
        // later save the document with updated annotations data
        documentViewer.addEventListener('annotationsLoaded', () => {
          this.is_annots_loaded = true;
      
          this.cdr.detectChanges();

        })
      })
  }

  blobImage(blob: any, api_data) {

    const formData = new FormData();
    // formData.append('file', blob, this.filename); // if you want to save with original file name
    formData.append('file', blob);
    console.log('blobbbb', blob);
   
   
    this.cs.pdfUpload(formData).subscribe(
      (events: any) => {

        console.log('HttpEventType',HttpEventType);
        

        if (events.type === HttpEventType.Response) {
          console.log(events.body);
          api_data.FileData.filename = events.body.filename;
          api_data.FileData.originalFileName = this.filename;
          if(this.is_save) {
          this.saveFile(api_data);
          this.cs.showSuccessMsg('File Saved Successfully', 'Uploaded');
          this.is_save = false;
          }else {
            this.shareWithUser(api_data);
            this.cs.showSuccessMsg('File Shared Successfully', 'Uploaded');
          }

        
          this.modalService.dismissAll();
        }
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }
  shareWithUser(api_data) {
    console.log(api_data);
    // const formData = new FormData();
    // formData.append('file', blob);
    console.log(api_data);


    this.cs.httpRequest('post', 'file-management/add-file', api_data).subscribe(
      (res: any) => {
        console.log(res);
        this.redirectLink();

      },
      (err: any) => {
        this.redirectLink();
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }

  saveFile(api_data) {
    console.log(api_data);
    // const formData = new FormData();
    // formData.append('file', blob);
    console.log(api_data);

  
   
    this.cs.httpRequest('post', 'file-management/save-file', api_data).subscribe(
      (res: any) => {
        console.log(res);
        this.redirectLink();

      },
      (err: any) => {
        this.redirectLink();
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }
  CreateNew() {
    // alert("Create New Clicked : "+city)
    console.log('selectedUser', this.selectedUser);
  }
  onAddUser(e, item) {
    console.log(e);
    console.log(item);
    console.log(this.selectedUser);
  }
  fetchUsers() {
    this.cs
      .httpRequest(
        'get',
        `user/all_user_list`
      )
      ?.subscribe(
        (res: any) => {
          console.log(res);
          let data:any = res.list;

           for (let item in data) {
              data[item].bindName =
                data[item].firstName + " " + data[item].lastName;
              this.users_list.push(data[item]);
          }
         
        },
        (err) => this.cs.handleError(err)
      );
  }

  shareFile() {
    console.log(this.otherUserFormArr.value)
    console.log('share btn');
    console.log(this.start_date);
    console.log(this.end_date);
    if(this.is_form_submit) {
      return
    }
    this.is_form_submit = true;
   

    if (this.selectedUser.length && this.start_date && this.end_date) {
      this.saveBtn.click();
      window.scrollTo(0, 1); //detect change of redirect route
    } else {
      this.is_submit = true;
      this.is_form_submit = false;
    }
    let btn = document.getElementById('savebtn')
    console.log(btn);
  }
  save() {
    console.log(this.otherUserFormArr.value)
    console.log('share btn');
    console.log(this.start_date);
    console.log(this.end_date);
    if(this.is_form_submit) {
      return
    }
    this.is_form_submit = true;

  //  return
    if (this.selectedUser.length && this.start_date && this.end_date) {
      this.is_save = true;
      this.saveBtn.click();
      window.scrollTo(0, 1); //detect change of redirect route
    } else {
      this.is_submit = true;
      this.is_form_submit = false;
    }


    let btn = document.getElementById('savebtn')
    console.log(btn);
  }
  onSelectUser(e: NgOption,index) {


    if (!e) {
      return
    }
    console.log(e);
    // this.selectedUser = [];
    console.log(this.selects2);
    // let item = this.selects.itemsList.findByLabel('label of the item');
    // console.log(item);
    setTimeout(() => {
      if (e.id) {
        this.signerEmails().at(index).patchValue({name:e.email});
        console.log(e.id);
        this.selects2.forEach((element) => {
          console.log(e);
          // console.log(element.select({firstName:'43gfg'}));
          console.log(element);

        })
      }
    }, 200);


    this.selectedUser.push(e);
    if (!e.id) {

    }
    // setTimeout(() => {
    //   this.selectedUser = this.selectedUser3;
    //   this.selectedUser.pop();
    // }, 100);

    // this.cdr.detach();
    // this.cdr.detectChanges();
    // this.cdr.detach();
    // this.cdr.reattach();
    console.log(e);
    console.log(this.selectedUser);

    // setTimeout(() => {
    //   this.selectedUser = [{
    //     email: "mks@user2.comws",
    //     firstName: "Mohitr44",
    //     id: 2,
    //     lastName: "Bhagat singh"
    //   }];
    // }, 2000);
    if (!e.id) {
      this.other_signer_name = e.firstName;
      this.other_signer_email = e.email;
    } else {
      this.selected_user_name = e.firstName + ' ' + e.lastName;
      this.selected_user_email = e.email;
      // this.selected_user_id = e.id;
      let btn = document.getElementById('add_annot_btn')
      btn.click();
    }

  }

  addUser() {

    if (this.other_signer_name == '' || this.other_signer_email == '') {
      return
    }

    console.log(this.signerEmails().length);
    console.log(this.signerEmails());
    this.other_signer_name = this.other_signer_name.trim();
    this.other_signer_email = this.other_signer_email.trim();

    if (this.signerEmails().length && !this.signerEmails().controls[this.signerEmails().length - 1].value.email) {
      this.signerEmails().controls[this.signerEmails().length - 1].patchValue({ email: this.other_signer_email });
      this.selectedUser = [{ firstName: this.other_signer_name + ' ', email: this.other_signer_email, id: this.other_signer_email + ' ' + this.other_signer_name }];
    } else {
      this.signerEmails().push(this.newSigner(this.other_signer_email, this.other_signer_name));
      this.selectedUser.push({ firstName: this.other_signer_name + ' ', email: this.other_signer_email, id: this.other_signer_email + ' ' + this.other_signer_name });
    }

    this.selected_user_name = this.other_signer_name;
    this.selected_user_email = this.other_signer_email;
    // this.selected_user_id = this.selectedUser3[this.selectedUser3.length-1].id;
    // this.selected_user_id = this.selectedUser3[this.selectedUser3.length-1].firstName;
    // this.selectedUser = this.selectedUser.filter(t => t.id !== 'element.Id'); // use for detect changes on ng select
    console.log('other_signer_name', this.other_signer_name);


    // this.cdr.detectChanges();

    // console.log('last id length',(this.selectedUser[this.selectedUser.length - 1].id).length);
    // console.log('last id trim length',(this.selectedUser[this.selectedUser.length - 1].id).trim().length);
    // return;
    

    let btn = document.getElementById('add_annot_btn')
    btn.click();
    this.is_add_other_user = false;
    this.other_signer_name = '';
    this.other_signer_email = '';

  }

 

  onRemoveSelectedUser(index) {
    console.log('index', index);
    console.log('selectedUser', this.selectedUser);
    this.selected_rm_index = index;
    if (this.selectedUser.length) {
      this.selected_rm_user_id = this.selectedUser[index].id && typeof this.selectedUser[index].id == 'number' ? this.selectedUser[index].id : this.selectedUser[index].email + ' ' + this.selectedUser[index].firstName;
    }


    if (index > -1) {
      this.selectedUser.splice(index, 1); // 2nd parameter means remove one item only
      this.removeSigner(index);
    }
    console.log('selectedUser', this.selectedUser);

    console.log('selected_rm_user_id', this.selected_rm_user_id);
    let btn = document.getElementById('rm_annot_btn')
    btn.click();

  }
  onAddOtherUser(e) {
    console.log(e);
    console.log(e.value);
    //  this.selected_rm_user_id = e.value.id;
    //   let  btn =  document.getElementById('rm_annot_btn')
    //   btn.click();

  }

  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
        console.log(res);
        this.user_details = res;
      },
      (err) => this.cs.handleError(err)
    );
  }
  customSearchFn(term: string, item: any) {
    console.log(term);
    console.log(item);

    term = term.toLocaleLowerCase();
    return item.email.toLocaleLowerCase().indexOf(term) > -1 ||
      item.firstName.toLocaleLowerCase().indexOf(term) > -1 ||
      (item.email + " - " + item.firstName).toLocaleLowerCase().indexOf(term) > -1;
  }

  disableElementsForAdmin() {
    return [
      'viewControlsButton',
      'downloadButton',
      'printButton',
      'fileAttachmentToolGroupButton',
      'toolbarGroup-Edit',
      'crossStampToolButton',
      'checkStampToolButton',
      'dotStampToolButton',
      'rubberStampToolGroupButton',
      'dateFreeTextToolButton',
      'toolbarGroup-Shapes',
      'toolbarGroup-Forms',
      'shapeToolGroupButton',
      'highlightToolGroupButton',
      'freeHandHighlightToolGroupButton',
      'freeHandToolGroupButton',
      'selectToolButton',
      'strikeoutToolGroupButton',
      'squigglyToolGroupButton',
      'calloutToolGroupButton',
      'dropdown-item-toolbarGroup-Annotate',
      'linkButton',
      // 'dropdown-item-toolbarGroup-FillAndSign',
      'dropdown-item-toolbarGroup-Insert',
      'freeTextToolButton',
      'freeHandToolButton',
      'eraserToolButton',
      'redoButton',
      'undoButton',
      // 'toggleNotesButton',
      'dropdown-item-toolbarGroup-Shapes',
      'toolbarGroup-FillAndSign',
      'toolbarGroup-Insert',
      'toolbarGroup-Shapes',
      'toolbarGroup-Annotate',
      'cropToolGroupButton',
      'checkBoxFieldToolGroupButton',
      'radioButtonFieldToolGroupButton',
      'listBoxFieldToolGroupButton',
      'comboBoxFieldToolGroupButton',
      'textFieldToolGroupButton',
      'freeTextToolGroupButton',
      'stickyToolGroupButton',
      'underlineToolGroupButton',
      'signatureModal',
      // 'formFieldEditPopup',
      'annotationDeleteButton',
      // 'annotationStyleEditButton',
      // 'annotationCommentButton'

    ]
  }
  // download() {
  //   console.log((window as any).instance);
  //   const arrayBuffer =(window as any).instance.exportPDF()
  //      const blob = new Blob([arrayBuffer], { type: 'application/pdf' });
  //   // console.log(this.instance);
  //   console.log(blob);
  //   var fileURL = URL.createObjectURL(blob);
  //   console.log(fileURL);

  // }


  // // implementation with PSPDFKit
  // uploadPSPDFKitFile(documentBlobObjectUrl) {
  //   let instance: any;
  //   const downloadButton = {
  //     type: "custom",
  //     id: "download-pdf",
  //     icon: "/download.svg",
  //     title: "Download",
  //     onPress: async () => {
  //       const documentBuffere = await instance.exportPDF();
  //       console.log(documentBuffere, 'documentBuffere');
  //       // console.log(JSON.parse(documentBuffere),'documentBuffere');
  //       console.log(JSON.stringify(documentBuffere), 'documentBuffere');

  //       const documentBuffer = instance.exportPDF().then((buffer: any) => {
  //         const blob = new Blob([buffer], { type: "application/pdf" });
  //         const fileName = "document.pdf";
  //         console.log(blob);
  //         this.blobImage(blob);
  //         return


  //         let blooob: any

  //         var fileURL = URL.createObjectURL(blob);
  //         localStorage.setItem('blob', fileURL)

  //         if ((window.navigator as any).msSaveOrOpenBlob) {
  //           // window.navigator.msSaveOrOpenBlob(blob, fileName);
  //           (window.navigator as any).msSaveOrOpenBlob(blob, fileName);
  //         } else {
  //           const objectUrl = URL.createObjectURL(blob);
  //           const a: any = document.createElement("a");
  //           a.href = objectUrl;
  //           a.style = "display: none";
  //           a.download = fileName;
  //           document.body.appendChild(a);
  //           a.click();
  //           URL.revokeObjectURL(objectUrl);
  //           setTimeout(() => {
  //             console.log('objectUrl', objectUrl);
  //             console.log(URL.revokeObjectURL(objectUrl));

  //             console.log(fileURL);
  //           }, 2000);

  //           document.body.removeChild(a);
  //         }
  //       });
  //     }
  //   };
  //   const items: any = PSPDFKit.defaultToolbarItems;
  //   // Add the download button to the toolbar.
  //   items.push(downloadButton);
  //   PSPDFKit.load({
  //     // Use the assets directory URL as a base URL. PSPDFKit will download its library assets from here.
  //     baseUrl: location.protocol + "//" + location.host + "/assets/",
  //     // document: 'http://localhost:3001/api/v1/user/a636b0526951d1d240ba9674aa580189',
  //     document: documentBlobObjectUrl,
  //     container: ".pspdfkit-container",
  //     toolbarItems: items,
  //   }).then(_instance => {
  //     instance = _instance;
  //     // For the sake of this demo, store the PSPDFKit for Web instance
  //     // on the global object so that you can open the dev tools and
  //     // play with the PSPDFKit API.
  //     (window as any).instance = _instance;
  //   });
  // }


  signerEmails(): FormArray {
    return this.productForm.get("signer_emails") as FormArray
  }

  newSigner(email = null,name=null): FormGroup {
    return this.fb.group({
      email: name,
      name: email 
    })
  }

  addSigner(is_new = null) {

    this.signerEmails().push(this.newSigner());
    this.cdr.detectChanges();
  }

  removeSigner(i: number) {
    this.signerEmails().removeAt(i);
  }

  onSubmit() {
    console.log(this.productForm.value);
  }

  redirectLink() {
    this.router.navigateByUrl('/file-management/shared');
  }

  changeStartDate() {
    this.end_date = '';
  }



}
