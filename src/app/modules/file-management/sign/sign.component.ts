import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import WebViewer, { WebViewerInstance } from '@pdftron/webviewer';
import { baseUrl } from '../../../Constants/constants';
import { HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit {
  isSignature: boolean = true;
  file_id: any;
  saveBtn: HTMLButtonElement;
  user_details: any;
  type: any;
  shared_file: any;
  is_annots_loaded:boolean;
  constructor(private cs: CommonService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.file_id = params['id'];
      // this.type =  params['type'];

    });
  }

  ngOnInit(): void {
    this.cs.httpRequest('get', 'file-management/shared-with-me-file?id=' + this.file_id).subscribe(res => {
      console.log(res);
      this.shared_file = res;
      const file_name = this.shared_file.shared_file[0].file.fileName;
      const documentBlobObjectUrl = baseUrl + 'file-management/file/' + file_name
      this.fetchUserDetails();
      this.uploadWebViewver(documentBlobObjectUrl);
    })
  }
  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
        console.log(res);
        this.user_details = res;
      },
      (err) => this.cs.handleError(err)
    );
  }

  uploadWebViewver(documentBlobObjectUrl) {
    WebViewer({
      path: './../../../../assets/lib',
      initialDoc: documentBlobObjectUrl
    }, document.getElementById('viewer'))
      .then(instance => {


        const { UI, Core } = instance;
        const { documentViewer, annotationManager, Tools, Annotations } = Core;
        // call methods from UI, Core, documentViewer and annotationManager as needed
        instance.UI.disableElements(this.disableElementsForAdmin());
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Z)
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Y)

        instance.UI.setZoomLevel(0.4);
        // create a form field
        const { WidgetFlags } = Annotations;
        // set author name for comment
        // annotationManager.setAnnotationDisplayAuthorMap((userId) => {
        //   return this.user_details ? this.user_details.firstName + ' ' + this.user_details.lastName : '';

        // });

        // const displayAuthor = annotationManager.getDisplayAuthor(annotation.Author);
        // add_annot_btn is your own custom button
        // document.getElementById('rm_annot_btn').addEventListener('click', () => {


        // });

        // add_annot_btn is your own custom button
        // document.getElementById('add_annot_btn').addEventListener('click', () => {

        // });

        documentViewer.addEventListener('documentLoaded', () => {
          console.log('document loaded');

        });

        documentViewer.addEventListener('annotationsLoaded', () => {
          console.log('annots loadded');
          
          this.is_annots_loaded = true;
        });
        instance.UI.setHeaderItems(header => {
          const parent = documentViewer.getScrollViewElement().parentElement;

          const menu = document.createElement('div');
          menu.classList.add('Overlay');
          menu.classList.add('FlyoutMenu');
          menu.style.padding = '1em';

          const downloadBtn = document.createElement('button');
          downloadBtn.textContent = 'Download';
          downloadBtn.onclick = () => {
            // Download
          };

          menu.appendChild(downloadBtn);

          let isMenuOpen = false;

          const renderCustomMenu = () => {
            this.saveBtn = document.createElement('button');
            this.saveBtn.textContent = 'Save';
            this.saveBtn.id = 'savebtn';
            this.saveBtn.disabled = false;
            this.saveBtn.hidden = true;

            this.saveBtn.onclick = () => {
              console.log(55);
              annotationManager.exportAnnotations().then(xfdfString => {
                console.log(44555);

                const doc = documentViewer.getDocument();
                doc.getFileData({ xfdfString }).then(data => {
                
                  const arr = new Uint8Array(data);
                  const blob = new Blob([arr], { type: 'application/pdf' });
                  console.log(blob);
                  console.log(URL.createObjectURL(blob));
                  let api_data = {
                    file_id: parseInt(this.file_id)
                  }
                  // this.redirectLink();
                  // return;
                  // console.log(this.selectedUser);
                  this.blobImage(blob,api_data);

                })
              });

              if (isMenuOpen) {
                parent.removeChild(menu);
              } else {
                menu.style.left = `${document.body.clientWidth - (this.saveBtn.clientWidth + 40)}px`;
                menu.style.right = 'auto';
                menu.style.top = '40px';
                parent.appendChild(menu);
              }

              isMenuOpen = !isMenuOpen;
            };

            return this.saveBtn;
          };

          const newCustomElement = {
            type: 'customElement',
            render: renderCustomMenu,
          };

          header.push(newCustomElement);
        });
        annotationManager.addEventListener('annotationChanged', (annotations, action) => {
          // use when new annots added
         
          if (action === 'add' &&  !this.is_annots_loaded) {
            const annots = annotationManager.getAnnotationsList();
            console.log('this is a change that added annotations');
            const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
            // const signAnnots2 = annots.filter(a => a instanceof Annotations.type);
            const signatureWidgetAnnots = annotationManager.getAnnotationsList().filter(
              annot => annot instanceof Annotations.SignatureWidgetAnnotation
            );
          console.log('widgetAnnots',widgetAnnots);
          console.log('signAnnots',signAnnots);
          console.log('signAnnots',annots);
          console.log('signatureWidgetAnnots',signatureWidgetAnnots);
         
          
            // widgetAnnots.forEach((element, i) => {
            //   element.Locked = true;
            //   element.LockedContents = true;
            //   // element.NoView = true;
            //   // element.isVisible()
            //   element.Hidden = this.user_details.id != element.Id? true:false;
            //   element.ReadOnly = true;
            // });
            annots.forEach((element, i) => {
              console.log('element id',element.Id);
              console.log('element',element);
              console.log('time_id',element.getCustomData('time_id'));
              console.log('this.user_details.id',this.user_details.id);
              if(element.getCustomData('user_id')) {
              element.Hidden = this.user_details.id !=element.getCustomData('user_id')? true:false;
              }else {
                
                if(element.Subject != 'Note') {
                element.Hidden = this.user_details.id != element.Id? true:false;
                }
              }
              element.ReadOnly = true;
              console.log(element);
              console.log('user_id',element.getCustomData('user_id'));

            });
          }
        
          // use when annots read from docs
          if (action === 'add' &&  this.is_annots_loaded) {
            console.log('annotations', annotations);
            annotations[0].setCustomData('user_id',this.user_details.id);
            annotations[0].NoMove = true;
            const annots = annotationManager.getAnnotationsList();
            console.log('this is a change that added annotations');
            const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
            const sign_here_annots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
            const sticky_annots = annots.filter(a => a instanceof Annotations.StickyAnnotation);
            const signatureWidgetAnnots = annotationManager.getAnnotationsList().filter(
              annot => annot instanceof Annotations.SignatureWidgetAnnotation
            );
            console.log('widgetAnnots',widgetAnnots);
            console.log('signAnnots',signAnnots);
            console.log('signatureWidgetAnnots',signatureWidgetAnnots);
            console.log('sticky_annots',sticky_annots);
            sticky_annots.forEach(element => {
              if(element.getCustomData('user_id') == this.user_details.id) {
                console.log('reply');
                element.Author = this.user_details.firstName + ' ' + this.user_details.lastName
                
              }
            });

            signatureWidgetAnnots.forEach(annot => {
              annot.Subject
              console.log( annot.Subject);
              console.log( annot.getStatus());
              console.log( annot.getCustomData('user_id'));
              
              // annot.isSignedDigitally().then(isSigned => {
              //   if (isSigned) {
              //     // if this signature field is signed initially
              //   } else {
              //     // if this signature field is not signed initially
              //   }
              // });
            });
            widgetAnnots.forEach((element, i) => {
              element.Locked = true;
              element.LockedContents = true;
              console.log(element);
              console.log(element.getCustomData('time_id'));
              
              // element.NoView = true;
              // element.isVisible()
              element.Hidden = (this.user_details.id+'_'+element.getCustomData('time_id')) != element.Id? true:false;
              element.ReadOnly = true;
            });
            // sign_here_annots.forEach((element, i) => {
            //   element.Locked = true;
            //   element.LockedContents = true;
            
           
              

            //   element.Hidden = this.user_details.id != (element.getCustomData('user_id')).trim() ? true:false;
            //   element.ReadOnly = true;
            // });
            signAnnots.forEach((element, i) => {
              element.Hidden = this.user_details.id !=element.getCustomData('user_id')? true:false;
              element.ReadOnly = this.user_details.id !=element.getCustomData('user_id')? true:false;
             
              console.log(element);
              // element.setCustomData('user_id','4')
              console.log('user_id',element.getCustomData('user_id'));
            });

          } else if (action === 'modify') {

            console.log('this change modified annotations');

          } else if (action === 'delete') {
            console.log('deleted annotation', annotations);

          }
          // this.is_annots_loaded = true;
        });


        instance.UI.loadDocument(documentBlobObjectUrl);
        // later save the document with updated annotations data
        documentViewer.addEventListener('annotationsLoaded', () => {

        })
      })
  }

  blobImage(blob: any,api_data) {
    console.log();
    
    const formData = new FormData();
    formData.append('file', blob, this.shared_file.shared_file.fileName);
    console.log('blobbbb',blob);
    
    this.cs.pdfUpload(formData).subscribe(
      (events: any) => {
       
        if (events.type === HttpEventType.Response) {
          console.log(events.body);
          api_data.filename = events.body.filename;
          this.updateFile(api_data);
          this.cs.showInfoMsg('File Shared Successfully', 'Uploaded');

        }
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }
  updateFile(api_data) {
    this.cs.httpRequest('post','file-management/update-file',api_data).subscribe(
      (res:any) => {
       console.log(res);
       this.router.navigateByUrl('/file-management/listing');
       this.redirectLink();
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      });
  }
  redirectLink() {
    this.router.navigateByUrl('/file-management/shared');
  }
  saveFile() {
      this.saveBtn.click();
      window.scrollTo(0, 1); //detect change of redirect route
   let  btn =  document.getElementById('savebtn')
   console.log(btn); 
  }
  handleDenial() {

  }
  handleDismiss(e) {
    
  }

  disableElementsForAdmin() {
    return [
      'viewControlsButton',
      'downloadButton',
      'printButton',
      'fileAttachmentToolGroupButton',
      'toolbarGroup-Edit',
      'crossStampToolButton',
      'checkStampToolButton',
      'dotStampToolButton',
      'rubberStampToolGroupButton',
      'dateFreeTextToolButton',
      // 'toolbarGroup-Shapes',
      'toolbarGroup-Forms',
      'shapeToolGroupButton',
      'highlightToolGroupButton',
      'freeHandHighlightToolGroupButton',
      'freeHandToolGroupButton',
      'selectToolButton',
      'strikeoutToolGroupButton',
      'squigglyToolGroupButton',
      'calloutToolGroupButton',
      'dropdown-item-toolbarGroup-Annotate',
      'linkButton',
      //  'dropdown-item-toolbarGroup-FillAndSign',
      'dropdown-item-toolbarGroup-Insert',
      'freeTextToolButton',
      'freeHandToolButton',
      'eraserToolButton',
      'redoButton',
      'undoButton',
      'dropdown-item-toolbarGroup-View',
      'dropdown-item-toolbarGroup-Shapes',
      'toggleNotesButton',
      'dropdown-item-toolbarGroup-Shapes',
      'toolbarGroup-Insert',
      'toolbarGroup-Shapes',
      'toolbarGroup-Annotate',
      'freeTextToolGroupButton',
      'toolbarGroup-View',
      'toolbarGroup-FillAndSign',
      'signatureToolGroupButton',
      'toolsOverlay'
    ]
  }

}
