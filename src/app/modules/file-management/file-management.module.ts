import { EditComponent } from './edit/edit.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileManagementRoutingModule } from './file-management-routing.module';
import { FileManagementComponent } from './file-management.component';
import { ListingComponent } from './listing/listing.component';
import { SharedComponent } from './shared/shared.component';
import { UploadingComponent } from './uploading/uploading.component';
import { ViewfileComponent } from './viewfile/viewfile.component';
import { PreviewComponent } from './preview/preview.component';
import { CommentComponent } from './comment/comment.component';
import { SignComponent } from './sign/sign.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { EditFileComponent } from './edit-file/edit-file.component';
@NgModule({
  declarations: [
    FileManagementComponent,
    UploadingComponent,
    SharedComponent,
    ListingComponent,
    ViewfileComponent,
    PreviewComponent,
    EditComponent,
    CommentComponent,
    SignComponent,
    EditFileComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PdfViewerModule,
    NgbModule,
    PdfViewerModule,
    FormsModule,
    ReactiveFormsModule,
    FileManagementRoutingModule,
    NgSelectModule
  ]
})
export class FileManagementModule { }
