import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import WebViewer, { WebViewerInstance } from '@pdftron/webviewer';
import { baseUrl,imgBaseUrl } from '../../../Constants/constants';
@Component({
  selector: 'app-viewfile',
  templateUrl: './viewfile.component.html',
  styleUrls: ['./viewfile.component.css'],
})
export class ViewfileComponent implements OnInit {
  isComments: boolean = true;
  shared_file;
  file_id: any;
  saveBtn: HTMLButtonElement;
  user_details: any;
  type: any;
  is_locked:boolean;
  initiator_id:any;
  imgBaseUrl =imgBaseUrl;
  is_date_expired:boolean;
  isAdmin:boolean;
  is_shared_user:boolean;
  request_status:number; // 1 for accepted, 2 for rejected
  page: number = 0;
  total:any;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private cs: CommonService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.file_id = params['id'];
      this.type = params['type'];

    });
  }
  ngOnInit(): void {
    this.cs.httpRequest('get', 'file-management/shared-file?id=' + this.file_id).subscribe(res => {
      console.log(res);
      this.shared_file = res;
      const file_name = this.shared_file.shared_file.fileName;

     this.initiator_id = this.shared_file.shared_users[0].initiator_id;
      const documentBlobObjectUrl = baseUrl + 'file-management/file/' + file_name
      console.log('documentBlobObjectUrl',documentBlobObjectUrl);
      
      this.fetchUserDetails();
      this.uploadWebViewver(documentBlobObjectUrl);
      this.isAdmin = this.cs.isAdmin();
    })


  }
  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
        console.log(res);
        this.user_details = res;
        this.shared_file.shared_users.forEach(element => {
          if(element.user_id == this.user_details.id && element.is_locked) {
            this.is_locked = true;
          }
          if(element.user_id == this.user_details.id) { 
            this.is_shared_user = true;
            this.request_status = element.is_accept;
          }
        });
        let date = new Date(new Date().toDateString());
        let start_date = new Date(new Date(this.shared_file.shared_file.startDate).toDateString());
        let end_date = new Date(new Date(this.shared_file.shared_file.endDate).toDateString());
  
        console.log('date',date);
        console.log('start_date',start_date);
        console.log('end_date',end_date);
        console.log('date',date.getTime());
        console.log('start_date',start_date.getTime());
        console.log('end_date',end_date.getTime());
        console.log('s_date',date.getTime() <= start_date.getTime());
        console.log('end_date', date.getTime() > end_date.getTime());
        if(date.getTime() < start_date.getTime() || date.getTime() > end_date.getTime()) {
          this.is_locked = true;
        }
        if( end_date.getTime() < date.getTime()) {
          this.is_date_expired = true;
        }
      },
      (err) => this.cs.handleError(err)
    );
  }


  uploadWebViewver(documentBlobObjectUrl) {
    WebViewer({
      path: './../../../../assets/lib',
      initialDoc: documentBlobObjectUrl
    }, document.getElementById('viewer'))
      .then(instance => {


        const { UI, Core } = instance;
        const { documentViewer, annotationManager, Tools, Annotations } = Core;
        // call methods from UI, Core, documentViewer and annotationManager as needed
        instance.UI.disableElements(this.disableElementsForAdmin());
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Z)
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Y)

        instance.UI.setZoomLevel(0.4);
        // create a form field
        const { WidgetFlags } = Annotations;
        // set author name for comment
        annotationManager.setAnnotationDisplayAuthorMap((userId) => {
          return this.user_details ? this.user_details.firstName + ' ' + this.user_details.lastName : '';

        });
        // const displayAuthor = annotationManager.getDisplayAuthor(annotation.Author);
        // add_annot_btn is your own custom button
       

        documentViewer.addEventListener('documentLoaded', () => {
          // call methods relating to the loaded document
        });
        instance.UI.setHeaderItems(header => {
          const parent = documentViewer.getScrollViewElement().parentElement;

          const menu = document.createElement('div');
          // menu.classList.add('Overlay');
          // menu.classList.add('FlyoutMenu');
          menu.style.padding = '1em';

          const downloadBtn = document.createElement('button');
          downloadBtn.textContent = 'Download';
          downloadBtn.onclick = () => {
            // Download
          };

          menu.appendChild(downloadBtn);

          let isMenuOpen = false;

          const renderCustomMenu = () => {
            this.saveBtn = document.createElement('button');
            this.saveBtn.textContent = 'Save';
            this.saveBtn.id = 'savebtn';
            this.saveBtn.disabled = false;
            this.saveBtn.hidden = true;

            this.saveBtn.onclick = () => {
              console.log(55);
              annotationManager.exportAnnotations().then(xfdfString => {
                console.log(44555);

                const doc = documentViewer.getDocument();
                doc.getFileData({ xfdfString }).then(data => {
                  // console.log('aaaaaaaaaaaaaaa',data);
                  // return;
                  const arr = new Uint8Array(data);
                  const blob = new Blob([arr], { type: 'application/pdf' });
                  console.log(blob);
                  console.log(URL.createObjectURL(blob));
                  // console.log(this.selectedUser);

                  //  let user_ids =  this.selectedUser.map(u=>u.id);
                  //  // distinct user id
                  //  user_ids = user_ids.filter((v, i, a) => a.indexOf(v) === i);
                  //  console.log(user_ids);
                  //   let api_data = {
                  //     FileData : {
                  //    startDate: this.start_date,
                  //    endDate: this.end_date,
                  //    comment: this.comment
                  //  },
                  //  SharedUser : {
                  //   // user_ids: user_ids.join(),
                  //   user_ids: user_ids
                  //  }
                  // }
                  //  this.addFile(file_user_data)
                  // this.blobImage(blob,api_data);
                })
              });

              if (isMenuOpen) {
                parent.removeChild(menu);
              } else {
                menu.style.left = `${document.body.clientWidth - (this.saveBtn.clientWidth + 40)}px`;
                menu.style.right = 'auto';
                menu.style.top = '40px';
                parent.appendChild(menu);
              }

              isMenuOpen = !isMenuOpen;
            };

            return this.saveBtn;
          };

          const newCustomElement = {
            type: 'customElement',
            render: renderCustomMenu,
          };

          header.push(newCustomElement);
        });
        // var docViewer = documentViewer.docViewer;
        annotationManager.addEventListener('annotationChanged', (annotations, action) => {
          if (action === 'add') {
            console.log('this is a change that added annotations');
            const annots = annotationManager.getAnnotationsList();

            console.log(annots,'annotssssss');
            
            // const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            annots.forEach((element, i) => {
              element.Locked = true;
              element.LockedContents = true;
              element.ReadOnly = true;
            });

          } else if (action === 'modify') {

            console.log('this change modified annotations');

          } else if (action === 'delete') {
            console.log('deleted annotation', annotations);

          }
        });


        instance.UI.loadDocument(documentBlobObjectUrl);
        // later save the document with updated annotations data
        documentViewer.addEventListener('annotationsLoaded', () => {

        })
      })
  }
  openComment(content) {
    this.modalService.open(content, { centered: true });
  }
  view() {
    this.router.navigateByUrl('/file-management/upload/comment');
    this.modalService.dismissAll();
  }
  addSign() {
    if(this.request_status == 1) {
      if(this.shared_file.shared_file.status != 'Inactive') {
        if(this.user_details.id == this.initiator_id && !this.is_shared_user) {
          this.router.navigate(['/file-management/upload/edit'],{queryParams: {id:this.file_id}})
        }else {
          this.router.navigate(['/file-management/sign'],{queryParams: {id:this.file_id}})
        }
      }else {
        this.cs.showInfoMsg('Inactive file', 'Inactive');
      }

    }else if(this.request_status == 2){
      this.cs.showErrorMsg('Reject','File has been rejecting by you');
    }else {
      this.cs.showErrorMsg('Please accept the file request in notifications', 'Accept');
    }
  }

  disableElementsForAdmin() {
    console.log(' this.shared_file.shared_file.status ', this.shared_file.shared_file.status );
    
    return [
      'viewControlsButton',
     this.shared_file.shared_file.status == 'Completed' ? '' : 'downloadButton',
      'printButton',
      'fileAttachmentToolGroupButton',
      'toolbarGroup-Edit',
      'crossStampToolButton',
      'checkStampToolButton',
      'dotStampToolButton',
      'rubberStampToolGroupButton',
      'dateFreeTextToolButton',
      // 'toolbarGroup-Shapes',
      'toolbarGroup-Forms',
      'shapeToolGroupButton',
      'highlightToolGroupButton',
      'freeHandHighlightToolGroupButton',
      'freeHandToolGroupButton',
      'selectToolButton',
      'strikeoutToolGroupButton',
      'squigglyToolGroupButton',
      'calloutToolGroupButton',
      'dropdown-item-toolbarGroup-Annotate',
      'linkButton',
      'dropdown-item-toolbarGroup-FillAndSign',
      'dropdown-item-toolbarGroup-Insert',
      'freeTextToolButton',
      'freeHandToolButton',
      'eraserToolButton',
      'redoButton',
      'undoButton',
      'toggleNotesButton',
    'dropdown-item-toolbarGroup-Shapes',
    'toolbarGroup-FillAndSign',
    'toolbarGroup-Insert',
    'toolbarGroup-Shapes',
    'toolbarGroup-Annotate',
    'annotationCommentButton',
    'signatureModal',
    'toolsOverlay'
    ]
  }
}
