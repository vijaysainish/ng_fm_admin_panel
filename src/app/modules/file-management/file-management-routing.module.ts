import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditFileComponent } from './edit-file/edit-file.component';
import { EditComponent } from './edit/edit.component';
import { FileManagementComponent } from './file-management.component';
import { ListingComponent } from './listing/listing.component';
import { PreviewComponent } from './preview/preview.component';
import { SharedComponent } from './shared/shared.component';
import { SignComponent } from './sign/sign.component';
import { ViewfileComponent } from './viewfile/viewfile.component';

const routes: Routes = [
  {
    path: '',
    component: FileManagementComponent,
    children: [
      {
        path: '',
        redirectTo: 'shared',
        pathMatch: 'full',
      },
      {
        path: 'listing',
        component: ListingComponent,
      },
      {
        path: 'sign',
        component: SignComponent,
      },
      {
        path: 'shared',
        component: SharedComponent,
      },
      {
        path: 'upload/view',
        component: ViewfileComponent,
      },
      {
        path: 'upload/preview',
        component: PreviewComponent,
      },
      {
        path: 'upload/edit',
        component: EditComponent,
      },
      {
        path: 'view/edit-file',
        component: EditFileComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FileManagementRoutingModule {}
