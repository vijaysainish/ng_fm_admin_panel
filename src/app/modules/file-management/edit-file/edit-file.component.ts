import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import WebViewer, { WebViewerInstance } from '@pdftron/webviewer';
import { baseUrl } from '../../../Constants/constants';
import { HttpEventType } from '@angular/common/http';
import { NgOption } from '@ng-select/ng-select';

@Component({
  selector: 'app-edit-file',
  templateUrl: './edit-file.component.html',
  styleUrls: ['./edit-file.component.css']
})
export class EditFileComponent implements OnInit {

  isSignature: boolean = true;
  file_id: any;
  saveBtn: HTMLButtonElement;
  user_details: any;
  type: any;
  shared_file: any;
  is_annots_loaded:boolean;
  productForm: FormGroup;
  users_list: any = [];

  selectedUser: any = [];
  other_signer_name: any = '';
  other_signer_email: any = '';

  selectedUser2: any;
  selectedUser3: any = [];

  selected_user_name: any = '';
  selected_user_email: any = '';
  selected_user_id: any;
  selected_rm_user_id: any;
  selected_rm_index: any;

  pdf_current_page: number = 1;
  is_deleted_by_btn: boolean;

  is_add_other_user: boolean;

  old_user_ids = [];
  new_user_ids = [];
  rm_user_ids = [];
  is_submit:boolean;

  constructor(private cs: CommonService,
    private router: Router,
    private fb: FormBuilder,
    public cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute) {
      this.fetchUsers();
    this.activatedRoute.queryParams.subscribe(params => {
      this.file_id = params['id'];
      // this.type =  params['type'];

    });
  }

  ngOnInit(): void {
    this.cs.httpRequest('get', 'file-management/initiator-file-details?id=' + this.file_id).subscribe(res => {
      console.log(res);
      this.shared_file = res;
      const file_name = this.shared_file.shared_file[0].file.fileName;
      console.log('this.shared_file.shared_file',this.shared_file.shared_file);
      


      const documentBlobObjectUrl = baseUrl + 'file-management/file/' + file_name
      this.fetchUserDetails();
      this.uploadWebViewver(documentBlobObjectUrl);

      this.productForm = this.fb.group({
        name: '',
        signer_emails: this.fb.array([]),
      });

      (this.shared_file.shared_file).forEach(element => {
        this.addSharedSigner(element);
      });
    })
  }

  onSelectUser(e: NgOption,index) {


    if (!e) {
      return
    }
    console.log(e);
    // this.selectedUser = [];
    // console.log(this.selects2);
    // let item = this.selects.itemsList.findByLabel('label of the item');
    // console.log(item);
    setTimeout(() => {
      if (e.id) {
        this.signerEmails().at(index).patchValue({name:e.email});
        console.log(e.id);
        // this.selects2.forEach((element) => {
        //   console.log(e);
        //   // console.log(element.select({firstName:'43gfg'}));
        //   console.log(element);

        // })
      }
    }, 200);


    this.selectedUser.push(e);
    if (!e.id) {

    }
    // setTimeout(() => {
    //   this.selectedUser = this.selectedUser3;
    //   this.selectedUser.pop();
    // }, 100);

    // this.cdr.detach();
    // this.cdr.detectChanges();
    // this.cdr.detach();
    // this.cdr.reattach();
    console.log(e);
    console.log(this.selectedUser);
    console.log('this.selectedUser id',this.selectedUser[this.selectedUser.length - 1].id);

    // setTimeout(() => {
    //   this.selectedUser = [{
    //     email: "mks@user2.comws",
    //     firstName: "Mohitr44",
    //     id: 2,
    //     lastName: "Bhagat singh"
    //   }];
    // }, 2000);
   const is_exist =  this.old_user_ids.includes(e.id);
   const is_rm_exist =  this.rm_user_ids.includes(e.id);
   console.log('is_exist',is_exist);
   
   if(!is_exist) {
    this.new_user_ids.push(e.id);
   }

   if(is_rm_exist) {
    this.rm_user_ids = this.rm_user_ids.filter((ele) =>{ 
      return ele != e.id; 
    });
   }

    
    if (!e.id) {
      this.other_signer_name = e.firstName;
      this.other_signer_email = e.email;
    } else {
      this.selected_user_name = e.firstName + ' ' + e.lastName;
      this.selected_user_email = e.email;
      // this.selected_user_id = e.id;
      let btn = document.getElementById('add_annot_btn')
      btn.click();
    }

  }

  onAddSharedUser(e) {
    if (!e) {
      return
    }
    console.log(e);
    // this.selectedUser.push(e);
    if (!e.id) {
      this.other_signer_name = e.firstName;
      this.other_signer_email = e.email;
    } else {
      this.selected_user_name = e.firstName + ' ' + e.lastName;
      this.selected_user_email = e.email;
      // this.selected_user_id = e.id;
      let btn = document.getElementById('add_annot_btn')
      btn.click();
    }

  }
  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
        console.log(res);
        this.user_details = res;
      },
      (err) => this.cs.handleError(err)
    );
  }
  fetchUsers() {
    this.cs
      .httpRequest(
        'get',
        `user/all_user_list`
      )
      ?.subscribe(
        (res: any) => {
          console.log(res);
          let data:any = res.list;

           for (let item in data) {
              data[item].bindName =
                data[item].firstName + " " + data[item].lastName;
              this.users_list.push(data[item]);
          }
         
        },
        (err) => this.cs.handleError(err)
      );
  }

  uploadWebViewver(documentBlobObjectUrl) {
    WebViewer({
      path: './../../../../assets/lib',
      initialDoc: documentBlobObjectUrl
    }, document.getElementById('viewer'))
      .then(instance => {


        const { UI, Core } = instance;
        const { documentViewer, annotationManager, Tools, Annotations } = Core;
        // call methods from UI, Core, documentViewer and annotationManager as needed
        instance.UI.disableElements(this.disableElementsForAdmin());
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Z)
        instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Y)

        instance.UI.setZoomLevel(0.4);
        // create a form field
        const { WidgetFlags } = Annotations;
        // set author name for comment
        annotationManager.setAnnotationDisplayAuthorMap((userId) => {
          return this.user_details ? this.user_details.firstName + ' ' + this.user_details.lastName : '';

        });

         // add_annot_btn is your own custom button
         document.getElementById('rm_annot_btn').addEventListener('click', () => {
          const annots = annotationManager.getAnnotationsList();
          console.log(annots);

          this.is_deleted_by_btn = true;
          const text_widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
          const sign_widgetAnnots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
          console.log(text_widgetAnnots);
          text_widgetAnnots.forEach((element, i) => {
            // remove annotations
            console.log((this.selected_rm_user_id).length);
            console.log((element.Id).length);

            // if (this.selected_rm_user_id.toString().trim() == element.Id) {
            //   console.log('remove id', element.Id);
            //   let rm_annot = []
            //   rm_annot.push(text_widgetAnnots[i])
            //   annotationManager.deleteAnnotations(rm_annot);
            // }
            if ( this.selected_rm_index == i) {
              console.log('remove id', element.Id);
              let rm_annot = []
              rm_annot.push(text_widgetAnnots[i])
              annotationManager.deleteAnnotations(rm_annot);
            }
          });
          console.log('sign_widgetAnnots',sign_widgetAnnots);
          
          sign_widgetAnnots.forEach((element, i) => {
            console.log(element.getCustomData('user_id'));
            
            // remove annotations
            const id = element.getCustomData('user_id') || element.Id
            console.log('iddd',id);
            console.log('rm user iddd',this.selected_rm_user_id.toString().trim());
            
            // if (this.selected_rm_user_id.toString().trim() == id) {
            //   console.log('remove id', element.Id);
            //   let rm_annot = []
            //   rm_annot.push(sign_widgetAnnots[i])
            //   annotationManager.deleteAnnotations(rm_annot);
            // }
            if (this.selected_rm_index == i) {
              console.log('remove id', element.Id);
              let rm_annot = []
              rm_annot.push(sign_widgetAnnots[i])
              annotationManager.deleteAnnotations(rm_annot);
            }
          });

        });

        // add_annot_btn is your own custom button
        document.getElementById('add_annot_btn').addEventListener('click', () => {

          //  const sign_here = new Annotations.SignatureWidgetAnnotation()
          // const flags = new WidgetFlags();
          // flags.set('Required', true);
          const sign_here_field = new Annotations.Forms.Field("some signature field name", {
            type: 'Sig',
            // flags,
          });
          // create a widget annotation
          const widgetAnnot = new Annotations.SignatureWidgetAnnotation(sign_here_field, {
            appearance: '_DEFAULT',
            appearances: {
              _DEFAULT: {
                Normal: {
                  data:
                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuMWMqnEsAAAANSURBVBhXY/j//z8DAAj8Av6IXwbgAAAAAElFTkSuQmCC',
                  offset: {
                    x: 100,
                    y: 100,
                  },
                },
              },
            },
          });
          // set position and size
          widgetAnnot.PageNumber = this.pdf_current_page;
          widgetAnnot.X = 400;
          widgetAnnot.Y = 590;
          widgetAnnot.Width = 100;
          widgetAnnot.Height = 50;

          // widgetAnnot.ReadOnly = false;
          // widgetAnnot.NoMove = false
          // widgetAnnot.setcu = false
          let time_id = Date.now();
          widgetAnnot.Id = this.selectedUser[this.selectedUser.length - 1].id +'_'+time_id;
          if(typeof this.selectedUser[this.selectedUser.length - 1].id != 'number'){
            console.log('(this.selectedUser[this.selectedUser.length - 1].id).trim()',(this.selectedUser[this.selectedUser.length - 1].id).trim());
            
            widgetAnnot.setCustomData('user_id', (this.selectedUser[this.selectedUser.length - 1].id).trim());
            widgetAnnot.setCustomData('time_id', time_id.toString());
          }else {
            console.log('this.selectedUser[this.selectedUser.length - 1].id',this.selectedUser[this.selectedUser.length - 1].id);
            
            widgetAnnot.setCustomData('user_id', this.selectedUser[this.selectedUser.length - 1].id);
            widgetAnnot.setCustomData('time_id', time_id.toString());
          }

          //add the form field and widget annotation
          annotationManager.getFieldManager().addField(sign_here_field);
          annotationManager.addAnnotation(widgetAnnot);
          annotationManager.drawAnnotationsFromList([widgetAnnot]);

          const freeText = new Annotations.FreeTextAnnotation();
          freeText.PageNumber = this.pdf_current_page;
          freeText.X = 300;
          freeText.Y = 600;
          freeText.Width = 300;
          freeText.Height = 150;
          freeText.StrokeThickness = 0;
          freeText.ReadOnly = false;

          // freeText.Color = new Annotations.Color(0, 255, 255);
          freeText.Id = this.selectedUser[this.selectedUser.length - 1].id+'_'+time_id;
          if(typeof this.selectedUser[this.selectedUser.length - 1].id != 'number'){
            freeText.setCustomData('user_id', (this.selectedUser[this.selectedUser.length - 1].id).trim());
            freeText.setCustomData('time_id', time_id.toString());
          }else {
            freeText.setCustomData('user_id', this.selectedUser[this.selectedUser.length - 1].id);
            freeText.setCustomData('time_id', time_id.toString());
          }
          freeText.TextColor = new Annotations.Color(0, 0, 0);


          // freeText.setPadding(new Annotations.Rect(0, 0, 0, 0));
          freeText.setContents('Signature: ' + '_____________' + `\n\n` + 'Name: ' + this.selected_user_name +
            `\n\n` + 'Email: ' + this.selected_user_email);
          // freeText.setContents(this.user_email);
          // freeText.FillColor = new Annotations.Color(0, 255, 255);
          freeText.FontSize = '18pt';

          annotationManager.addAnnotation(freeText, { autoFocus: true, isUndoRedo: false });
          // annotationManager.re;
          annotationManager.redrawAnnotation(freeText);



          const annots = annotationManager.getAnnotationsList();
          console.log('this is a change that added annotations');
          const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
          const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
          // const signAnnots2 = annots.filter(a => a instanceof Annotations.type);
        console.log('widgetAnnots',widgetAnnots);
        console.log('signAnnots',signAnnots);
        console.log('signAnnots',annots);


        
        });

      

        documentViewer.addEventListener('documentLoaded', () => {
          console.log('document loaded');

        });
        documentViewer.addEventListener('pageNumberUpdated', (pageNumber) => {
          console.log(pageNumber, 'pageNumber');
          this.pdf_current_page = pageNumber;
          // here it's guaranteed that page {pageNumber} is fully rendered
          // you can get or set pixels on the canvas, etc
        })
        instance.UI.setHeaderItems(header => {
          const parent = documentViewer.getScrollViewElement().parentElement;

          const menu = document.createElement('div');
          menu.classList.add('Overlay');
          menu.classList.add('FlyoutMenu');
          menu.style.padding = '1em';

          const downloadBtn = document.createElement('button');
          downloadBtn.textContent = 'Download';
          downloadBtn.onclick = () => {
            // Download
          };

          menu.appendChild(downloadBtn);

          let isMenuOpen = false;

          const renderCustomMenu = () => {
            this.saveBtn = document.createElement('button');
            this.saveBtn.textContent = 'Save';
            this.saveBtn.id = 'savebtn';
            this.saveBtn.disabled = false;
            this.saveBtn.hidden = true;

            this.saveBtn.onclick = () => {
              console.log(55);
              annotationManager.exportAnnotations().then(xfdfString => {
                console.log(44555);

                const doc = documentViewer.getDocument();
                doc.getFileData({ xfdfString }).then(data => {
                
                  const arr = new Uint8Array(data);
                  const blob = new Blob([arr], { type: 'application/pdf' });
                  console.log(blob);
                  console.log(URL.createObjectURL(blob));
                  // let api_data = {
                  //   file_id: parseInt(this.file_id)
                  // }
                 
                  let api_data = {
                    FileData: {
                      file_id: parseInt(this.file_id)
                    },
                    SharedUser: {
                      // user_ids: user_ids.join(),
                      new_user_ids: this.new_user_ids,
                      rm_user_ids: this.rm_user_ids,
                    }
                  }
                  console.log('SharedUser',api_data);
                  
                  // return
                  this.blobImage(blob,api_data);

                })
              });

              if (isMenuOpen) {
                parent.removeChild(menu);
              } else {
                menu.style.left = `${document.body.clientWidth - (this.saveBtn.clientWidth + 40)}px`;
                menu.style.right = 'auto';
                menu.style.top = '40px';
                parent.appendChild(menu);
              }

              isMenuOpen = !isMenuOpen;
            };

            return this.saveBtn;
          };

          const newCustomElement = {
            type: 'customElement',
            render: renderCustomMenu,
          };

          header.push(newCustomElement);
        });
        annotationManager.addEventListener('annotationChanged', (annotations, action,info) => {
          // use when new annots added
          if (action === 'add' &&  !this.is_annots_loaded) {
            const annots = annotationManager.getAnnotationsList();
            console.log('this is a change that added annotations');
            const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
            // const signAnnots2 = annots.filter(a => a instanceof Annotations.type);
          console.log('widgetAnnots',widgetAnnots);
          console.log('signAnnots',signAnnots);
          console.log('signAnnots',annots);
         
          
          
            annots.forEach((element, i) => {
              // if(element.getCustomData('user_id')) {
              // element.Hidden = this.user_details.id !=element.getCustomData('user_id')? true:false;
              // }else {
              //   element.Hidden = this.user_details.id != element.Id? true:false;
              // }
              // element.ReadOnly = true;
              console.log(element);
              console.log('user_id',element.getCustomData('user_id'));

            });
          }
         
          // use when annots read from docs
          if (action === 'add' &&  this.is_annots_loaded) {
            console.log('annotations', annotations);
            // annotations[0].setCustomData('user_id',this.user_details.id);
            const annots = annotationManager.getAnnotationsList();
            console.log('this is a change that added annotations');
            const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
            const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
           
            
            widgetAnnots.forEach((element, i) => {
              
              
              // element.Locked = true;
              // element.LockedContents = true;
              // // element.NoView = true;
              // // element.isVisible()
              // element.Hidden = this.user_details.id != element.Id? true:false;
              // element.ReadOnly = true;
            });
            signAnnots.forEach((element, i) => {
              // element.Hidden = this.user_details.id !=element.getCustomData('user_id')? true:false;
              // element.ReadOnly = this.user_details.id !=element.getCustomData('user_id')? true:false;
              // console.log(element);
              // // element.setCustomData('user_id','4')
              console.log('user_id',element.getCustomData('user_id'));
            });

          } else if (action === 'modify') {

            console.log('this change modified annotations');
            if (annotations.length) {
              console.log('annotations x', annotations[0].X);
              console.log('annotations y', annotations[0].Y);
              console.log('id', annotations[0].Id);
              console.log('info', info);

              const annots = annotationManager.getAnnotationsList();
              console.log('annots', annots);

              const FreeTextAnnotation = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
              const widget_annots = annots.filter(a => a instanceof Annotations.WidgetAnnotation);
              const signature_widget_annotation = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
              // const _orms = annots.filter(a => a instanceof Annotations.Forms);
              console.log('FreeTextAnnotation', FreeTextAnnotation);
              console.log('widgetAnnots', widget_annots);
              console.log('SignatureWidgetAnnotation', signature_widget_annotation);
              // console.log('Forms',_orms);
              signature_widget_annotation.forEach(element => {
                console.log('sign here button id', element.Id);
                const u_id = element.getCustomData('user_id') || element.Id;
                const time_id = element.getCustomData('time_id') || element.Id;
                console.log('modifiye user id', element.Id);
                console.log('modifiye u_id id', u_id);
                console.log('modifiye u_id id', time_id);
                console.log('annotations user id', annotations[0].Id);
                console.log('annotations user id', annotations[0]);
                // element.setPageNumber(this.pdf_current_page);
                if ((u_id+'_'+time_id) == annotations[0].Id) {
                  console.log('sign here button X', element.X);
                  console.log('sign here button Y', element.Y);
                  element.setPageNumber(annotations[0].PageNumber);
                  element.setX(annotations[0].X + 100);
                  element.setY(annotations[0].Y - 10);
                  // annotationManager.updateAnnotation(element);
                  annotationManager.drawAnnotationsFromList([element]);
                }

              });

              console.log('SignatureWidgetAnnotation', signature_widget_annotation);
            }

          } else if (action === 'delete') {
            console.log('deleted annotation', annotations);
            console.log('before', this.selectedUser);
            annotations.forEach((element, i) => {
              console.log(element);

              console.log(element.Id);
              let index = this.selectedUser.findIndex(ele => ele.id === element.Id);
              console.log(index);
              if (!this.is_deleted_by_btn && index >= 0) {
                this.removeSigner(index)
              }

              this.selectedUser = this.selectedUser.filter(t => t.id !== element.Id);
              this.cdr.detectChanges();
              this.is_deleted_by_btn = false;
            });


            console.log('After', this.selectedUser);
            console.log('there were annotations deleted');

          }
          this.is_annots_loaded = true;
        });


        instance.UI.loadDocument(documentBlobObjectUrl);

        instance.UI.enableElements(['contentEditButton']);
        // later save the document with updated annotations data
        documentViewer.addEventListener('annotationsLoaded', () => {
          console.log('annotation loaded');

          const annots = annotationManager.getAnnotationsList();
          const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
          const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
         console.log(widgetAnnots);
         
          
          widgetAnnots.forEach((element, i) => {
            console.log('user_id',element.getCustomData('user_id'));
            const user_id = element.getCustomData('user_id');
           
            this.shared_file.shared_file.forEach(data => {
              //  this.addSharedSigner(element,true);
              console.log(data);
              const id =  data.user ? data.user.id : data.other_user_email+ ' '+data.other_user_name
              if(user_id == id) {
               
              const name = data.user? (data.user.firstName + ' '+ data.user.lastName) : data.other_user_name;
              const email =  data.user ? data.user.email : data.other_user_email;
              const e = {
                bindName: name,
                email,
                firstName: data.user? (data.user.firstName) : data.other_user_name,
                id:  data.user? (data.user.id) : data.other_user_email + ' ' + data.other_user_name,
                lastName:  data.user? (data.user.lastName) : ''
              }
              this.selectedUser.push(e)
                 if(data.user) {
                  this.old_user_ids.push(data.user.id);
                }else {
                  this.old_user_ids.push(email+ ' '+name);
                }
                console.log('old_user_ids',this.old_user_ids);
                
  
                this.signerEmails().push(this.newSigner(email,name));
              }
            });
          });
          
          
        })
      })
  }

  blobImage(blob: any,api_data) {
    // console.log();

    // console.log('new ids',this.new_user_ids);
    // console.log('old ids',this.old_user_ids);
    // console.log('rm ids',this.rm_user_ids);
    

    // return
    
    const formData = new FormData();
    formData.append('file', blob, this.shared_file.shared_file.fileName);
    console.log('blobbbb',blob);
    
    this.cs.pdfUpload(formData).subscribe(
      (events: any) => {
       
        if (events.type === HttpEventType.Response) {
          console.log(events.body);
          api_data.FileData.filename = events.body.filename;
          this.updateFile(api_data);
         

        }
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      }
    );
  }
  updateFile(api_data) {
    this.cs.httpRequest('post','file-management/edit-sharing-file',api_data).subscribe(
      (res:any) => {
       console.log(res);
       this.router.navigateByUrl('/file-management/listing');
       this.redirectLink();
      this.cs.showInfoMsg('File Saved Successfully', 'Saved');
      },
      (err: any) => {
        if (err.status == 401) return this.cs.unAuthorized();
        this.cs.showErrorMsg(err.message, 'Error');
      });
  }
  redirectLink() {
    this.router.navigateByUrl('/file-management/shared');
  }
  saveFile() {

    console.log(this.is_submit);
    console.log(this.signerEmails().controls.length);
   
    if(!this.signerEmails().controls.length) {
      this.is_submit = false;
      return
    }
    this.is_submit = true;

      this.saveBtn.click();
   let  btn =  document.getElementById('savebtn')
   console.log(btn); 
  }
  handleDenial() {

  }
  handleDismiss(e) {
    
  }

  disableElementsForAdmin() {
    return [
      'viewControlsButton',
      'downloadButton',
      'printButton',
      'fileAttachmentToolGroupButton',
      // 'toolbarGroup-Edit',
      'crossStampToolButton',
      'checkStampToolButton',
      'dotStampToolButton',
      'rubberStampToolGroupButton',
      'dateFreeTextToolButton',
      'toolbarGroup-Shapes',
      'toolbarGroup-Forms',
      'shapeToolGroupButton',
      'highlightToolGroupButton',
      'freeHandHighlightToolGroupButton',
      'freeHandToolGroupButton',
      'selectToolButton',
      'strikeoutToolGroupButton',
      'squigglyToolGroupButton',
      'calloutToolGroupButton',
      'dropdown-item-toolbarGroup-Annotate',
      'linkButton',
       'dropdown-item-toolbarGroup-FillAndSign',
       'toolbarGroup-FillAndSign',
      'dropdown-item-toolbarGroup-Insert',
      'freeTextToolButton',
      'freeHandToolButton',
      'eraserToolButton',
      // 'redoButton',
      // 'undoButton',
      'dropdown-item-toolbarGroup-View',
      'dropdown-item-toolbarGroup-Shapes',
      'toggleNotesButton',
      'dropdown-item-toolbarGroup-Shapes',
      'toolbarGroup-Insert',
      'toolbarGroup-Shapes',
      'toolbarGroup-Annotate',
      'freeTextToolGroupButton',
      'toolbarGroup-View'
    ]
  }

  signerEmails(): FormArray {
    return this.productForm.get("signer_emails") as FormArray
  }

  newSigner(email = null,name=null): FormGroup {
    return this.fb.group({
      email: name,
      name: email 
    })
  }


  addSigner(is_new = null) {

    this.signerEmails().push(this.newSigner());
  }
  addSharedSigner(data,is_annots_loaded=false) {

   
    const name = data.user? (data.user.firstName + ' '+ data.user.lastName) : data.other_user_name;
    const email =  data.user ? data.user.email : data.other_user_email;

    // if(data.user) {
    //   this.old_user_ids.push(data.user.id);
    // }else {
    //   this.old_user_ids.push(email+ ' '+name);
    // }
    // console.log('old_user_ids',this.old_user_ids);
    

    // this.signerEmails().push(this.newSigner(email,name));
    const e = {
      bindName: name,
      email,
      firstName: data.user? (data.user.firstName) : data.other_user_name,
      id:  data.user? (data.user.id) : data.other_user_email + ' ' + data.other_user_name,
      lastName:  data.user? (data.user.lastName) : ''
    }
    if(!is_annots_loaded) {
      this.onAddSharedUser(e);
    }
  }

  removeSigner(i: number) {
    this.signerEmails().removeAt(i);
  }

  onSubmit() {
    console.log(this.productForm.value);
  }

  onRemoveSelectedUser(index) {
    console.log('index', index);
    console.log('selectedUser', this.selectedUser);
    this.selected_rm_index = index;
    if (this.selectedUser.length) {
      this.selected_rm_user_id = this.selectedUser[index].id && typeof this.selectedUser[index].id == 'number' ? this.selectedUser[index].id : this.selectedUser[index].email + ' ' + this.selectedUser[index].firstName;
    }


    if (index > -1) {
      this.selectedUser.splice(index, 1); // 2nd parameter means remove one item only
      this.removeSigner(index);
    }
    console.log('selectedUser', this.selectedUser);

    console.log('selected_rm_user_id', this.selected_rm_user_id);

    const is_exist =  this.old_user_ids.includes(this.selected_rm_user_id);
   const is_duplicate = this.old_user_ids.some((element, index) => {
    if(this.selected_rm_user_id == element) {
    return this.old_user_ids.indexOf(element) !== index;
    }
  });
   console.log(is_duplicate);
   if(is_duplicate) { 
    this.old_user_ids.splice(index, 1);
   }
   

    if(is_exist && !is_duplicate) {
      this.rm_user_ids.push( this.selected_rm_user_id);
    }else if(!is_duplicate) { 
      let new_index;
      const is_new_duplicate = this.new_user_ids.some((element, index) => {
        if(this.selected_rm_user_id == element) {
          new_index = index;
          return this.new_user_ids.indexOf(element) !== index;
        }
      });
      if(is_new_duplicate) { 
        this.new_user_ids.splice(new_index, 1);
       }
       else  {
        this.new_user_ids = this.new_user_ids.filter((ele) =>{ 
          return ele != this.selected_rm_user_id; 
        });
      }
    }


    let btn = document.getElementById('rm_annot_btn')
    btn.click();

  }

  addUser() {

    if (this.other_signer_name == '' || this.other_signer_email == '') {
      return
    }

    console.log(this.signerEmails().length);
    console.log(this.signerEmails());
    this.other_signer_name = this.other_signer_name.trim();
    this.other_signer_email = this.other_signer_email.trim();

    if (this.signerEmails().length && !this.signerEmails().controls[this.signerEmails().length - 1].value.email) {
      this.signerEmails().controls[this.signerEmails().length - 1].patchValue({ email: this.other_signer_email });
      this.selectedUser = [{ firstName: this.other_signer_name + ' ', email: this.other_signer_email, id: this.other_signer_email + ' ' + this.other_signer_name }];
    } else {
      this.signerEmails().push(this.newSigner(this.other_signer_email, this.other_signer_name));
      this.selectedUser.push({ firstName: this.other_signer_name + ' ', email: this.other_signer_email, id: this.other_signer_email + ' ' + this.other_signer_name });
    }
    const check_id = this.other_signer_email + ' ' + this.other_signer_name;

    const is_exist =  this.old_user_ids.includes(check_id);
    const is_rm_exist =  this.rm_user_ids.includes(check_id);
    console.log('is_exist',is_exist);
    
    if(!is_exist) {
     this.new_user_ids.push(check_id);
    }
 
    if(is_rm_exist) {
     this.rm_user_ids = this.rm_user_ids.filter((ele) =>{ 
       return ele != check_id; 
     });
    }
    // this.new_user_ids.push(this.other_signer_email + ' ' + this.other_signer_name);
    this.selected_user_name = this.other_signer_name;
    this.selected_user_email = this.other_signer_email;
    // this.selected_user_id = this.selectedUser3[this.selectedUser3.length-1].id;
    // this.selected_user_id = this.selectedUser3[this.selectedUser3.length-1].firstName;
    // this.selectedUser = this.selectedUser.filter(t => t.id !== 'element.Id'); // use for detect changes on ng select
    console.log('other_signer_name', this.other_signer_name);


    // this.cdr.detectChanges();

    // console.log('last id length',(this.selectedUser[this.selectedUser.length - 1].id).length);
    // console.log('last id trim length',(this.selectedUser[this.selectedUser.length - 1].id).trim().length);
    // return;
    

    let btn = document.getElementById('add_annot_btn')
    btn.click();
    this.is_add_other_user = false;
    this.other_signer_name = '';
    this.other_signer_email = '';

  }

}
