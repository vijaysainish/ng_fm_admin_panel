import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { throttle } from 'rxjs/operators';
import { CommonService } from 'src/app/services/common.service';
import { ExcelService } from 'src/app/services/excel/excel.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css'],
})
export class ListingComponent implements OnInit {
  myListing: boolean = false;
  isAdmin: boolean = false;
  shared_files:any;
  shared_with_me_fiels:any;
  my_uploaded:any;
  type:any = 'uploaded-by-me';
  user_details:any;
  file_details:any  = '';
  status:any = 0;
  totalPages: number;
  totalCount: any;

  total_me_pages: number;
  total_me_count: any;

  total_sharing_pages: number;
  total_sharing_count: any;

  page: number = 1;
  start_date:any;
  end_date:any;
  comment:any;
   searchForm: FormGroup;
   selected_files =[]
  constructor(private modalService: NgbModal, private cs: CommonService,private excelSrv: ExcelService,private fb: FormBuilder) {
    this.isAdmin = this.cs.isAdmin();
    this.myListing = this.cs.isAdmin();
    this.searchForm = this.fb.group({
      keyword: [''],
      role: [''],
      date: [null],
      // end_date: [null],
      status: [''],
    });
    this.fetchUserDetails();
  }

  ngOnInit(): void {
    this.fetchFiles();
    if (!this.isAdmin) {
      this.sharedWithMeApi();
    }
    
  }

  fetchFiles(page =1) {
    this.cs.httpRequest('get',`file-management/files-list?page=${page}&limit=10`).subscribe((res:any) =>{
      console.log(res);
      this.shared_files = this.my_uploaded = res.list;
      this.total_me_count = res.total;
      // this.data = this.users;
      this.total_me_pages = Math.ceil(this.total_me_count / 10);

      this.totalCount = this.total_me_count;
    this.totalPages = this.total_me_pages;
    });
  }

  search() {

    console.log( this.searchForm.value);
    
    let url =  this.type == 'shared-with-me' ? 'search-shared-with-me' : 'search-listing-file';
    this.cs.httpRequest('post', `file-management/${url}`, this.searchForm.value).subscribe((res:any) => {
      console.log(res);
      this.shared_files = res.list;
      this.totalCount = res.total;
      // this.data = this.users;
      this.totalPages = Math.ceil(this.totalCount / 10);
    })
  }

  fetchPage(page) {
    this.page = page == 1 ? this.page + 1 : this.page - 1;
    this.fetchFiles(this.page);
  }
  openShared(content,file) {
    
    this.file_details = file.file? file.file :file;

    console.log( this.file_details);
    

    this.start_date = new Date(this.file_details.startDate).toISOString().substr(0, 10);
    this.end_date = new Date(this.file_details.endDate).toISOString().substr(0, 10);
  
    this.comment = this.file_details.comment &&this.file_details.comment.length ? this.file_details.comment[0].comment : '';
    this.modalService.open(content);
  }

  sharedWithMeApi(page =1) {
    this.cs.httpRequest('get',`file-management/files-shared-with-me?page=${page}&limit=10`).subscribe((res:any) =>{
      console.log(res);
      this.shared_with_me_fiels = res.list;
      if(this.type == 'shared-with-me') {
        this.shared_files = this.shared_with_me_fiels;
      }
      
    

      this.total_sharing_count = res.total;
      // this.data = this.users;
      this.total_sharing_pages = Math.ceil(this.total_sharing_count / 10);

      this.totalCount = this.total_sharing_count;
      this.totalPages = this.total_sharing_pages;
    });
  }
  sharedWithMeFiles() {
    this.type = 'shared-with-me';
    this.shared_files = this.shared_with_me_fiels;
    this.totalCount = this.total_sharing_count;
      this.totalPages = this.total_sharing_pages;
      this.reset();
  }
  myUploadedFiles() {
    console.log( this.my_uploaded);
    this.type = 'uploaded-by-me';
    this.shared_files =  this.my_uploaded;

    this.totalCount = this.total_me_count;
    this.totalPages = this.total_me_pages;
    this.reset();
  }

  fetchUserDetails() {
    this.cs.httpRequest('get', `user`)?.subscribe(
      (res: any) => {
       console.log(res);
       this.user_details = res;
      },
      (err) => this.cs.handleError(err)
    );
  }

  updateFileStatus() {
    const data = {
      file_id:this.file_details.id,
      status:this.status
    }
    this.cs.httpRequest('patch','file-management/update-file-status',data).subscribe(res =>{
      console.log(res);
      // this.file_details.status = this.status.toUpperCase();
      this.file_details.status = this.status.charAt(0).toUpperCase() + this.status.slice(1);
      this.modalService.dismissAll();
      this.cs.showSuccessMsg('File status has been updated', 'Updated');
      // this.shared_files = res;
    })
  }

  shareFile() {
    if(!this.start_date || !this.end_date) {
      return
    }
    const data = {
      file_id: this.file_details.id,
     comment :  this.comment,
     startDate: new Date(this.start_date).toISOString(),
     endDate: new Date(this.end_date).toISOString()
    }
    this.cs.httpRequest('post', 'file-management/share-file', data).subscribe(res => {
      console.log(res);
     
      this.modalService.dismissAll();
      this.fetchFiles();
      this.cs.showSuccessMsg('File shared successfully', 'Success');
    },(err) => this.cs.handleError(err))
  }

  deleteFile() {
    const data = {
      file_id: this.file_details.id,
    }
    this.cs.httpRequest('delete', 'file-management/delete-file', data).subscribe(res => {
      console.log(res);
      this.modalService.dismissAll();
     this.fetchFiles();
    })
  }

  exportData(tableId: string) {

   
    console.log(this.pushToArray(this.shared_files));
    
    // return
     let data:any = this.selected_files.length ? this.pushToArray(this.selected_files) : this.pushToArray(this.shared_files)
     // this.excelSrv.exportToFile('users_data', tableId);
     this.excelSrv.exportAsExcelFile(data,'files_list');
   }
   pushToArray = (arr = []) => {
     const result = arr.reduce((acc, obj,i) => {
       // console.log(i);
       // console.log(acc);
       // console.log('obj',obj);
       const is_exist =  this.selected_files.includes(obj.id);
       
      delete obj['comment'];
      delete obj['initiator_id'];
      delete obj['is_editable'];
      delete obj['profilePic'];
      delete obj['is_sharing'];
      delete obj['updatedAt'];
      delete obj['fileName'];
      delete obj['deletedAt'];
     //  delete obj['createdAt'];
     //  delete obj['id'];
      obj.id = i+1;
     
      // obj.firstName =  obj.firstName;
      // obj.isActive = obj.isActive ? 'Active':'Inactive';
      obj.startDate = new Date(obj.startDate);
      obj.endDate = new Date(obj.endDate);
      obj.createdAt = new Date(obj.createdAt);
      acc.push(obj);
     
     // acc.push(obj.firstName);
     return acc;
  }, []);
     return result;
  };
 
  selectFiles(e,file) {
    console.log(e.target.checked );
    console.log(file);
 
    if(e.target.checked) {
      this.selected_files.push(file);
    }else {
     this.selected_files = this.selected_files.filter((ele) =>{ 
       return ele.id != file.id; 
     });
    }
 
    console.log( this.selected_files);
    
    
  }

  reset() {
    this.searchForm.reset();
    this.searchForm.patchValue({role:''});
    this.searchForm.patchValue({status:''});
    this.searchForm.patchValue({keyword:''});
   
   
      if (!this.isAdmin && this.type == 'shared-with-me') {
        this.sharedWithMeApi();
      }else {
        this.fetchFiles();
      }
  }
}
