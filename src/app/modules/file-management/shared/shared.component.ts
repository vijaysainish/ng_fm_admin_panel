import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { ExcelService } from 'src/app/services/excel/excel.service';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css'],
})
export class SharedComponent implements OnInit {
  shared_files: any;
  type: any = 'uploaded-by-me';
  file_details: any = '';
  status: any = 0;
  searchForm: FormGroup;
  totalPages: number;
  totalCount: any;
  page: number = 1;
  start_date:any;
  end_date:any;
  comment:any;
  comment_id:number;
  selected_files =[]
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private cs: CommonService,
    private fb: FormBuilder,
    private excelSrv: ExcelService
  ) {
    if (!this.cs.isAdmin()) {
      this.router.navigateByUrl('/file-management/listing');
    }

    this.searchForm = this.fb.group({
      keyword: [''],
      role: [''],
      date: [null],
      // end_date: [null],
      status: [''],
    });


  }

  ngOnInit(): void {
    // this.cs.httpRequest('get','file-management/files-list').subscribe(res =>{
    //   console.log(res);
    //   this.shared_files = res;
    // })
    this.fetchFiles();

  }
  fetchFiles(page = 1) {

    this.cs.httpRequest('get', `file-management/sharing-files?page=${page}&limit=10`).subscribe((res:any) => {
      console.log(res);
      this.shared_files = res.list;
      this.totalCount = res.total;
      // this.data = this.users;
      this.totalPages = Math.ceil(this.totalCount / 10);
    })
  }

  fetchPage(page) {
    this.page = page == 1 ? this.page + 1 : this.page - 1;
    this.fetchFiles(this.page);
  }
  openShared(content, file = null) {
    this.file_details = file.file ? file.file : file;
    // this.start_date = this.file_details.startDate;

    this.start_date = new Date(this.file_details.startDate).toISOString().substr(0, 10);
    this.end_date = new Date(this.file_details.endDate).toISOString().substr(0, 10);
 
    this.comment = this.file_details.comment.length ? this.file_details.comment[0].comment : '';
    this.comment_id = this.file_details.comment.length ? this.file_details.comment[0].id : null;
    console.log( this.start_date );
    console.log( this.file_details );
    
    this.modalService.open(content);
  }

  shareFile() {
    if(!this.start_date || !this.end_date) {
      return
    }
    const data = {
      file_id: this.file_details.id,
     comment :  this.comment,
     comment_id: this.comment_id,
     startDate: new Date(this.start_date).toISOString(),
     endDate: new Date(this.end_date).toISOString()
    }
    this.cs.httpRequest('post', 'file-management/share-file', data).subscribe(res => {
      console.log(res);
     
      this.modalService.dismissAll();
      this.fetchFiles();
      this.cs.showSuccessMsg('File shared successfully', 'Success');
    },(err) => this.cs.handleError(err))
  }

  search() {
    console.log(this.searchForm.value);

    this.cs.httpRequest('post', 'file-management/search-sharing-file', this.searchForm.value).subscribe((res:any) => {
      console.log(res);
      this.shared_files = res.list;
      this.totalCount = res.total;
      // this.data = this.users;
      this.totalPages = Math.ceil(this.totalCount / 10);
    })
  }

  updateFileStatus() {
    const data = {
      file_id: this.file_details.id,
      status: this.status
    }
    this.cs.httpRequest('patch', 'file-management/update-file-status', data).subscribe(res => {
      console.log(res);
      this.file_details.status = this.status;
      this.modalService.dismissAll();
      this.cs.showSuccessMsg('File status has been updated', 'Updated');
      // this.shared_files = res;
    })
  }

  deleteFile() {
    const data = {
      file_id: this.file_details.id,
    }
    this.cs.httpRequest('delete', 'file-management/delete-file', data).subscribe(res => {
      console.log(res);
      this.modalService.dismissAll();
     this.fetchFiles();
    })
  }

  // exportData(tableId: string) {
  //   this.excelSrv.exportToFile('Files', tableId);
  // }


  exportData(tableId: string) {

   
    console.log(this.pushToArray(this.shared_files));
    
    // return
     let data:any = this.selected_files.length ? this.pushToArray(this.selected_files) : this.pushToArray(this.shared_files)
     // this.excelSrv.exportToFile('users_data', tableId);
     this.excelSrv.exportAsExcelFile(data,'sharing_files_list');
   }
   pushToArray = (arr = []) => {
     const result = arr.reduce((acc, obj,i) => {
       // console.log(i);
       // console.log(acc);
       // console.log('obj',obj);
       const is_exist =  this.selected_files.includes(obj.id);
       
      delete obj['comment'];
      delete obj['initiator_id'];
      delete obj['is_editable'];
      delete obj['profilePic'];
      delete obj['is_sharing'];
      delete obj['updatedAt'];
      delete obj['fileName'];
      delete obj['deletedAt'];
     //  delete obj['createdAt'];
     //  delete obj['id'];
      obj.id = i+1;
     
      // obj.firstName =  obj.firstName;
      // obj.isActive = obj.isActive ? 'Active':'Inactive';
      obj.startDate = new Date(obj.startDate);
      obj.endDate = new Date(obj.endDate);
      obj.createdAt = new Date(obj.createdAt);
      acc.push(obj);
     
     // acc.push(obj.firstName);
     return acc;
  }, []);
     return result;
  };
 
  selectFiles(e,file) {
    console.log(e.target.checked );
    console.log(file);
 
    if(e.target.checked) {
      this.selected_files.push(file);
    }else {
     this.selected_files = this.selected_files.filter((ele) =>{ 
       return ele.id != file.id; 
     });
    }
 
    console.log( this.selected_files);
    
    
  }

  reset() {
    this.searchForm.reset();
    this.searchForm.patchValue({role:''});
    this.searchForm.patchValue({status:''});
    this.searchForm.patchValue({keyword:''});
    this.fetchFiles();
  }
}
