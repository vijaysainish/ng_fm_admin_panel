import { Component } from '@angular/core';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public cs:CommonService) {
    // alert(location.hostname)
    if (localStorage.getItem('debug') == 'false' && (location.hostname !== "localhost")) {
      window.console.log = function () { };   // disable any console.log debugging statements in production mode
      window.console.error = function () { };
    }

  }
  ngOnInit(): void {
    this.getNotifications();
  }
   getNotifications() {
    this.cs.httpRequest('get', 'notifications/user-notifications').subscribe((res:any) => {
      console.log(res);
      this.cs.updateApprovalMessage(res.unread_msg);
    })
  }
  
}
