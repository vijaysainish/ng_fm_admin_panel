import { CookieService } from 'ngx-cookie';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map } from 'rxjs/operators';
import { baseUrl } from '../Constants/constants';
import { FormGroup, FormControl } from '@angular/forms';
import { isPlatformBrowser } from '@angular/common';
import iziToast from 'izitoast';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  baseUrl = baseUrl;
  private approvalStageMessage = new BehaviorSubject('');
  currentApprovalStageMessage = this.approvalStageMessage.asObservable();
 

  private userProfilePic = new BehaviorSubject('');
  getUpdatedProfilePic = this.userProfilePic.asObservable();

  private userLogoPic = new BehaviorSubject('');
  getUpdatedLogoPic = this.userLogoPic.asObservable();
  constructor(
    @Inject(PLATFORM_ID) private _platformId: Object,
    private http: HttpClient,
    private cookie: CookieService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
  ) {
    this.initIziToast();
  }

  updateApprovalMessage(message: any) {
    this.approvalStageMessage.next(message)
    }

  updateProfilePic(img: any) {
    console.log('imge updated');
      this.userProfilePic.next(img)
  }
  updateLogoPic(img: any) {
    this.userLogoPic.next(img)
  }

  getHeader() {
    let token = this.cookie.get('token') || '';
    console.log(token);

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    });
    return headers;
  }

  isAdmin(): boolean {
    let user: any = JSON.parse(this.cookie.get('user') || null);
    if (!user) {
      return false;
    }
    if (user.role === 'Admin') return true;
    return false;
  }

  httpRequest(req: string, url: string, data?: any) {
    if (req == 'get') {
      return this.http.get(this.getUrl(url), {
        headers: this.getHeader(),
      });
    } else if (req == 'post') {
      return this.http.post(this.getUrl(url), data, {
        headers: this.getHeader(),
      });
    } else if (req == 'patch') {
      return this.http.patch(this.getUrl(url), data, {
        headers: this.getHeader(),
      });
    } else if (req == 'put') {
      return this.http.put(this.getUrl(url), data, {
        headers: this.getHeader(),
      });
    } else if (req == 'del' || req == 'delete' || req == 'dlt') {
      let request = {
        body: data,
        headers: this.getHeader(),
      };
      return this.http.delete(this.getUrl(url), request);
    }
    return null;
  }

  getUrl(url: string) {
    return this.baseUrl + url;
  }

  navigate(url: string,data=null) {
    if(data) {
      this.router.navigateByUrl(url,data);
    }else {
      this.router.navigateByUrl(url);
    }
    
  }

  setCookie(name: string, value: string) {
    this.cookie.put(name, value, {
      path: '/',
    });
  }

  setStrCookie(name: string, value: any) {
    value = JSON.stringify(value);
    this.cookie.put(name, value, {
      path: '/',
    });
  }

  getCookie(name: string) {
    return this.cookie.get(name);
  }

  getJSONCookie(name: string) {
    let val = this.cookie.get(name);
    return JSON.parse(val);
  }

  deleteAllCookies() {
    this.cookie.removeAll({
      path: '/',
    });
  }
login(){
  if(this.isAdmin()){
    this.navigate('/dashboard');
    
  }
  this.deleteAllCookies();

}
  logout() {
    if(this.isAdmin()){
      this.showSuccessMsg('Logged Out', 'Admin Logged out Successfully');
       this.navigate('/admin/login');
    
      }
   else if(!this.isAdmin()){
    this.showSuccessMsg('Logged Out', 'Users Logged out Successfully');
   this.navigate('/auth/login')
  
  }
  this.deleteAllCookies();
  
  }

  setTitle() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => {
          let child = this.activatedRoute;
          while (child) {
            if (child.firstChild) {
              child = child.firstChild;
            } else if (child.snapshot.data && child.snapshot.data['title']) {
              return child.snapshot.data['title'];
            } else {
              return null;
            }
          }
          return null;
        })
      )
      .subscribe((data: any) => {
        if (data) {
          this.setTitleValue(data);
        }
      });
  }

  setTitleValue(val: string) {
    this.titleService.setTitle('Potswork : ' + val);
  }

  initIziToast() {
    iziToast.settings({
      timeout: 3000,
      pauseOnHover: true,
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX',
    });
  }

  showSuccessMsg(title?: string, msg?: string, position?: undefined) {
    iziToast.success({
      position: position || 'bottomRight',
      title: title,
      overlay: position ? true : false,
      message: msg,
    });
  }
  imgUpload(data: any) {
    return this.http.post(this.getUrl('file/upload/image'), data, {
      reportProgress: true,
      observe: 'events',
    });
  }
  pdfUpload(data: any) {
    return this.http.post(this.getUrl('file-management/upload'), data, {
      reportProgress: true,
      observe: 'events',
    });
  }
  showInfoMsg(title?: string, msg?: string, position?: any) {
    iziToast.info({
      title: title,
      position: position || 'bottomRight',
      overlay: position ? true : false,
      message: msg,
    });
  }

  showWarningMsg(title?: string, msg?: string, position?: any) {
    iziToast.warning({
      title: title,
      position: position || 'bottomRight',
      overlay: position ? true : false,
      message: msg,
    });
  }

  showErrorMsg(title?: string, msg?: string, position?: undefined) {
    iziToast.error({
      title: title,
      position: position || 'bottomRight',
      overlay: position ? true : false,
      message: msg,
    });
  }

  showSomethingWentWrong(title?: string, msg?: string, position?: any) {
    iziToast.error({
      title: 'Oops!',
      position: position || 'bottomRight',
      overlay: position ? true : false,
      message: 'Something Went Wrong!',
    });
  }

  handleError(err: any) {
    console.log(err)
    if (err.status == 401) {
      return this.unAuthorized();
    } else if (err.status == 0) {
      return this.showErrorMsg(
        'Unkown Error',
        'Please Check Your Internet Connection'
      );
    } else if (
      err.status == 500 ||
      err.status == 501 ||
      err.status == 502 ||
      err.status == 503 ||
      err.status == 504 ||
      err.status == 505
    ) {
      return this.showErrorMsg(
        'Server Side Error',
        'Something Went Wrong on Server'
      );
    } else if (err.status == 404) {
      return this.showErrorMsg('Not Found', 'Data Missing');
    } else if (err.status == 403) {
      return this.showErrorMsg(
        'Not Allowed',
        'Not Authorized to access this Page'
      );
    } else {
      this.showErrorMsg('Ooops!', err.error.message);
    }
  }

  unAuthorized() {
    this.navigate('/auth/login');
    this.showErrorMsg('Unauthorized', 'Please Contact to admin');
    this.deleteAllCookies();
  }

  matchPass(pass: string, confirm: string) {
    return (group: FormGroup) => {
      let passkey = group.controls[pass];
      let confirmpasskey = group.controls[confirm];

      if (passkey.value.toString() !== confirmpasskey.value.toString()) {
        return {
          mismatch: true,
        };
      }
      return null;
    };
  }

  isBrowser() {
    if (isPlatformBrowser(this._platformId)) {
      return true;
    }
    return false;
  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid
      ? null
      : {
          whitespace: true,
        };
  }

  noWhitespaceValidatorModel(control: any) {
    const isWhitespace = (control || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid;
  }
}
