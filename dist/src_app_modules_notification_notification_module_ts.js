(self["webpackChunkfilesharing"] = self["webpackChunkfilesharing"] || []).push([["src_app_modules_notification_notification_module_ts"],{

/***/ 3621:
/*!*********************************************************************!*\
  !*** ./src/app/modules/notification/notification-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotificationRoutingModule": () => (/* binding */ NotificationRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _notification_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notification.component */ 8855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);




const routes = [{
        path: '',
        component: _notification_component__WEBPACK_IMPORTED_MODULE_0__.NotificationComponent
    }];
class NotificationRoutingModule {
}
NotificationRoutingModule.ɵfac = function NotificationRoutingModule_Factory(t) { return new (t || NotificationRoutingModule)(); };
NotificationRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: NotificationRoutingModule });
NotificationRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](NotificationRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule] }); })();


/***/ }),

/***/ 8855:
/*!****************************************************************!*\
  !*** ./src/app/modules/notification/notification.component.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotificationComponent": () => (/* binding */ NotificationComponent)
/* harmony export */ });
/* harmony import */ var _Constants_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Constants/constants */ 5029);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/common.service */ 5620);
/* harmony import */ var _shared_components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/components/breadcrumb/breadcrumb.component */ 1299);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _shared_pipes_custom_datepipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/pipes/custom.datepipe */ 1970);








function NotificationComponent_div_7_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate2"]("", notification_r2.send_by_user.firstName, " ", notification_r2.send_by_user.lastName, "");
} }
function NotificationComponent_div_7_p_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate3"](" ", notification_r2.send_by_user.firstName, " ", notification_r2.send_by_user.lastName, " has been signed the '", notification_r2.shared_file.originalFileName, "' file. ");
} }
function NotificationComponent_div_7_p_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", notification_r2.message, " ");
} }
function NotificationComponent_div_7_p_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate3"](" ", notification_r2.send_by_user.firstName, " ", notification_r2.send_by_user.lastName, " has been shared the '", notification_r2.shared_file.originalFileName, "' file with you. ");
} }
function NotificationComponent_div_7_p_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, " Please add sign, Now it's your time to submit the signature. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function NotificationComponent_div_7_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "Accepted");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](notification_r2.message);
} }
function NotificationComponent_div_7_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "Rejected");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](notification_r2.message);
} }
function NotificationComponent_div_7_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "File Status updated");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate2"](" File ", notification_r2.shared_file.originalFileName, " is ", notification_r2.message, " by Inittiator");
} }
function NotificationComponent_div_7_p_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, " Inittiator has been reshare the edited file with you. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { "error-msg": a0 }; };
function NotificationComponent_div_7_div_13_Template(rf, ctx) { if (rf & 1) {
    const _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "textarea", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function NotificationComponent_div_7_div_13_Template_textarea_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r21); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](2); return ctx_r20.message = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "button", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function NotificationComponent_div_7_div_13_Template_button_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r21); const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit; const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r22.updateFileStatus(notification_r2, 1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](5, " Accept ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "button", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function NotificationComponent_div_7_div_13_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r21); const notification_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit; const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r24.updateFileStatus(notification_r2, 2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](7, "Reject");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](2, _c0, ctx_r12.is_msg_error))("ngModel", ctx_r12.message);
} }
const _c1 = function () { return ["/file-management/upload/view"]; };
const _c2 = function (a0) { return { id: a0 }; };
function NotificationComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "img", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](4, NotificationComponent_div_7_span_4_Template, 2, 2, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](5, NotificationComponent_div_7_p_5_Template, 2, 3, "p", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](6, NotificationComponent_div_7_p_6_Template, 2, 1, "p", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](7, NotificationComponent_div_7_p_7_Template, 2, 3, "p", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](8, NotificationComponent_div_7_p_8_Template, 2, 0, "p", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](9, NotificationComponent_div_7_div_9_Template, 6, 1, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](10, NotificationComponent_div_7_div_10_Template, 6, 1, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](11, NotificationComponent_div_7_div_11_Template, 6, 2, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](12, NotificationComponent_div_7_p_12_Template, 2, 0, "p", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](13, NotificationComponent_div_7_div_13_Template, 8, 4, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](16, "customDate");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](17, "hr", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const notification_r2 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("src", notification_r2.send_by_user && notification_r2.send_by_user.profilePic ? ctx_r0.imgBaseUrl + notification_r2.send_by_user.profilePic : "../assets/images/user/3.jpg", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction0"](16, _c1))("queryParams", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](17, _c2, notification_r2.file_id));
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.send_by_user);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 2 && notification_r2.send_by_user);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 2 && !notification_r2.send_by_user);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.status == 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", notification_r2.send_to_user.id == ctx_r0.user_details.id && (notification_r2.status == 1 || notification_r2.status == 7) && notification_r2.request_sent);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](16, 14, notification_r2.createdAt), " ago ");
} }
function NotificationComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "No Notifications");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
class NotificationComponent {
    constructor(cs) {
        this.cs = cs;
        this.message = "";
        this.page = 1;
        this.notifications = [];
        this.imgBaseUrl = _Constants_constants__WEBPACK_IMPORTED_MODULE_0__.imgBaseUrl;
    }
    ngOnInit() {
        this.fetchUserDetails();
        this.getNotifications();
    }
    fetchUserDetails() {
        var _a;
        (_a = this.cs.httpRequest('get', `user`)) === null || _a === void 0 ? void 0 : _a.subscribe((res) => {
            console.log(res);
            this.user_details = res;
        }, (err) => this.cs.handleError(err));
    }
    fetchPage(page) {
        this.page = page == 1 ? this.page + 1 : this.page - 1;
        this.getNotifications(this.page);
    }
    getNotifications(page = 1) {
        this.cs.httpRequest('get', `notifications/user-notifications?page=${this.page}&limit=10`).subscribe((res) => {
            console.log(res);
            this.notifications = res.list;
            this.total = res.total;
            this.total = Math.ceil(this.total / 10);
            this.readAllNotifications();
        });
    }
    readAllNotifications() {
        this.cs.httpRequest('patch', 'notifications/read-all').subscribe((res) => {
            console.log(res);
            this.cs.updateApprovalMessage(0);
        });
    }
    updateFileStatus(data, status) {
        console.log(data);
        if (!this.message && status == 2) {
            this.is_msg_error = true;
            return;
        }
        else {
            data.request_sent = 0;
            this.is_msg_error = false;
            let req = {
                notification_id: data.id,
                file_id: data.file_id,
                status,
                initiator_id: data.send_by_user.id,
                message: this.message
            };
            console.log(req);
            this.cs.httpRequest('post', 'file-management/update-file-request-status', req).subscribe((res) => {
                console.log(res);
                this.cs.showSuccessMsg('Request has been sent to initiator', 'Requested');
            });
        }
    }
}
NotificationComponent.ɵfac = function NotificationComponent_Factory(t) { return new (t || NotificationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](src_app_services_common_service__WEBPACK_IMPORTED_MODULE_1__.CommonService)); };
NotificationComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: NotificationComponent, selectors: [["app-notification"]], decls: 14, vars: 6, consts: [[3, "title", "active_item"], [1, "container-fluid"], [1, ""], [1, "row"], [1, "col-xl-12", "col-md-12", "box-col-12"], [1, "card", "mb-0"], [1, "card-body", "social-status", "filter-cards-view"], ["class", "media notification-click", 4, "ngFor", "ngForOf"], ["class", "text-center", 4, "ngIf"], [1, "pull-right", "mb-5"], ["type", "button", "data-bs-original-title", "", "title", "", 1, "btn", "btn-primary", "m-r-15", 3, "disabled", "click"], ["type", "button", "data-bs-original-title", "", "title", "", 1, "btn", "btn-primary", 3, "disabled", "click"], [1, "media", "notification-click"], ["height", "50px", "alt", "", 1, "img-50", "rounded-circle", "m-r-15", 3, "src"], [1, "media-body"], [3, "routerLink", "queryParams"], ["class", "f-w-600 d-block", 4, "ngIf"], [4, "ngIf"], ["class", "p-t-15 pb-3", 4, "ngIf"], [1, "light-span"], [1, "hrline"], [1, "f-w-600", "d-block"], [1, "p-t-15", "pb-3"], ["placeholder", "Please enter message", "maxlength", "500", 1, "form-control", 3, "ngClass", "ngModel", "ngModelChange"], [1, "row", "btn-group-sm", "p-t-10"], [1, "btn", "w-auto", "ml-3", "mr-3", "btn-primary", 3, "click"], [1, "btn", "w-auto", "btn-danger", 3, "click"], [1, "text-center"]], template: function NotificationComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](0, "app-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](7, NotificationComponent_div_7_Template, 18, 19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](8, NotificationComponent_div_8_Template, 3, 0, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function NotificationComponent_Template_button_click_10_listener() { return ctx.fetchPage(-1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](11, " Previous ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function NotificationComponent_Template_button_click_12_listener() { return ctx.fetchPage(1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](13, " Next ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("title", "Notifications")("active_item", "Notifications");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx.notifications);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.notifications || !ctx.notifications.length);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.page == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.page >= ctx.total);
    } }, directives: [_shared_components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_2__.BreadcrumbComponent, _angular_common__WEBPACK_IMPORTED_MODULE_5__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_5__.NgIf, _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterLink, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.MaxLengthValidator, _angular_common__WEBPACK_IMPORTED_MODULE_5__.NgClass, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgModel], pipes: [_shared_pipes_custom_datepipe__WEBPACK_IMPORTED_MODULE_3__.CustomDatePipe], styles: [".notification-click[_ngcontent-%COMP%] {\r\ncursor: pointer;\r\n}\r\n.scrollbar-deep-purple[_ngcontent-%COMP%]::-webkit-scrollbar-track {\r\n    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);\r\n    background-color: #F5F5F5;\r\n    border-radius: 10px; }\r\n.scrollbar-deep-purple[_ngcontent-%COMP%]::-webkit-scrollbar {\r\n    width: 12px;\r\n    background-color: #F5F5F5; }\r\n.scrollbar-deep-purple[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\r\n    border-radius: 10px;\r\n    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);\r\n    background-color: #512da8; }\r\n.scrollbar-deep-purple[_ngcontent-%COMP%] {\r\n    scrollbar-color: #512da8 #F5F5F5;\r\n    }\r\n.thin[_ngcontent-%COMP%]::-webkit-scrollbar {\r\n    width: 6px; }\r\n.example-1[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    overflow-y: scroll;\r\n    overflow-x:hidden ;\r\n    height: 500px; }\r\n\r\n.col-1[_ngcontent-%COMP%] {width: 8.33%;}\r\n.col-2[_ngcontent-%COMP%] {width: 16.66%;}\r\n.col-3[_ngcontent-%COMP%] {width: 25%;}\r\n.col-4[_ngcontent-%COMP%] {width: 33.33%;}\r\n.col-5[_ngcontent-%COMP%] {width: 41.66%;}\r\n.col-6[_ngcontent-%COMP%] {width: 50%;}\r\n.col-7[_ngcontent-%COMP%] {width: 58.33%;}\r\n.col-8[_ngcontent-%COMP%] {width: 66.66%;}\r\n.col-9[_ngcontent-%COMP%] {width: 75%;}\r\n.col-10[_ngcontent-%COMP%] {width: 83.33%;}\r\n.col-11[_ngcontent-%COMP%] {width: 91.66%;}\r\n.col-12[_ngcontent-%COMP%] {width: 100%;}\r\n@media only screen and (max-width: 768px) {\r\n  [class*=\"col-\"][_ngcontent-%COMP%] {\r\n    width: 100%;\r\n  }\r\n}\r\n@media screen and (min-width: 320px) {\r\n    .col-8[_ngcontent-%COMP%] {\r\n        flex: 0 0 50% !important;\r\n        max-width: 50% !important;\r\n    }\r\n}\r\n@media screen and (min-width: 321px) and (max-width:480px){\r\n    .col-8[_ngcontent-%COMP%] {\r\n        flex: 0 0 50% !important;\r\n        max-width: 50% !important;\r\n    }\r\n}\r\n@media screen and (min-width: 481px) and (max-width:720px) {\r\n    .col-8[_ngcontent-%COMP%] {\r\n        flex: 0 0 50% !important;\r\n        max-width: 50% !important;\r\n    }\r\n}\r\n@media screen and(min-width: 721px) and (max-width:1080px) {\r\n    .col-8[_ngcontent-%COMP%] {\r\n        flex: 0 0 50% !important;\r\n        max-width: 50% !important;\r\n    }\r\n}\r\n@media screen and(min-width: 1080px) and (max-width: 1280px){\r\n    .col-8[_ngcontent-%COMP%] {\r\n        flex: 0 0 50% !important;\r\n        max-width: 50% !important;\r\n    }\r\n}\r\n@media screen and (min-width: 1281px) and (max-width: 1920px){\r\n    .col-8[_ngcontent-%COMP%] {\r\n        flex: 0 0 50% !important;\r\n        max-width: 50% !important;\r\n    }\r\n}\r\n.error-msg[_ngcontent-%COMP%] {\r\n    border-color: red;\r\n}\r\n.hrline[_ngcontent-%COMP%] {\r\n    width: 96%;\r\n    background-color: #ededed;\r\n    height: 0px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vdGlmaWNhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtJQUNJLG9EQUFvRDtJQUNwRCx5QkFBeUI7SUFDekIsbUJBQW1CLEVBQUU7QUFFckI7SUFDQSxXQUFXO0lBQ1gseUJBQXlCLEVBQUU7QUFFM0I7SUFDQSxtQkFBbUI7SUFDbkIsb0RBQW9EO0lBQ3BELHlCQUF5QixFQUFFO0FBRTNCO0lBQ0EsZ0NBQWdDO0lBQ2hDO0FBR0E7SUFDQSxVQUFVLEVBQUU7QUFFWjtJQUNBLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGFBQWEsRUFBRTtBQUNmLGlCQUFpQjtBQUNyQixRQUFRLFlBQVksQ0FBQztBQUNyQixRQUFRLGFBQWEsQ0FBQztBQUN0QixRQUFRLFVBQVUsQ0FBQztBQUNuQixRQUFRLGFBQWEsQ0FBQztBQUN0QixRQUFRLGFBQWEsQ0FBQztBQUN0QixRQUFRLFVBQVUsQ0FBQztBQUNuQixRQUFRLGFBQWEsQ0FBQztBQUN0QixRQUFRLGFBQWEsQ0FBQztBQUN0QixRQUFRLFVBQVUsQ0FBQztBQUNuQixTQUFTLGFBQWEsQ0FBQztBQUN2QixTQUFTLGFBQWEsQ0FBQztBQUN2QixTQUFTLFdBQVcsQ0FBQztBQUVyQjtFQUNFO0lBQ0UsV0FBVztFQUNiO0FBQ0Y7QUFFQTtJQUNJO1FBQ0ksd0JBQXdCO1FBQ3hCLHlCQUF5QjtJQUM3QjtBQUNKO0FBQ0E7SUFDSTtRQUNJLHdCQUF3QjtRQUN4Qix5QkFBeUI7SUFDN0I7QUFDSjtBQUVBO0lBQ0k7UUFDSSx3QkFBd0I7UUFDeEIseUJBQXlCO0lBQzdCO0FBQ0o7QUFFQTtJQUNJO1FBQ0ksd0JBQXdCO1FBQ3hCLHlCQUF5QjtJQUM3QjtBQUNKO0FBRUE7SUFDSTtRQUNJLHdCQUF3QjtRQUN4Qix5QkFBeUI7SUFDN0I7QUFDSjtBQUVBO0lBQ0k7UUFDSSx3QkFBd0I7UUFDeEIseUJBQXlCO0lBQzdCO0FBQ0o7QUFDQTtJQUNJLGlCQUFpQjtBQUNyQjtBQUVBO0lBQ0ksVUFBVTtJQUNWLHlCQUF5QjtJQUN6QixXQUFXO0FBQ2YiLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbi5ub3RpZmljYXRpb24tY2xpY2sge1xyXG5jdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLnNjcm9sbGJhci1kZWVwLXB1cnBsZTo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgNnB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGNUY1RjU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4OyB9XHJcbiAgICBcclxuICAgIC5zY3JvbGxiYXItZGVlcC1wdXJwbGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Y1RjVGNTsgfVxyXG4gICAgXHJcbiAgICAuc2Nyb2xsYmFyLWRlZXAtcHVycGxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgNnB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM1MTJkYTg7IH1cclxuICAgIFxyXG4gICAgLnNjcm9sbGJhci1kZWVwLXB1cnBsZSB7XHJcbiAgICBzY3JvbGxiYXItY29sb3I6ICM1MTJkYTggI0Y1RjVGNTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgXHJcbiAgICAudGhpbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgd2lkdGg6IDZweDsgfVxyXG4gICAgXHJcbiAgICAuZXhhbXBsZS0xIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICAgIG92ZXJmbG93LXg6aGlkZGVuIDtcclxuICAgIGhlaWdodDogNTAwcHg7IH1cclxuICAgIC8qIEZvciBkZXNrdG9wOiAqL1xyXG4uY29sLTEge3dpZHRoOiA4LjMzJTt9XHJcbi5jb2wtMiB7d2lkdGg6IDE2LjY2JTt9XHJcbi5jb2wtMyB7d2lkdGg6IDI1JTt9XHJcbi5jb2wtNCB7d2lkdGg6IDMzLjMzJTt9XHJcbi5jb2wtNSB7d2lkdGg6IDQxLjY2JTt9XHJcbi5jb2wtNiB7d2lkdGg6IDUwJTt9XHJcbi5jb2wtNyB7d2lkdGg6IDU4LjMzJTt9XHJcbi5jb2wtOCB7d2lkdGg6IDY2LjY2JTt9XHJcbi5jb2wtOSB7d2lkdGg6IDc1JTt9XHJcbi5jb2wtMTAge3dpZHRoOiA4My4zMyU7fVxyXG4uY29sLTExIHt3aWR0aDogOTEuNjYlO31cclxuLmNvbC0xMiB7d2lkdGg6IDEwMCU7fVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gIFtjbGFzcyo9XCJjb2wtXCJdIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMzIwcHgpIHtcclxuICAgIC5jb2wtOCB7XHJcbiAgICAgICAgZmxleDogMCAwIDUwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1heC13aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMzIxcHgpIGFuZCAobWF4LXdpZHRoOjQ4MHB4KXtcclxuICAgIC5jb2wtOCB7XHJcbiAgICAgICAgZmxleDogMCAwIDUwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1heC13aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDQ4MXB4KSBhbmQgKG1heC13aWR0aDo3MjBweCkge1xyXG4gICAgLmNvbC04IHtcclxuICAgICAgICBmbGV4OiAwIDAgNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWF4LXdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQobWluLXdpZHRoOiA3MjFweCkgYW5kIChtYXgtd2lkdGg6MTA4MHB4KSB7XHJcbiAgICAuY29sLTgge1xyXG4gICAgICAgIGZsZXg6IDAgMCA1MCUgIWltcG9ydGFudDtcclxuICAgICAgICBtYXgtd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZChtaW4td2lkdGg6IDEwODBweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCl7XHJcbiAgICAuY29sLTgge1xyXG4gICAgICAgIGZsZXg6IDAgMCA1MCUgIWltcG9ydGFudDtcclxuICAgICAgICBtYXgtd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgxcHgpIGFuZCAobWF4LXdpZHRoOiAxOTIwcHgpe1xyXG4gICAgLmNvbC04IHtcclxuICAgICAgICBmbGV4OiAwIDAgNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWF4LXdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG4uZXJyb3ItbXNnIHtcclxuICAgIGJvcmRlci1jb2xvcjogcmVkO1xyXG59XHJcblxyXG4uaHJsaW5lIHtcclxuICAgIHdpZHRoOiA5NiU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWRlZGVkO1xyXG4gICAgaGVpZ2h0OiAwcHg7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 5596:
/*!*************************************************************!*\
  !*** ./src/app/modules/notification/notification.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotificationModule": () => (/* binding */ NotificationModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _notification_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notification-routing.module */ 3621);
/* harmony import */ var _notification_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./notification.component */ 8855);
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/shared.module */ 4466);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);





class NotificationModule {
}
NotificationModule.ɵfac = function NotificationModule_Factory(t) { return new (t || NotificationModule)(); };
NotificationModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: NotificationModule });
NotificationModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule,
            _notification_routing_module__WEBPACK_IMPORTED_MODULE_0__.NotificationRoutingModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](NotificationModule, { declarations: [_notification_component__WEBPACK_IMPORTED_MODULE_1__.NotificationComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
        src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule,
        _notification_routing_module__WEBPACK_IMPORTED_MODULE_0__.NotificationRoutingModule] }); })();


/***/ })

}]);
//# sourceMappingURL=src_app_modules_notification_notification_module_ts.js.map