(self["webpackChunkfilesharing"] = self["webpackChunkfilesharing"] || []).push([["src_app_modules_other-user_other-user_module_ts"],{

/***/ 1:
/*!*****************************************************************!*\
  !*** ./src/app/modules/other-user/other-user-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OtherUserRoutingModule": () => (/* binding */ OtherUserRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _other_user_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./other-user.component */ 2774);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);




const routes = [{
        path: '',
        component: _other_user_component__WEBPACK_IMPORTED_MODULE_0__.OtherUserComponent
    }];
class OtherUserRoutingModule {
}
OtherUserRoutingModule.ɵfac = function OtherUserRoutingModule_Factory(t) { return new (t || OtherUserRoutingModule)(); };
OtherUserRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: OtherUserRoutingModule });
OtherUserRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](OtherUserRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule] }); })();


/***/ }),

/***/ 2774:
/*!************************************************************!*\
  !*** ./src/app/modules/other-user/other-user.component.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OtherUserComponent": () => (/* binding */ OtherUserComponent)
/* harmony export */ });
/* harmony import */ var _pdftron_webviewer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @pdftron/webviewer */ 7309);
/* harmony import */ var _pdftron_webviewer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pdftron_webviewer__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Constants_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Constants/constants */ 5029);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/common.service */ 5620);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);







function OtherUserComponent_h4_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "h4");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r0.shared_file.file.originalFileName);
} }
function OtherUserComponent_ng_template_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h4", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "Add Comment");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function OtherUserComponent_ng_template_16_Template_button_click_3_listener() { const modal_r3 = ctx.$implicit; return modal_r3.dismiss("Cross click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, "\u00D7");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "div", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](11, "textarea", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](14, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](15, "Start Date");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](16, "input", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](17, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](20, "End Date");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](21, "input", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](22, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](23, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](24, "a", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function OtherUserComponent_ng_template_16_Template_a_click_24_listener() { const modal_r3 = ctx.$implicit; return modal_r3.close("Close click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](25, " Share ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](26, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function OtherUserComponent_ng_template_16_Template_button_click_26_listener() { const modal_r3 = ctx.$implicit; return modal_r3.close("Close click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](27, " Cancel ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
class OtherUserComponent {
    constructor(cs, router, activatedRoute) {
        this.cs = cs;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.isSignature = true;
        this.sign_submitted = false;
        this.activatedRoute.queryParams.subscribe(params => {
            this.file_id = params['id'];
            this.name = params['name'];
            this.email = params['email'];
            // this.type =  params['type'];
        });
    }
    ngOnInit() {
        this.cs.httpRequest('get', `file-management/other-user-file?id=${this.file_id}&name=${this.name}&email=${this.email}`).subscribe(res => {
            console.log(res);
            this.shared_file = res;
            const file_name = this.shared_file.file.fileName;
            const documentBlobObjectUrl = _Constants_constants__WEBPACK_IMPORTED_MODULE_1__.baseUrl + 'file-management/file/' + file_name;
            // this.fetchUserDetails();
            this.uploadWebViewver(documentBlobObjectUrl);
        });
    }
    // fetchUserDetails() {
    //   this.cs.httpRequest('get', `user`)?.subscribe(
    //     (res: any) => {
    //       console.log(res);
    //       this.user_details = res;
    //     },
    //     (err) => this.cs.handleError(err)
    //   );
    // }
    uploadWebViewver(documentBlobObjectUrl) {
        _pdftron_webviewer__WEBPACK_IMPORTED_MODULE_0___default()({
            path: './../../../../assets/lib',
            initialDoc: documentBlobObjectUrl
        }, document.getElementById('viewer'))
            .then(instance => {
            const { UI, Core } = instance;
            const { documentViewer, annotationManager, Tools, Annotations } = Core;
            // call methods from UI, Core, documentViewer and annotationManager as needed
            instance.UI.disableElements(this.disableElementsForAdmin());
            instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Z);
            instance.UI.hotkeys.off(instance.UI.hotkeys.Keys.CTRL_Y);
            instance.UI.setZoomLevel(0.4);
            // create a form field
            const { WidgetFlags } = Annotations;
            // set author name for comment
            annotationManager.setAnnotationDisplayAuthorMap((userId) => {
                return this.name;
            });
            // const displayAuthor = annotationManager.getDisplayAuthor(annotation.Author);
            // add_annot_btn is your own custom button
            // document.getElementById('rm_annot_btn').addEventListener('click', () => {
            // });
            // add_annot_btn is your own custom button
            // document.getElementById('add_annot_btn').addEventListener('click', () => {
            // });
            documentViewer.addEventListener('documentLoaded', () => {
                console.log('document loaded');
            });
            documentViewer.addEventListener('annotationsLoaded', () => {
                console.log('annots loadded');
                this.is_annots_loaded = true;
            });
            instance.UI.setHeaderItems(header => {
                const parent = documentViewer.getScrollViewElement().parentElement;
                const menu = document.createElement('div');
                menu.classList.add('Overlay');
                menu.classList.add('FlyoutMenu');
                menu.style.padding = '1em';
                const downloadBtn = document.createElement('button');
                downloadBtn.textContent = 'Download';
                downloadBtn.onclick = () => {
                    // Download
                };
                menu.appendChild(downloadBtn);
                let isMenuOpen = false;
                const renderCustomMenu = () => {
                    this.saveBtn = document.createElement('button');
                    this.saveBtn.textContent = 'Save';
                    this.saveBtn.id = 'savebtn';
                    this.saveBtn.disabled = false;
                    this.saveBtn.hidden = true;
                    this.saveBtn.onclick = () => {
                        console.log(55);
                        annotationManager.exportAnnotations().then(xfdfString => {
                            console.log(44555);
                            const doc = documentViewer.getDocument();
                            doc.getFileData({ xfdfString }).then(data => {
                                const arr = new Uint8Array(data);
                                const blob = new Blob([arr], { type: 'application/pdf' });
                                console.log(blob);
                                console.log(URL.createObjectURL(blob));
                                let api_data = {
                                    file_id: parseInt(this.file_id)
                                };
                                // this.redirectLink();
                                // return;
                                // console.log(this.selectedUser);
                                this.blobImage(blob, api_data);
                            });
                        });
                        if (isMenuOpen) {
                            parent.removeChild(menu);
                        }
                        else {
                            menu.style.left = `${document.body.clientWidth - (this.saveBtn.clientWidth + 40)}px`;
                            menu.style.right = 'auto';
                            menu.style.top = '40px';
                            parent.appendChild(menu);
                        }
                        isMenuOpen = !isMenuOpen;
                    };
                    return this.saveBtn;
                };
                const newCustomElement = {
                    type: 'customElement',
                    render: renderCustomMenu,
                };
                header.push(newCustomElement);
            });
            annotationManager.addEventListener('annotationChanged', (annotations, action) => {
                // use when new annots added
                if (action === 'add' && !this.is_annots_loaded) {
                    const annots = annotationManager.getAnnotationsList();
                    console.log('this is a change that added annotations');
                    const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
                    const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
                    const sign_here_annots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
                    // const signAnnots2 = annots.filter(a => a instanceof Annotations.type);
                    console.log('widgetAnnots', widgetAnnots);
                    console.log('signAnnots', signAnnots);
                    console.log('signAnnots', annots);
                    console.log('sign_here_annots', sign_here_annots);
                    annots.forEach((element, i) => {
                        console.log('element id', element.Id);
                        // console.log('this.user_details.id',this.user_details.id);
                        if (element.getCustomData('user_id')) {
                            element.Hidden = (this.email + ' ' + this.name) != (element.getCustomData('user_id')).trim() ? true : false;
                        }
                        else {
                            element.Hidden = (this.email + ' ' + this.name) != (element.Id).trim() ? true : false;
                        }
                        element.ReadOnly = true;
                        console.log(element);
                        console.log('user_id', element.getCustomData('user_id'));
                    });
                }
                // use when annots read from docs
                if (action === 'add' && this.is_annots_loaded) {
                    console.log('annotations', annotations);
                    annotations[0].setCustomData('user_id', (this.email + ' ' + this.name));
                    annotations[0].NoMove = true;
                    const annots = annotationManager.getAnnotationsList();
                    console.log('annots', annots);
                    console.log('this is a change that added annotations');
                    const widgetAnnots = annots.filter(a => a instanceof Annotations.FreeTextAnnotation);
                    const signAnnots = annots.filter(a => a instanceof Annotations.FreeHandAnnotation);
                    const sign_here_annots = annots.filter(a => a instanceof Annotations.SignatureWidgetAnnotation);
                    console.log('widgetAnnots', widgetAnnots);
                    console.log('signAnnots', signAnnots);
                    console.log('sign_here_annots', sign_here_annots);
                    widgetAnnots.forEach((element, i) => {
                        element.Locked = true;
                        element.LockedContents = true;
                        // console.log('length',(this.email+ ' '+ this.name).length);
                        // console.log((this.email+ ' '+ this.name));
                        // console.log('element id',element.Id);
                        // console.log('elemnt user id length',(element.Id).length);
                        // console.log( (this.email+ ' '+ this.name) != element.Id);
                        element.Hidden = (this.email + ' ' + this.name) != (element.Id).trim() ? true : false;
                        element.ReadOnly = true;
                    });
                    // sign_here_annots.forEach((element, i) => {
                    //   element.Locked = true;
                    //   element.LockedContents = true;
                    //   console.log('length',(this.email+ ' '+ this.name).length);
                    //   console.log((this.email+ ' '+ this.name));
                    //   console.log('element id',element.getCustomData('user_id'));
                    //   console.log('elemnt user id length',(element.getCustomData('user_id')).length);
                    //   console.log( (this.email+ ' '+ this.name) != element.getCustomData('user_id'));
                    //   element.Hidden = (this.email+ ' '+ this.name) != (element.getCustomData('user_id')).trim() ? true:false;
                    //   element.ReadOnly = true;
                    // });
                    signAnnots.forEach((element, i) => {
                        element.Hidden = (this.email + ' ' + this.name) != (element.getCustomData('user_id')).trim() ? true : false;
                        element.ReadOnly = (this.email + ' ' + this.name) != (element.getCustomData('user_id')).trim() ? true : false;
                        console.log(element);
                        console.log('user_id', element.getCustomData('user_id'));
                    });
                }
                else if (action === 'modify') {
                    console.log('this change modified annotations');
                }
                else if (action === 'delete') {
                    console.log('deleted annotation', annotations);
                }
            });
            instance.UI.loadDocument(documentBlobObjectUrl);
            // later save the document with updated annotations data
            documentViewer.addEventListener('annotationsLoaded', () => {
            });
        });
    }
    blobImage(blob, api_data) {
        const formData = new FormData();
        formData.append('file', blob, this.shared_file.fileName);
        console.log('blobbbb', blob);
        this.cs.pdfUpload(formData).subscribe((events) => {
            if (events.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpEventType.Response) {
                console.log(events.body);
                api_data.filename = events.body.filename;
                api_data.name = this.name;
                api_data.email = this.email;
                this.updateFile(api_data);
                this.cs.showInfoMsg('File Shared Successfully', 'Uploaded');
            }
        }, (err) => {
            if (err.status == 401)
                return this.cs.unAuthorized();
            this.cs.showErrorMsg(err.message, 'Error');
        });
    }
    updateFile(api_data) {
        this.cs.httpRequest('post', 'file-management/update-other-user-file', api_data).subscribe((res) => {
            console.log(res);
            this.sign_submitted = true;
            location.reload();
            //  this.router.navigateByUrl('/file-management/listing');
            //  this.redirectLink();
        }, (err) => {
            if (err.status == 401)
                return this.cs.unAuthorized();
            this.cs.showErrorMsg(err.message, 'Error');
        });
    }
    redirectLink() {
        this.router.navigateByUrl('/file-management/shared');
    }
    saveFile() {
        this.saveBtn.click();
        let btn = document.getElementById('savebtn');
        console.log(btn);
    }
    handleDenial() {
    }
    handleDismiss(e) {
    }
    submitSign() {
        if (this.shared_file.sign_status == 'Pending' && this.shared_file.is_locked) {
            this.cs.showInfoMsg('Locked File', 'file is locked, its not your turn.');
        }
        else if (!this.shared_file.is_locked) {
            this.saveFile();
        }
    }
    disableElementsForAdmin() {
        return [
            'viewControlsButton',
            'downloadButton',
            'printButton',
            'fileAttachmentToolGroupButton',
            'toolbarGroup-Edit',
            'crossStampToolButton',
            'checkStampToolButton',
            'dotStampToolButton',
            'rubberStampToolGroupButton',
            'dateFreeTextToolButton',
            // 'toolbarGroup-Shapes',
            'toolbarGroup-Forms',
            'shapeToolGroupButton',
            'highlightToolGroupButton',
            'freeHandHighlightToolGroupButton',
            'freeHandToolGroupButton',
            'selectToolButton',
            'strikeoutToolGroupButton',
            'squigglyToolGroupButton',
            'calloutToolGroupButton',
            'dropdown-item-toolbarGroup-Annotate',
            'linkButton',
            //  'dropdown-item-toolbarGroup-FillAndSign',
            'dropdown-item-toolbarGroup-Insert',
            'freeTextToolButton',
            'freeHandToolButton',
            'eraserToolButton',
            'redoButton',
            'undoButton',
            'dropdown-item-toolbarGroup-View',
            'dropdown-item-toolbarGroup-Shapes',
            'toggleNotesButton',
            'dropdown-item-toolbarGroup-Shapes',
            'toolbarGroup-Insert',
            'toolbarGroup-Shapes',
            'toolbarGroup-Annotate',
            'freeTextToolGroupButton',
            'toolbarGroup-View',
            'toolbarGroup-FillAndSign',
            'signatureToolGroupButton',
            'toolsOverlay'
        ];
    }
}
OtherUserComponent.ɵfac = function OtherUserComponent_Factory(t) { return new (t || OtherUserComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_common_service__WEBPACK_IMPORTED_MODULE_2__.CommonService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute)); };
OtherUserComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: OtherUserComponent, selectors: [["app-other-user"]], decls: 18, vars: 3, consts: [[1, "row"], [1, "col-xl-12", "col-md-12", "box-col-12"], [1, "file-content"], [1, "card"], [1, "card-header"], [1, "d-flex", "align-items-center"], [4, "ngIf"], [1, "media-body", "text-right", 3, "hidden"], [1, "btn", "btn-outline-primary", "ms-2", 3, "disabled", "click"], [1, "fa", "fa-check", "p-r-5"], [1, "card-body"], [1, "col-12", "px-5"], ["id", "viewer", 1, "pdfFile"], ["content", ""], [1, "modal-header"], [1, "modal-title"], ["type", "button", "aria-label", "Close", 1, "close", 3, "click"], ["aria-hidden", "true"], [1, "modal-body"], [1, "form-group"], [1, "input-group"], [1, "col-sm-12", "mb-3"], ["name", "Text1", "placeholder", "Comment (Optional)", "cols", "60", "rows", "5", 1, "form-control"], [1, "col-sm-6", "col-md-6"], [1, "mb-3"], [1, "form-label"], ["type", "date", 1, "form-control"], [1, "modal-footer"], ["type", "button", 1, "btn", "btn-light"], [1, "text-dark", 3, "click"], ["type", "button", 1, "btn", "btn-primary", 3, "click"]], template: function OtherUserComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](7, OtherUserComponent_h4_7_Template, 2, 1, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function OtherUserComponent_Template_button_click_9_listener() { return ctx.submitSign(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](10, "i", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, "Submit Sign ");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](14, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](15, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](16, OtherUserComponent_ng_template_16_Template, 28, 0, "ng-template", null, 13, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplateRefExtractor"]);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.shared_file);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("hidden", ctx.shared_file && ctx.shared_file.sign_status == "Completed" && ctx.shared_file.is_locked);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("disabled", ctx.sign_submitted);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__.NgIf], styles: [".pdfFile[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height: 100vh;\r\n    border: 1px solid #888;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm90aGVyLXVzZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0FBQ3ZCIiwiZmlsZSI6Im90aGVyLXVzZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wZGZGaWxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 7833:
/*!*********************************************************!*\
  !*** ./src/app/modules/other-user/other-user.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OtherUserModule": () => (/* binding */ OtherUserModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _other_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./other-user-routing.module */ 1);
/* harmony import */ var _other_user_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./other-user.component */ 2774);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);




class OtherUserModule {
}
OtherUserModule.ɵfac = function OtherUserModule_Factory(t) { return new (t || OtherUserModule)(); };
OtherUserModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: OtherUserModule });
OtherUserModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _other_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.OtherUserRoutingModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](OtherUserModule, { declarations: [_other_user_component__WEBPACK_IMPORTED_MODULE_1__.OtherUserComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
        _other_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.OtherUserRoutingModule] }); })();


/***/ })

}]);
//# sourceMappingURL=src_app_modules_other-user_other-user_module_ts.js.map